package utils

var AssessmentStatusTransition = map[string]map[string]bool{
	"ON_WORK": {
		"ON_WORK":                              true,
		"DC_REFUSAL":                           true,
		"ON_MANAGER_APPROVAL":                  true,
		"ASSESSMENT_APPROVED":                  true,
		"CLIENT_NOT_SATISFIED_WITH_ASSESSMENT": true,
		"DIAGNOSTICS":                          true,
		"DELETED":                              true,
	},
	"DC_REFUSAL": {
		"ON_WORK": true,
		"DELETED": true,
	},
	"ON_MANAGER_APPROVAL": {
		"ON_WORK":             true,
		"ON_MANAGER_APPROVAL": true,
		"ASSESSMENT_APPROVED": true,
		"DELETED":             true,
	},
	"ASSESSMENT_APPROVED": {
		"ON_WORK":                              true,
		"CLIENT_NOT_SATISFIED_WITH_ASSESSMENT": true,
		"DIAGNOSTICS":                          true,
		"DELETED":                              true,
	},
	"CLIENT_NOT_SATISFIED_WITH_ASSESSMENT": {
		"ON_WORK":                              true,
		"CLIENT_NOT_SATISFIED_WITH_ASSESSMENT": true,
		"DELETED":                              true,
	},
	"DIAGNOSTICS": {
		"ON_WORK":                               true,
		"CLIENT_NOT_SATISFIED_WITH_ASSESSMENT":  true,
		"DIAGNOSTICS":                           true,
		"DIAGNOSTICS_ON_APPROVAL":               true,
		"CLIENT_NOT_SATISFIED_WITH_DIAGNOSTICS": true,
		"SERVICE_CENTER_REMOVAL":                true,
		"DELETED":                               true,
	},
	"DIAGNOSTICS_ON_APPROVAL": {
		"ON_WORK":                               true,
		"DIAGNOSTICS":                           true,
		"DIAGNOSTICS_ON_APPROVAL":               true,
		"DIAGNOSTICS_APPROVED":                  true,
		"CLIENT_NOT_SATISFIED_WITH_DIAGNOSTICS": true,
		"SERVICE_CENTER_REMOVAL":                true,
		"DELETED":                               true,
	},
	"DIAGNOSTICS_APPROVED": {
		"ON_WORK":                               true,
		"DIAGNOSTICS_APPROVED":                  true,
		"CLIENT_NOT_SATISFIED_WITH_DIAGNOSTICS": true,
		"SERVICE_CENTER_REMOVAL":                true,
		"DELETED":                               true,
	},
	"CLIENT_NOT_SATISFIED_WITH_DIAGNOSTICS": {
		"ON_WORK": true,
		"DELETED": true,
	},
	"CLIENT_REFUSAL": {
		"ON_WORK":        true,
		"CLIENT_REFUSAL": true,
		"DELETED":        true,
	},
	"SERVICE_CENTER_REMOVAL": {
		"ON_WORK":                true,
		"SERVICE_CENTER_REMOVAL": true,
		"CLIENT_REFUSAL":         true,
		"DIAGNOSTICS": 			  true,
		"SALE_PREPARATION":       true,
		"DELETED":                true,
	},
	"SALE_PREPARATION": {
		"ON_ROP_APPROVAL":     true,
		"CLIENT_REFUSAL":      true,
		"SALE_PREPARATION":    true,
		"ON_SALE":             true,
		"RESERVE":             true,
		"TEMPORALLY_NOT_SALE": true,
		"RETURN_TO_CLIENT":    true,
		"DELETED":             true,
	},
	"ON_ROP_APPROVAL": {
		"SALE_PREPARATION": true,
		"ON_SALE":          true,
		"RETURN_TO_CLIENT": true,
		"DELETED":          true,
	},
	"ON_SALE": {
		"SALE_PREPARATION":    true,
		"RESERVE":             true,
		"RETURN_TO_CLIENT":    true,
		"TEMPORALLY_NOT_SALE": true,
		"DELETED":             true,
	},
	"RESERVE": {
		"SALE_PREPARATION":    true,
		"ON_SALE":             true,
		"RESERVE":             true,
		"TEMPORALLY_NOT_SALE": true,
		"RETURN_TO_CLIENT":    true,
		"DELETED":             true,
	},
	"RETURN_TO_CLIENT": {
		"RETURN_TO_CLIENT": true,
		"DELETED":          true,
	},
	"TEMPORALLY_NOT_SALE": {
		"ON_WORK":             true,
		"SALE_PREPARATION":    true,
		"TEMPORALLY_NOT_SALE": true,
		"DELETED":             true,
	},
	"DELETED": {
		"ON_WORK": true,
		"DELETED": true,
	},
}

func IsAssessmentStatusTransitionValid(srcStatus string, dstStatus string) (isValid bool) {
	srcStatusTransition, _ := AssessmentStatusTransition[srcStatus]
	if srcStatusTransition[dstStatus] {
		return true
	}
	return
}
