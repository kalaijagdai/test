package utils

import (
	"crypto/tls"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/tradeincar/backend/api/model"
	gomail "gopkg.in/mail.v2"
	"gorm.io/gorm"
)

const SQLInjectPattern = `(?i);|'`

var CharacteristicTypes = map[string]uint{
	"engineTypeName":       12,
	"engineVolume":         13,
	"enginePower":          14,
	"kppTypeName":          24,
	"driveWheelTypeName":   27,
	"carBodyStyleTypeName": 2,
	"doorCount":            3,
	"steeringTypeName":     53,
}

const DefaultPrice float32 = 0

func TryGetPrice(price *float32) float32 {
	if price == nil {
		return DefaultPrice
	} else {
		return *price
	}
}

// takes a struct as parameter
func GetDictionary(entity interface{}) (dict map[string]interface{}, err error) {
	dict = make(map[string]interface{})
	v := reflect.ValueOf(entity)
	typeOfV := v.Type()
	for i := 0; i < v.NumField(); i++ {
		fmt.Printf("Field: %s\tValue: %v\n", typeOfV.Field(i).Name, v.Field(i).Interface())
		dict[typeOfV.Field(i).Name] = v.Field(i).Interface()
	}

	if len(dict) == 0 {
		err = errors.New("Empty dict in GetDictionary returned")
	}

	return
}

func GetQueryParams(c *gin.Context) map[string]interface{} {
	params := make(map[string]interface{})
	for i, j := range c.Request.URL.Query() {
		params[i] = j[0]
	}
	return params
}

func CsvReader(filename string) ([][]string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return [][]string{}, err
	}
	defer f.Close()

	records, err := csv.NewReader(f).ReadAll()
	if err != nil {
		return [][]string{}, err
	}
	fmt.Println(reflect.TypeOf(records))
	return records, nil
}

func FileDownloader(filepath string, url string) error {

	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)
	return err
}

func ParseTime(str string) (t time.Time, err error) {
	t, err = time.Parse(time.RFC3339, str)
	return
}

func PasswordGenerator(length int) string {
	rand.Seed(time.Now().UnixNano())
	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"abcdefghijklmnopqrstuvwxyz" +
		"0123456789")
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}
	return b.String()
}

func SendMail(to string, msg string) bool {

	m := gomail.NewMessage()
	m.SetHeader("From", "info@adaptive.plus")
	m.SetHeader("To", to)
	m.SetHeader("Subject", "Mycar PRO")
	m.SetBody("text/plain", msg)

	d := gomail.NewDialer("smtp.yandex.com", 465, "info@adaptive.plus", "jatsiw-nozxUs-kakde6")

	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	if err := d.DialAndSend(m); err != nil {
		fmt.Println(err)
		return false
	}

	fmt.Println("to: ", to)
	return true
}

func FileUploader(c *gin.Context) (filepath string, err error) {
	file, header, err := c.Request.FormFile("file")
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	// unique file name
	newFileName := uuid.New().String() + header.Filename
	dst, err := os.Create("static/" + newFileName)
	defer dst.Close()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	_, err = io.Copy(dst, file)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	filepath = "/static/" + newFileName

	return
}

func StructTransformer(from, to interface{}) (err error) {
	data, err := json.Marshal(from)
	if err != nil {
		return
	}
	err = json.Unmarshal(data, &to)
	return
}

func CustomCharacteristicValsMapper(customCharacteristicVals []model.CarCustomCharacteristicValue) map[uint]string {
	customCharacteristicValMap := make(map[uint]string)

	for _, val := range customCharacteristicVals {
		index := val.CarCharacteristicID
		if val.CarCharacteristicValue != nil {
			customCharacteristicValMap[index] = val.CarCharacteristicValue.Value
		} else {
			customCharacteristicValMap[index] = val.Value
		}
	}

	return customCharacteristicValMap
}

func Unscopedizer() func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.Unscoped()
	}
}

func FindInSlice(slice []string, val string) (int, bool) {
	for i, item := range slice {
		if item == val {
			return i, true
		}
	}
	return -1, false
}

type FlexibleJsonFloat32 float32

func (f *FlexibleJsonFloat32) ParseFloat() float32 {
	return float32(*f)
}

func (f *FlexibleJsonFloat32) UnmarshalJSON(b []byte) error {
	if len(b) == 0 {
		return fmt.Errorf("cannot unmarshal empty float")
	} else if b[0] == '"' && b[1] == '"' {
		return nil
	} else if b[0] == '"' {

		var targetStr string
		err := json.Unmarshal(b, &targetStr)
		if err != nil {
			return err
		}

		parsedFloat32, err := strconv.ParseFloat(targetStr, 32)
		if err != nil {
			return err
		}

		*f = FlexibleJsonFloat32(parsedFloat32)
		return nil
	} else {
		var targetFloat32 float32
		err := json.Unmarshal(b, &targetFloat32)
		if err != nil {
			return err
		}

		*f = FlexibleJsonFloat32(targetFloat32)
		return nil
	}
}

type FlexibleJsonUint uint

func (f *FlexibleJsonUint) ParseUint() uint {
	if f != nil {
		return uint(*f)
	}
	return 0
}

func (f *FlexibleJsonUint) UnmarshalJSON(b []byte) error {
	if len(b) == 0 {
		return fmt.Errorf("cannot unmarshal empty float")
	} else if b[0] == '"' && b[1] == '"' {
		return nil
	} else if b[0] == '"' {

		var targetStr string
		err := json.Unmarshal(b, &targetStr)
		if err != nil {
			return err
		}

		parsedUint, err := strconv.ParseUint(targetStr, 10, 32)
		if err != nil {
			return err
		}

		*f = FlexibleJsonUint(parsedUint)
		return nil
	} else {
		var targetUint uint
		err := json.Unmarshal(b, &targetUint)
		if err != nil {
			return err
		}

		*f = FlexibleJsonUint(targetUint)
		return nil
	}
}
