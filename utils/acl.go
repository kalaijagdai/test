package utils

var Acl = map[string]map[string]bool{
	"RESERVE": {
		"MANAGER":    true,
		"SPECIALIST": true,
	},
	"assessment_status_without_approval": {
		"MANAGER": true, // todo: logic for status approve
	},
	"ASSESSMENT_APPROVE": {
		"MANAGER": true, // todo: logic for status approve
	},
	"SALE_APPROVE": {
		"MANAGER": true, // todo: logic for status approve
	},
	"CLOSE_APPROVE": {
		"MANAGER": true, // todo: logic for status approve
	},
	"close_sale": {
		"MANAGER":    true, // status transition
		"SPECIALIST": true,
	},
	"assessment_status_from_sale_to_sale_preparation": { // ?
		"MANAGER":    true, // from status to status exactly or just to status?
		"SPECIALIST": true,
		//(все кроме атз),
	},
	// закрыта -> подготовка к продаже только менеджер
	"assessment_status_from_ON_ROP_APPROVAL_to_sale_preparation": {
		"MANAGER": true, // from status to status exactly or just to status?
	},
	"take_car_to_warehouse": {
		"MANAGER":    true, // (which status?) (спец цоне - подготовка к продаже)
		"SPECIALIST": true,
	},
}

//var Acl = map[string]map[string]bool{
//	"CREATE_DELETE_DEALERSHIP": {
//		"OWNER": true,
//	},
//	"CREATE_UPDATE_DELETE_USER_MANAGER": { // inner logic
//		"OWNER": true,
//	},
//	"assessment_status_to_reserve": {
//		"MANAGER":    true, // status transition
//		"SPECIALIST": true,
//	},
//	"CREATE_UPDATE_DELETE_USER": {
//		"MANAGER": true,
//	},
//	"READ_DEALERSHIP": {
//		"OWNER": true,
//	},
//	"CREATE_CLIENT": {
//		"MANAGER":    true,
//		"ATZ":        true,
//		"SPECIALIST": true,
//	},
//	"CREATE_ASSESSMENT": {
//		"MANAGER":    true,
//		"SPECIALIST": true,
//	},
//	"assessment_status_without_approval": {
//		"MANAGER": true, // todo: logic for status approve
//	},
//	"ASSESSMENT_APP ROVE": {
//		"MANAGER": true, // todo: logic for status approve
//	},
//	"SALE_APPROVE": {
//		"MANAGER": true, // todo: logic for status approve
//	},
//	"CLOSE_APPROVE": {
//		"MANAGER": true, // todo: logic for status approve
//	},
//	"UPDATE_DEAL_USER": { // inner logic
//		"MANAGER": true,
//		"ATZ":     true,
//	},
//	"close_deal_with_approval": {
//		"SPECIALIST": true, // status transition
//	},
//	"close_deal_without_approval": {
//		"MANAGER": true, // status transition
//	},
//	"suspend_sale": {
//		"MANAGER":    true, // status transition
//		"SPECIALIST": true,
//	},
//	"close_sale": {
//		"MANAGER":    true, // status transition
//		"SPECIALIST": true,
//	},
//	"assessment_status_from_sale_to_sale_preparation": {
//		"MANAGER":    true, // status transition
//		"SPECIALIST": true,
//	},
//	"assessment_status_from_ON_ROP_APPROVAL_to_sale_preparation": {
//		"MANAGER": true, // status transition
//	},
//	"take_car_to_warehouse": {
//		"MANAGER":    true, // status transition
//		"SPECIALIST": true,
//	},
//	"UPDATE_ASSESSMENT_DEALERSHIP": {
//		"MANAGER": true, // inner logic
//	},
//}

// func CanGoToStatusByRole(status string, role string) (isValid bool) {
// 	roleAction, _ := Acl[action]
// 	if roleAction[role] {
// 		return true
// 	}
// 	return
// }

//func CanDoByRole(action string, role string) (isValid bool) {
//	roleAction, _ := Acl[action]
//	if roleAction[role] {
//		return true
//	}
//	return
//}
