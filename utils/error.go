package utils

import (
	"encoding/json"
	"io/ioutil"
)

type ErrorCatalog struct {
	ErrorResponse []ErrorResponse `json:"errors"`
}

type ErrorResponse struct {
	Code     int     `json:"code"`
	Message  string  `json:"message"`
	HTTPCode int     `json:"HTTPCode"`
	Fields   []Field `json:"fields"`
}

type Field struct {
	Name    string   `json:"name"`
	Message []string `json:"message"`
}

func New(code int, err error) (response ErrorResponse, errorMessage string) {
	file, _ := ioutil.ReadFile("errors.json")
	data := ErrorCatalog{}
	_ = json.Unmarshal([]byte(file), &data)
	for _, v := range data.ErrorResponse {
		if v.Code == code {
			response = v
		}
	}
	errorMessage = err.Error()
	return response, errorMessage
}
