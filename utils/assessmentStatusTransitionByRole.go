package utils

import (
	"errors"
)

var AppropriateStatuses = map[string]map[string][]string {
	"ON_WORK": {
		"SPECIALIST": {"DC_REFUSAL", "ON_MANAGER_APPROVAL", "CLIENT_NOT_SATISFIED_WITH_ASSESSMENT"},
		"MANAGER": {"DC_REFUSAL", "CLIENT_NOT_SATISFIED_WITH_ASSESSMENT", "DIAGNOSTICS" },
		"TECHNICIAN": {},
		"OWNER": {"DC_REFUSAL", "CLIENT_NOT_SATISFIED_WITH_ASSESSMENT", "DIAGNOSTICS",  "DELETED"},
		"ATZ": {},
	},
	"DC_REFUSAL": {
		"SPECIALIST": {"COPY"},
		"MANAGER": {"COPY"},
		"TECHNICIAN": {},
		"OWNER": {"ON_WORK", "COPY", "DELETED"},
		"ATZ": {},
	},
	"ON_MANAGER_APPROVAL": {
		"SPECIALIST": {},
		"MANAGER": {"ON_WORK", "ASSESSMENT_APPROVED"},
		"TECHNICIAN": {},
		"OWNER": {"ON_WORK", "ASSESSMENT_APPROVED", "DELETED"},
		"ATZ": {},
	},
	"ASSESSMENT_APPROVED": {
		"SPECIALIST": {"CLIENT_NOT_SATISFIED_WITH_ASSESSMENT", "DIAGNOSTICS"},
		"MANAGER": {"ON_WORK", "CLIENT_NOT_SATISFIED_WITH_ASSESSMENT", "DIAGNOSTICS"},
		"TECHNICIAN": {},
		"OWNER": {"ON_WORK", "CLIENT_NOT_SATISFIED_WITH_ASSESSMENT", "DIAGNOSTICS",  "DELETED"},
		"ATZ": {},
	},
	"CLIENT_NOT_SATISFIED_WITH_ASSESSMENT": {
		"SPECIALIST": {"COPY"},
		"MANAGER": {"COPY"},
		"TECHNICIAN": {},
		"OWNER": {"ON_WORK", "COPY", "DELETED"},
		"ATZ": {},
	},
	"DIAGNOSTICS": {
		"SPECIALIST": {"DIAGNOSTICS_ON_APPROVAL", "CLIENT_NOT_SATISFIED_WITH_DIAGNOSTICS"},
		"MANAGER": {"ON_WORK", "CLIENT_NOT_SATISFIED_WITH_DIAGNOSTICS", "SERVICE_CENTER_REMOVAL"},
		"TECHNICIAN": {},
		"OWNER": {"ON_WORK", "CLIENT_NOT_SATISFIED_WITH_ASSESSMENT", "CLIENT_NOT_SATISFIED_WITH_DIAGNOSTICS", "SERVICE_CENTER_REMOVAL", "DELETED"},
		"ATZ": {},
	},
	"DIAGNOSTICS_ON_APPROVAL": {
		"SPECIALIST": {},
		"MANAGER": {"DIAGNOSTICS", "DIAGNOSTICS_APPROVED", "CLIENT_NOT_SATISFIED_WITH_DIAGNOSTICS", "SERVICE_CENTER_REMOVAL"},
		"TECHNICIAN": {},
		"OWNER": {"ON_WORK", "DIAGNOSTICS", "DIAGNOSTICS_APPROVED", "CLIENT_NOT_SATISFIED_WITH_DIAGNOSTICS", "SERVICE_CENTER_REMOVAL", "DELETED"},
		"ATZ": {},
	},
	"DIAGNOSTICS_APPROVED": {
		"SPECIALIST": {"CLIENT_NOT_SATISFIED_WITH_DIAGNOSTICS", "SERVICE_CENTER_REMOVAL"},
		"MANAGER": {"DIAGNOSTICS", "CLIENT_NOT_SATISFIED_WITH_DIAGNOSTICS", "SERVICE_CENTER_REMOVAL"},
		"TECHNICIAN": {},
		"OWNER": {"ON_WORK", "CLIENT_NOT_SATISFIED_WITH_DIAGNOSTICS", "SERVICE_CENTER_REMOVAL", "DELETED"},
		"ATZ": {},
	},
	"CLIENT_NOT_SATISFIED_WITH_DIAGNOSTICS": {
		"SPECIALIST": {"COPY"},
		"MANAGER": {"COPY"},
		"TECHNICIAN": {},
		"OWNER": {"COPY", "ON_WORK", "SERVICE_CENTER_REMOVAL", "CLIENT_REFUSAL", "DELETED"},
		"ATZ": {},
	},
	"CLIENT_REFUSAL": {
		"SPECIALIST": {"COPY"},
		"MANAGER": {"COPY"},
		"TECHNICIAN": {},
		"OWNER": {"ON_WORK", "COPY", "SERVICE_CENTER_REMOVAL", "DELETED"},
		"ATZ": {},
	},
	"SERVICE_CENTER_REMOVAL": {
		"SPECIALIST": {"CLIENT_REFUSAL", "SALE_PREPARATION"},
		"MANAGER": {"CLIENT_REFUSAL", "SALE_PREPARATION", "DIAGNOSTICS"},
		"TECHNICIAN": {},
		"OWNER": {"ON_WORK", "CLIENT_REFUSAL", "SALE_PREPARATION", "DIAGNOSTICS", "DELETED"},
		"ATZ": {},
	},
	"SALE_PREPARATION": {
		"SPECIALIST": {"ON_ROP_APPROVAL", "CLIENT_REFUSAL"},
		"MANAGER": {"ON_SALE", "RESERVE", "TEMPORALLY_NOT_SALE", "RETURN_TO_CLIENT"},
		"TECHNICIAN": {"ON_ROP_APPROVAL"},
		"OWNER": {"ON_SALE", "RESERVE", "TEMPORALLY_NOT_SALE", "DELETED"},
		"ATZ": {},
	},
	"ON_ROP_APPROVAL": {
		"SPECIALIST": {},
		"MANAGER": {"SALE_PREPARATION", "ON_SALE", "RETURN_TO_CLIENT"},
		"TECHNICIAN": {},
		"OWNER": {"SALE_PREPARATION", "ON_SALE", "RETURN_TO_CLIENT", "DELETED"},
		"ATZ": {},
	},
	"ON_SALE": {
		"SPECIALIST": {"SALE_PREPARATION", "RESERVE", "TEMPORALLY_NOT_SALE"},
		"MANAGER": {"SALE_PREPARATION", "RESERVE", "RETURN_TO_CLIENT", "TEMPORALLY_NOT_SALE"},
		"TECHNICIAN": {"SALE_PREPARATION"},
		"OWNER": {"SALE_PREPARATION", "RESERVE", "RETURN_TO_CLIENT", "TEMPORALLY_NOT_SALE", "DELETED"},
		"ATZ": {},
	},
	"RESERVE": {
		"SPECIALIST": {"SALE_PREPARATION", "ON_SALE", "TEMPORALLY_NOT_SALE"},
		"MANAGER": {"SALE_PREPARATION", "ON_SALE", "TEMPORALLY_NOT_SALE"},
		"TECHNICIAN": {},
		"OWNER": {"SALE_PREPARATION", "ON_SALE", "TEMPORALLY_NOT_SALE", "DELETED"},
		"ATZ": {},
	},
	"RETURN_TO_CLIENT": {
		"SPECIALIST": {"COPY"},
		"MANAGER": {"COPY"},
		"TECHNICIAN": {},
		"OWNER": {"COPY", "DELETED"},
		"ATZ": {},
	},
	"TEMPORALLY_NOT_SALE": {
		"SPECIALIST": {"COPY"},
		"MANAGER": {"COPY", "SALE_PREPARATION"},
		"TECHNICIAN": {},
		"OWNER": {"ON_WORK", "COPY", "DIAGNOSTICS", "SALE_PREPARATION", "DELETED"},
		"ATZ": {},
	},
	"DELETED": {
		"SPECIALIST": {"COPY"},
		"MANAGER": {"COPY"},
		"TECHNICIAN": {},
		"OWNER": {"ON_WORK", "COPY", "DELETED"},
		"ATZ": {},
	},
}

func GetAppropriateAssessmentStatuses(assessmentTypeName string, managerAffirmation bool, saleTransfer bool, srcStatus string, roleName string) (statuses []string, err error) {
	appropriateRoleStatuses, ok := AppropriateStatuses[srcStatus]
	if ok == false {
		err = errors.New("No such source status in AppropriateAssessmentStatuses")
		return
	}

	appropriateStatuses, ok := appropriateRoleStatuses[roleName]
	if ok == false {
		err = errors.New("No such role in AppropriateAssessmentStatuses")
		return
	}

	statuses = make([]string, len(appropriateStatuses))
	copy(statuses, appropriateStatuses)

	if assessmentTypeName != "COMMISSION" {
		for i, status := range statuses {
			if status == "RETURN_TO_CLIENT" {
				statuses = append(statuses[:i], statuses[i+1:]...)
			}
		}
	}

	// подтверждение оценок руководителем не требутеся
	if managerAffirmation == false {
		for i, status := range statuses {
			// manager doesn't need to approve (remove ON_MANAGER_APPROVAL status)
			if status == "ASSESSMENT_APPROVED" {
				statuses = append(statuses[:i], statuses[i+1:]...)
			}

			if status == "DIAGNOSTICS_APPROVED" {
				statuses = append(statuses[:i], statuses[i+1:]...)
			}

			// specialist doesn't need to send on approval, he can change to required status immediately
			if status == "ON_MANAGER_APPROVAL" {
				statuses[i] = "DIAGNOSTICS"
			}

			// specialist doesn't need to send on approval, he can change to required status immediately
			if status == "DIAGNOSTICS_ON_APPROVAL" {
				statuses[i] = "SERVICE_CENTER_REMOVAL"
			}

		}
	}

	// подтверждение перевода в продажу руководителем не требутеся
	if saleTransfer == false {
		for i, status := range statuses {
			// specialist doesn't need to send on approval, he can change to required status immediately
			if status == "ON_ROP_APPROVAL" {
				statuses[i] = "ON_SALE"
				statuses = append(statuses,"RETURN_TO_CLIENT")
			}
		}
	}

	return
}


