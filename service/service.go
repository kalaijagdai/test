package service

import (
	"gitlab.com/tradeincar/backend/api/db"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/dict"
)

type Service struct {
	Manager       *db.Manager
	Configuration *model.Configuration
	Dict          *dict.Dict
}

func NewService(manager *db.Manager, configuration *model.Configuration, dict *dict.Dict) *Service {
	return &Service{
		Manager:       manager,
		Configuration: configuration,
		Dict:          dict,
	}
}
