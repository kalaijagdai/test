package main

import (
	"strconv"

	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/controller"
	"gitlab.com/tradeincar/backend/api/db"
	"gitlab.com/tradeincar/backend/api/dict"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/service"
)

func main() {
	_cfg := model.NewConfiguration("config.json")
	_db := db.NewDB(_cfg)
	_dbm := db.NewDBManager(_db)
	_dict := dict.NewDict(_dbm)
	_service := service.NewService(_dbm, _cfg, _dict)
	_controller := controller.NewController(_service)

	if _cfg.DbMigrate {
		_dbm.AutoMigrate()
	}
	if _cfg.DbDataMigrate {
		_dbm.AutoMigrateData()
	}
	if _cfg.GinReleaseMode {
		gin.SetMode(gin.ReleaseMode)
	}

	r := gin.Default()
	r.Use(controller.CORSMiddleware())
	r.Use(static.Serve("/", static.LocalFile("./static", true)))

	r.GET("/healthcheck", _controller.Healtcheck)

	// auth
	r.POST("/api/login", _controller.Login)
	r.POST("/api/reset-password", _controller.ResetPassword)
	r.POST("/api/logout", _controller.Logout)

	// workers
	r.POST("expired/assessment/reserve", _controller.ManangeExpiredAssessmentsOnReserve)
	r.POST("expired/assessment/sale", _controller.ManangeNotChangedAssessmentsOnSale)

	r.Use(_controller.AuthMiddleware())

	base := r.Group("/api")
	// client
	base.POST("/client", _controller.CreateClient)
	base.GET("/client", _controller.GetClients)
	base.GET("/clientFormatted", _controller.GetFormattedClients)
	base.GET("/client/:id", _controller.GetClient)
	base.PUT("/client/:id", _controller.UpdateClient)
	base.DELETE("/client/:id", _controller.DeleteClient)
	base.POST("/client/scan", _controller.ClientPassportScan) //сканирование удостоверения

	// car
	base.POST("/car", _controller.CreateCar)
	base.GET("/car", _controller.GetCars)
	base.GET("/car/assessment", _controller.GetCarOnAssessment)
	base.GET("/car/warehouse", _controller.GetCarsOnWarehouse)
	base.GET("/car/sale", _controller.GetCarsOnSale)
	// base.GET("/car/:id", _controller.GetCarByID)
	// base.GET("/car/:id/info", _controller.GetCarInfoByID)
	base.GET("/car/kppType", _controller.GetAllCarKppType)
	base.GET("/car/engineType", _controller.GetAllCarEngineType)
	base.GET("/car/driveWheelType", _controller.GetAllCarDriveWheelType)
	base.GET("/car/carBodyStyleType", _controller.GetAllCarBodyStyleType)
	base.GET("/car/steeringType", _controller.GetAllSteeringType)
	base.GET("/car/vin/:vin", _controller.GetCarByVIN)

	//marks
	base.GET("/mark", _controller.GetAllMarks)
	base.GET("/mark/:id/model", _controller.GetModelsByMarkID)
	// ! deprecated
	base.GET("/model/:id", _controller.GetModelsByMarkID)

	//models
	base.GET("/model/:id/year", _controller.GetCarYearsByModelID)
	base.GET("/model/:id/generation", _controller.GetGenerationsByModelIDAndYear)

	//modification
	base.GET("/modification/:id", _controller.GetModificationByID)

	// contact
	base.POST("/deal/:id/contact", _controller.CreateContact)
	base.GET("/contact", _controller.GetContacts)

	// dealStatus
	base.POST("deal/:id/status/:status", _controller.CreateDealStatus)

	// deal
	base.POST("/deal", _controller.CreateDeal)
	base.GET("/deal/:id", _controller.GetDeal)
	base.GET("/deal", _controller.GetDeals)

	//dc
	base.POST("/dealership", _controller.CreateDealership)
	base.GET("/dealership", _controller.GetAllDealerships)
	base.GET("/dealership/:id/user", _controller.GetDealershipUsers)
	base.GET("/dealership/:id", _controller.GetDealership)
	base.PUT("/dealership/:id", _controller.UpdateDealership)
	base.PATCH("/dealership/remove-mark/:dealershipId", _controller.RemoveMarkFromDealership)
	base.DELETE("/dealership/:id", _controller.DeleteDealership)

	//assessment
	base.POST("/assessment", _controller.CreateAssessment)
	base.POST("/assessment/:id/clone", _controller.CloneAssessment)
	base.GET("/assessment", _controller.GetAssessments)
	base.GET("/assessment/:id", _controller.GetAssessment)
	base.PUT("/assessment/:id", _controller.UpdateAssessment)
	base.DELETE("/assessment/:id", _controller.CanBeModifiedOnThisStatus, _controller.DeleteAssessment)
	//assessment-car-info
	base.PUT("/assessment/:id/car-info", _controller.CanBeModifiedOnThisStatus, _controller.UpdateAssessmentCarInfo)
	//assessment-price
	base.POST("/assessment/:id/planned-price", _controller.CanBeModifiedOnThisStatus, _controller.AddAssessmentPrice)
	base.GET("/assessment/:id/pricing-history", _controller.GetAssessmentPricingHistory)

	//assessment change status to manager approve
	//base.PUT("/assessment/:id/status/:status")

	//assessmentPricing
	base.GET("/assessment/:id/pricing", _controller.GetPricingByAssessmentID)

	//assessmentStatus
	base.POST("/assessment/:id/status/:status", _controller.ChangeAssessmentStatus)

	//assessmentAdmission
	base.PUT("/assessment/:id/admission", _controller.CanBeModifiedOnThisStatus, _controller.UpdateAssessmentAdmission)

	//service
	base.POST("/dealership/:id/service", _controller.CreateService)
	base.GET("/dealership/:id/service", _controller.GetDealershipServices)
	base.GET("/service", _controller.GetServices)
	base.GET("/service/:id", _controller.GetService)
	base.PUT("/service/:id", _controller.UpdateService)
	base.DELETE("/service/:id", _controller.DeleteService)

	//addition
	base.POST("/dealership/:id/addition", _controller.CreateAddition)
	base.GET("/dealership/:id/addition", _controller.GetDealershipAdditions)
	base.GET("/addition", _controller.GetAdditions)
	base.GET("/addition/:id", _controller.GetAddition)
	base.PUT("/addition/:id", _controller.UpdateAddition)
	base.DELETE("/addition/:id", _controller.DeleteAddition)

	//assessmentAddition
	base.POST("/assessment/:id/addition/:status", _controller.CanBeModifiedOnThisStatus, _controller.AddAssessmentAddition)
	base.PUT("/assessment/:id/addition", _controller.CanBeModifiedOnThisStatus, _controller.UpdateAssessmentAddition)
	base.DELETE("/assessment-addition", _controller.DeleteAssessmentAddition)

	//assessmentService
	base.POST("/assessment/:id/service/:status", _controller.CanBeModifiedOnThisStatus, _controller.AddAssessmentService)
	base.PUT("/assessment/:id/service", _controller.CanBeModifiedOnThisStatus, _controller.UpdateAssessmentService)
	base.DELETE("/assessment-service", _controller.DeleteAssessmentService)

	//assessmentGear
	base.POST("/assessment/:id/gear", _controller.CanBeModifiedOnThisStatus, _controller.AddAssessmentGear)
	base.PUT("/assessment/:id/gear", _controller.CanBeModifiedOnThisStatus, _controller.UpdateAssessmentGear)
	base.DELETE("/assessment-gear", _controller.DeleteAssessmentGear)

	//assessmentDocument
	base.GET("/assessment/:id/document", _controller.GetDocumentsByAssessmentID)
	base.POST("/assessment/:id/document", _controller.CanBeModifiedOnThisStatus, _controller.AddAssessmentDocument)
	base.DELETE("/document/:id", _controller.DeleteAssessmentDocument)
	// base.POST("/assessment/document/file", _controller.UploadDocumentFile)

	//assessmentPhoto
	base.GET("/assessment/:id/photo", _controller.GetPhotosByAssessmentID)
	base.POST("/assessment/:id/photo", _controller.CanBeModifiedOnThisStatus, _controller.AddAssessmentPhoto)
	base.POST("/assessment/:id/favorite-photo/:photoID", _controller.CanBeModifiedOnThisStatus, _controller.ChoiseFavoriteAssessmentPhoto)
	base.DELETE("/photo/:id", _controller.DeleteAssessmentPhoto)
	// base.POST("/assessment/:id/photo/file", _controller.UploadDocumentFile)

	//assessmentReport
	base.POST("/assessment-report", _controller.CreateAssessmentReport)
	base.PUT("/assessment-report/:id", _controller.UpdateAssessmentReport)
	base.GET("/assessment-report/:id", _controller.GetAssessmentReportByID)

	//tradein
	base.PUT("/assessment/:id/tradein", _controller.CanBeModifiedOnThisStatus, _controller.UpdateTradeIn)

	//sale
	base.PUT("/assessment/:id/sale", _controller.CanBeModifiedOnThisStatus, _controller.UpdateSale)

	//reserve
	base.PUT("/assessment/:id/reserve", _controller.CanBeModifiedOnThisStatus, _controller.UpdateReserve)

	//users
	base.GET("/me", _controller.GetMe)
	base.POST("/user", _controller.CreateUser)
	base.GET("/user", _controller.GetAllUsers)
	base.GET("/specialist", _controller.GetAllSpecialist)
	base.GET("/user/:id", _controller.GetUser)
	base.PUT("/user/:id", _controller.UpdateUser)
	base.PATCH("/user/remove-dealership/:userId", _controller.RemoveDealershipFromUser)
	base.DELETE("/user/:id", _controller.DeleteUser)

	//helpers
	base.GET("/helpers/dealership/status", _controller.GetAllDealershipStatuses)
	base.GET("/helpers/user/status", _controller.GetAllUserStatuses)
	base.GET("/helpers/gender", _controller.GetAllGenders)
	base.GET("/helpers/communication", _controller.GetAllCommunications)
	base.GET("/helpers/deal/goal", _controller.GetAllDealGoals)
	base.GET("/helpers/referral", _controller.GetAllReferrals)
	base.GET("/helpers/role", _controller.GetAllRoles)
	base.GET("/helpers/client/type", _controller.GetAllClientTypes)
	base.GET("/helpers/document/type", _controller.GetAllDocumentTypes)
	base.GET("/helpers/photo/type", _controller.GetAllPhotoTypes)
	base.GET("/helpers/assessment/status", _controller.GetAllAssessmentStatuses)
	base.GET("/helpers/assessment/type", _controller.GetAllAssessmentTypes)
	base.GET("/helpers/car/interest", _controller.GetAllCarInterests)
	base.GET("/helpers/relevance", _controller.GetAllRelevances)
	base.GET("/helpers/car/status", _controller.GetAllCarStatuses)
	base.GET("/helpers/assessment/stage", _controller.GetAllAssessmentStages)
	base.GET("/helpers/countries", _controller.GetCountries)
	base.GET("/helpers/countries/:countryID/regions", _controller.GetRegions)
	base.GET("/helpers/countries/:countryID/factories", _controller.GetFactories)
	base.GET("/helpers/countries/:countryID/banks", _controller.GetBanks)
	base.GET("/helpers/payment-methods", _controller.GetPaymentMethods)
	base.GET("/helpers/report/statuses", _controller.ReportStatuses)

	//inspections
	base.POST("/inspection", _controller.CreateInspection)
	base.PUT("/inspection/:id", _controller.UpdateInspection)
	base.GET("/inspection", _controller.GetAllInspections)
	base.GET("/inspection/detailed", _controller.GetAllInspectionsDetailed)

	//inner inspections
	base.POST("/inner-inspection", _controller.CreateInnerInspection)
	base.PUT("/inner-inspection/:id", _controller.UpdateInnerInspection)
	base.GET("/inner-inspection", _controller.GetAllInnerInspections)
	//inner inspections view
	base.POST("/inner-inspection-view", _controller.CreateInnerInspectionView)
	base.PUT("/inner-inspection-view/:id", _controller.UpdateInnerInspectionView)
	base.GET("/inner-inspection-view", _controller.GetAllInnerInspectionViews)
	//inner inspections view types
	base.POST("/inner-inspection-view-type", _controller.CreateInnerInspectionViewType)
	base.PUT("/inner-inspection-view-type/:id", _controller.UpdateInnerInspectionViewType)
	base.GET("/inner-inspection-view-type", _controller.GetAllInnerInspectionViewTypes)

	//vin report
	base.POST("/assessment/:id/vin-report", _controller.CreateVinReport)
	base.GET("/assessment/:id/vin-report", _controller.GetAllVinReportByAssessmentID)
	base.GET("/vin-report/:id", _controller.GetVinReportByID)

	base.POST("/test", _controller.TestApi)

	// report
	base.POST("/report/config", _controller.LoadExcel)
	base.POST("/report/save", _controller.SaveReportLabel)
	base.POST("/reports", _controller.CreateReport)
	base.PATCH("/report/:reportID/status", _controller.UpdateStatusByReportID)
	base.GET("/reports", _controller.GetReports)

	base.GET("/analytics/top-marks", _controller.GetReportsTopMarks)
	base.GET("/analytics/top-models", _controller.GetReportsTopModels)
	base.GET("/analytics/total-sells", _controller.GetTotalSells)
	base.GET("/dashboard/total-sells", _controller.GetTotalsForDashboard)
	base.POST("/reports/:reportID/sells", _controller.CreateSellsByReportID)
	base.PATCH("/report-sells/:sellID", _controller.UpdateSellbySellId)
	base.DELETE("/report-sells/:sellID", _controller.DeleteSellbySellId)
	base.GET("/report-sells/:sellID", _controller.GetSellbySellId)
	base.GET("/reports/:reportID/sells", _controller.GetSellsByReportID)

	// file
	base.POST("/file", _controller.UploadFile)
	// base.GET("/file/:filename", _controller.GetFile)

	// adscars
	base.POST("/adscars", _controller.AdsCars(_cfg))
	// filtercars
	base.POST("/filtercars", _controller.CreateFilterCarByUser)
	base.DELETE("/filtercars/:id", _controller.DeleteFilterByCar)
	base.PUT("/filtercars/:id", _controller.UpdateFilterByCar)
	base.GET("/filtercars", _controller.GetAllFilterByCar)

	if err := r.Run(":" + strconv.Itoa(_cfg.HTTPPort)); err != nil {
		panic(err)
	}

}
