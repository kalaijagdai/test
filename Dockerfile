FROM golang:1.15.3-alpine3.12 AS build

RUN apk --update --no-cache add gcc g++ make ca-certificates git tzdata
ENV TZ=Asia/Almaty

WORKDIR /go/src/app
COPY . .
RUN go mod download && go build -o /go/bin/app main.go

FROM alpine:3.7
ENV TZ=Asia/Almaty
RUN apk --update --no-cache add tzdata
WORKDIR /usr/bin
COPY --from=build /go/bin .
COPY --from=build /go/src/app/errors.json errors.json
COPY --from=build /go/src/app/config.json config.json
EXPOSE 4200
CMD ["./app"]
