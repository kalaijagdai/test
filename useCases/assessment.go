package useCases

import (
	"errors"
	"strings"

	"gitlab.com/tradeincar/backend/api/utils"

	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/service"
)

func CopyPriceList(service *service.Service, assessmentID uint, srcStatusID uint, dstStatusID uint) (err error) {
	assessment, err := service.Manager.GetAssessmentByID(assessmentID)
	if err != nil {
		return
	}

	// copy the last pricing to the dstStatus
	err = CopyLastPricing(service, &assessment, srcStatusID, dstStatusID)
	if err != nil {
		return
	}

	// copy services and additions if they don't exist on the dstStatusID
	err = CopySalePreparation(service, &assessment, srcStatusID, dstStatusID)
	if err != nil {
		return
	}

	return
}

func CopyLastPricing(service *service.Service, assessment *model.Assessment, srcStatusID uint, dstStatusID uint) (err error) {
	// copy assessmentPricing from srcStatus
	var assessmentPricingArrayToUpdate []model.AssessmentPricing
	for _, assessmentPricing := range assessment.AssessmentPricing {
		// delete all old pricing from destination status
		if assessmentPricing.AssessmentStatusID == dstStatusID {
			continue
		}

		// copy pricing
		assessmentPricingArrayToUpdate = append(assessmentPricingArrayToUpdate, assessmentPricing)

		// add pricing to dst status by copying it from src status
		if assessmentPricing.AssessmentStatusID == srcStatusID {
			assessmentPricingToCopyToDst := model.AssessmentPricing{
				AssessmentPrice:     assessmentPricing.AssessmentPrice,
				PlannedSellingPrice: assessmentPricing.PlannedSellingPrice,
				Margin:              assessmentPricing.Margin,
				MarginPercent:       assessmentPricing.MarginPercent,
				AssessmentID:        assessment.ID,
				AssessmentStatusID:  dstStatusID,
			}

			assessmentPricingArrayToUpdate = append(assessmentPricingArrayToUpdate, assessmentPricingToCopyToDst)
		}
	}

	// update AssessmentPricing
	err = service.Manager.ReplaceAssessmentPricing(assessment.ID, assessmentPricingArrayToUpdate)
	if err != nil {
		return
	}

	return
}

func CopySalePreparation(service *service.Service, assessment *model.Assessment, srcStatusID uint, dstStatusID uint) (err error) {
	// copy assessmentServices from srcStatus
	var assessmentServicesToUpdate []model.AssessmentService
	for _, assessmentService := range assessment.AssessmentServices {
		// delete all old services from destination status
		if assessmentService.AssessmentStatusID == dstStatusID {
			continue
		}

		// copy services
		assessmentServicesToUpdate = append(assessmentServicesToUpdate, assessmentService)

		// add services to dst status by copying it from src status
		if assessmentService.AssessmentStatusID == srcStatusID {
			assessmentServiceToCopyToDst := model.AssessmentService{
				Name:               assessmentService.Name,
				Price:              assessmentService.Price,
				ByCompany:          assessmentService.ByCompany,
				Visible:            assessmentService.Visible,
				AssessmentID:       assessment.ID,
				AssessmentStatusID: dstStatusID,
			}

			assessmentServicesToUpdate = append(assessmentServicesToUpdate, assessmentServiceToCopyToDst)
		}
	}

	// copy assessmentAdditions from srcStatus
	var assessmentAdditionsToUpdate []model.AssessmentAddition
	for _, assessmentAddition := range assessment.AssessmentAdditions {
		// delete all old additions from destination status
		if assessmentAddition.AssessmentStatusID == dstStatusID {
			continue
		}

		// copy additions
		assessmentAdditionsToUpdate = append(assessmentAdditionsToUpdate, assessmentAddition)

		// add additions to dst status by copying it from src status
		if assessmentAddition.AssessmentStatusID == srcStatusID {
			assessmentAdditionToCopyToDst := model.AssessmentAddition{
				Name:               assessmentAddition.Name,
				Price:              assessmentAddition.Price,
				ByCompany:          assessmentAddition.ByCompany,
				Visible:            assessmentAddition.Visible,
				AssessmentID:       assessment.ID,
				AssessmentStatusID: dstStatusID,
			}

			assessmentAdditionsToUpdate = append(assessmentAdditionsToUpdate, assessmentAdditionToCopyToDst)
		}
	}

	// update AssessmentServices
	err = service.Manager.ReplaceAssessmentServices(assessment.ID, assessmentServicesToUpdate)
	if err != nil {
		return
	}

	// update AssessmentAdditions
	err = service.Manager.ReplaceAssessmentAdditions(assessment.ID, assessmentAdditionsToUpdate)
	if err != nil {
		return
	}

	return
}

func CreateAssessmentEntity(service *service.Service, carID, carInfoID, clientID, assessmentTypeID, dealershipID, userID uint) (assessmentToCreate model.Assessment, err error) {
	onAssessmentStatus, _ := service.Dict.GetAssessmentStatusByName("ON_WORK")

	assessmentToCreate = model.Assessment{
		DealershipID:       dealershipID,
		CarID:              carID,
		CarInfoID:          carInfoID,
		UserID:             userID,
		ClientID:           clientID,
		AssessmentStatusID: onAssessmentStatus.ID,
		AssessmentTypeID:   assessmentTypeID,
	}

	err = service.Manager.CreateAssessment(&assessmentToCreate)
	if err != nil {
		return
	}
	return
}

func CalculateAssessmentPrice(s *service.Service, assessment model.Assessment, price scheme.AssessmentPricingRequest) (assessmentPrice model.AssessmentPricing, err error) {
	if assessment.AssessmentStatus.Name != "ON_WORK" && assessment.AssessmentStatus.Name != "DIAGNOSTICS" && assessment.AssessmentStatus.AssessmentStage.Name != "IN_STOCK" {
		err = errors.New("not satisfied with status")
		return
	}

	var dopSum float32
	for _, add := range assessment.AssessmentAdditions {
		if add.AssessmentStatusID == assessment.AssessmentStatusID {
			addPrice := utils.TryGetPrice(add.Price)
			dopSum += addPrice
		}
	}
	for _, serv := range assessment.AssessmentServices {
		if serv.AssessmentStatusID == assessment.AssessmentStatusID {
			servPrice := utils.TryGetPrice(serv.Price)
			dopSum += servPrice
		}
	}

	//NEW FORMULA
	//priceAssessmentPrice := utils.TryGetPrice(price.AssessmentPrice)
	//pricePlannedSellingPrice := utils.TryGetPrice(price.PlannedSellingPrice)
	//costPrice := dopSum + priceAssessmentPrice
	//margin := pricePlannedSellingPrice - costPrice
	//marginPercent := (margin / pricePlannedSellingPrice) * 100

	// FEATURED FORMULA - not used
	// costPrice := price.AssessmentPrice - dopSum
	// margin := price.PlannedSellingPrice - costPrice
	// marginPercent := (margin/price.PlannedSellingPrice) * 100

	// METACAR FORMULA - no used
	//costPrice := dopSum + price.AssessmentPrice
	//margin := price.PlannedSellingPrice - costPrice
	//marginPercent := (margin / costPrice) * 100

	assessmentPrice = model.AssessmentPricing{
		AssessmentPrice:     price.AssessmentPrice,
		PlannedSellingPrice: price.PlannedSellingPrice,
		AssessmentID:        assessment.ID,
		AssessmentStatusID:  assessment.AssessmentStatusID,
		Margin:              price.Margin,
		MarginPercent:       price.MarginPercent,
	}

	exist := false
	for _, pricing := range assessment.AssessmentPricing {
		if pricing.ID > 0 && pricing.AssessmentStatusID == assessment.AssessmentStatusID {
			exist = true
			assessmentPrice.ID = pricing.ID
			break
		}
	}

	if exist {
		err = s.Manager.UpdateAssessmentPricing(&assessmentPrice)
		if err != nil {
			return
		}
	} else {
		err = s.Manager.CreateAssessmentPricing(&assessmentPrice)
		if err != nil {
			return
		}
	}

	return assessmentPrice, err
}

func RecalculateMarginOfAssessmentPrice(s *service.Service, assessment model.Assessment) (err error) {
	if len(assessment.AssessmentPricing) < 1 {
		return
	}

	assessmentPricing, err := s.Manager.GetLastAssessmentPricingByIdAndStatusId(assessment.ID, assessment.AssessmentStatusID)

	if assessmentPricing.ID < 1 {
		return
	}
	var dopSum float32
	for _, add := range assessment.AssessmentAdditions {
		if add.AssessmentStatusID == assessment.AssessmentStatusID {
			addPrice := utils.TryGetPrice(add.Price)
			dopSum += addPrice
		}
	}
	for _, serv := range assessment.AssessmentServices {
		if serv.AssessmentStatusID == assessment.AssessmentStatusID {
			servPrice := utils.TryGetPrice(serv.Price)
			dopSum += servPrice
		}
	}

	// change margin if in_stock or change assessmentPrice if on_work/diagnostics
	if assessment.AssessmentStatus.AssessmentStage.Name != "IN_STOCK" {
		assessmentPricingPlannedSellingPrice := utils.TryGetPrice(assessmentPricing.PlannedSellingPrice)
		assessmentPricingMargin := utils.TryGetPrice(assessmentPricing.Margin)
		assessmentPricingAssessmentPrice := assessmentPricingPlannedSellingPrice - assessmentPricingMargin - dopSum

		assessmentPricing.AssessmentPrice = &assessmentPricingAssessmentPrice
	} else {
		assessmentPricingPlannedSellingPrice := utils.TryGetPrice(assessmentPricing.PlannedSellingPrice)
		assessmentPricingAssessmentPrice := utils.TryGetPrice(assessmentPricing.AssessmentPrice)

		marginToUpdate := assessmentPricingPlannedSellingPrice - assessmentPricingAssessmentPrice - dopSum
		assessmentPricing.Margin = &marginToUpdate

		marginPercentToUpdate := (marginToUpdate / assessmentPricingPlannedSellingPrice) * 100
		assessmentPricing.MarginPercent = &marginPercentToUpdate
	}

	// FEATURED FORMULA - not used - no delete
	//costPrice := assessmentPricing.AssessmentPrice - dopSum
	//margin := assessmentPricing.PlannedSellingPrice - costPrice
	//marginPercent := (margin/assessmentPricing.PlannedSellingPrice) * 100

	// METACAR FORMULAR - not used - no delete
	//costPrice := dopSum + assessmentPricing.AssessmentPrice
	//margin := assessmentPricing.PlannedSellingPrice - costPrice
	//marginPercent := (margin / costPrice) * 100

	// assessmentPricing.Margin = margin
	// assessmentPricing.MarginPercent = marginPercent

	err = s.Manager.UpdateAssessmentPricing(&assessmentPricing)
	if err != nil {
		return
	}

	history := model.AssessmentPricingHistory {
		AssessmentPrice: 	assessmentPricing.AssessmentPrice,
		PlannedSellingPrice: assessmentPricing.PlannedSellingPrice,
		AssessmentStatusID: assessment.AssessmentStatusID,
		AssessmentID:        assessment.ID,
	}
	err = s.Manager.CreateAssessmentPricingHistory(&history)

	return
}

func IsAssessmentReadyForOnManangerApproval(service *service.Service, assessment *model.Assessment) (assessmentToReturn model.Assessment, err error) {
	var missedFields []string
	if assessment.CarInfo == nil {
		err = errors.New("нет информации о машине")
		return
	}

	if assessment.CarInfo.PassportSeries == "" {
		missedFields = append(missedFields, "паспортная серия в информации о машине")
	}

	if assessment.CarInfo.PassportNumber == "" {
		missedFields = append(missedFields, "номер паспорта в информации о машине")
	}

	if assessment.CarInfo.PassportIssueDate == nil {
		missedFields = append(missedFields, "дата выдачи в информации о машине")
	}

	if assessment.CarInfo.Mileage == 0 {
		missedFields = append(missedFields, "пробег в информации о машине")
	}

	if assessment.CarInfo.MileageType == "" {
		missedFields = append(missedFields, "тип пробега в информации о машине")
	}

	if assessment.CarInfo.CarCondition == "" {
		missedFields = append(missedFields, "состояние в информации о машине")
	}

	if assessment.CarInfo.CarBodyColor == "" {
		missedFields = append(missedFields, "цвет в информации о машине")
	}

	if assessment.CarInfo.WarantyIssueDate == nil {
		missedFields = append(missedFields, "дата выдачи гарантии в информации о машине")
	}

	onWorkStatus, err := service.Dict.GetAssessmentStatusByName("ON_WORK")
	if err != nil {
		err = errors.New("No such status in assessment")
		return
	}

	// get AssessmentPrice from Assessment
	var assessmentPricingToCheck model.AssessmentPricing
	for _, assessmentPricing := range assessment.AssessmentPricing {
		if assessmentPricing.AssessmentStatusID == onWorkStatus.ID {
			assessmentPricingToCheck = assessmentPricing
		}
	}

	// if no value for assessmentPrice found
	assessmentPricingToCheckAssessmentPrice := utils.TryGetPrice(assessmentPricingToCheck.AssessmentPrice)
	if assessmentPricingToCheckAssessmentPrice == 0 {
		missedFields = append(missedFields, "Оценочная стоимость в оценке")
	}

	assessmentPricingToCheckPlannedSellingPrice := utils.TryGetPrice(assessmentPricingToCheck.PlannedSellingPrice)
	if assessmentPricingToCheckPlannedSellingPrice == 0 {
		missedFields = append(missedFields, "Планирумая стоимость продажи  в оценке")
	}

	if assessmentPricingToCheck.Margin == nil {
		missedFields = append(missedFields, "Margin в оценке")
	}

	if assessmentPricingToCheck.MarginPercent == nil {
		missedFields = append(missedFields, "MarginPercent в оценке")

	}

	if len(missedFields) > 0 {
		err = errors.New(strings.Join(missedFields, ","))
	}

	return
}

func IsAssessmentReadyForDiagnostics(service *service.Service, assessment *model.Assessment) (assessmentToReturn model.Assessment, err error) {
	var missedFields []string
	if assessment.CarInfo == nil {
		err = errors.New("нет информации о машине")
		return
	}
	if assessment.CarInfo.PassportSeries == "" {
		missedFields = append(missedFields, "паспортная серия в информации о машине")
	}
	if assessment.CarInfo.PassportIssueDate == nil {
		missedFields = append(missedFields, "дата выдачи в информации о машине")
	}
	if assessment.CarInfo.CarCondition == "" {
		missedFields = append(missedFields, "состояние в информации о машине")
	}
	if assessment.CarInfo.CarBodyColor == "" {
		missedFields = append(missedFields, "цвет в информации о машине")
	}
	if assessment.CarInfo.NumberOfOwners == "" {
		missedFields = append(missedFields, "количество владельцев в информации о машине")
	}
	if assessment.CarInfo.Mileage == 0 {
		missedFields = append(missedFields, "пробег в информации о машине")
	}

	engineVolume := utils.CustomCharacteristicValsMapper(assessment.Car.CarCustomCharacteristicValues)[13]

	if engineVolume == "" {
		missedFields = append(missedFields, "объем двигателя")
	}

	onWorkStatus, err := service.Dict.GetAssessmentStatusByName("ON_WORK")
	if err != nil {
		err = errors.New("No such status in assessment")
		return
	}

	// get AssessmentPrice from Assessment
	var assessmentPrice float32
	for _, assessmentPricing := range assessment.AssessmentPricing {
		if assessmentPricing.AssessmentStatusID == onWorkStatus.ID {
			assessmentPricingAssessmentPrice := utils.TryGetPrice(assessmentPricing.AssessmentPrice)
			assessmentPrice = assessmentPricingAssessmentPrice
		}
	}
	if assessmentPrice == 0 {
		missedFields = append(missedFields, "Оценочная стоимость со статуса в оценка работе")
	}

	if len(missedFields) > 0 {
		err = errors.New(strings.Join(missedFields, ","))
	}

	return
}

func IsAssessmentReadyForSalePreaparation(service *service.Service, assessment *model.Assessment) (assessmentToReturn model.Assessment, err error) {
	var missedFields []string
	if assessment.AssessmentAdmission == nil {
		err = errors.New("нет данных о поступлении")
		return
	}
	if assessment.AssessmentAdmission.ClientID == 0 {
		missedFields = append(missedFields, "ID клиента в поступлении")
	}

	assessmentAssessmentAdmissionAdmissionPrice := utils.TryGetPrice(assessment.AssessmentAdmission.AdmissionPrice)
	if assessmentAssessmentAdmissionAdmissionPrice == 0 {
		missedFields = append(missedFields, "стоимость при поступлении")
	}
	if assessment.AssessmentAdmission.ContractNumber == "" {
		missedFields = append(missedFields, "номер договора в поступлении")
	}

	if len(missedFields) > 0 {
		err = errors.New(strings.Join(missedFields, ","))
	}

	return
}

func IsAssessmentReadyForDiagnosticsOnApproval(service *service.Service, assessment *model.Assessment) (assessmentToReturn model.Assessment, err error) {
	var missedFields []string
	if assessment.AssessmentType.Name != "TRADEIN_NEW" && assessment.AssessmentType.Name != "TRADEIN_MILEAGE" {
		return
	}

	if assessment.TradeIn == nil {
		err = errors.New("нет данных о TradeIn")
		return
	}

	assessmentTradeInPrice := utils.TryGetPrice(assessment.TradeIn.Price)
	if assessmentTradeInPrice == 0 {
		missedFields = append(missedFields, "цена в TradeIn")
	}
	if assessment.TradeIn.AssessmentID == 0 {
		missedFields = append(missedFields, "ID оценки в TradeIn")
	}
	if assessment.TradeIn.ClientID == 0 {
		missedFields = append(missedFields, "ID клиента в TradeIn")
	}
	if assessment.TradeIn.UserID == 0 {
		missedFields = append(missedFields, "ID пользователя в TradeIn")
	}
	if assessment.TradeIn.CarID == 0 {
		missedFields = append(missedFields, "ID машины в TradeIn")
	}
	if assessment.TradeIn.SaleAt == nil {
		missedFields = append(missedFields, "дата продажи в TradeIn")
	}

	if len(missedFields) > 0 {
		err = errors.New(strings.Join(missedFields, ","))
	}

	return
}

func IsAssessmentReadyForReserve(service *service.Service, assessment *model.Assessment) (assessmentToReturn model.Assessment, err error) {
	var missedFields []string

	// check if actual reserves for reserve period exists
	actualReserves, err := service.Manager.GetActualReserveRequestsByAssessmentID(assessment.ID, service.Configuration.DaysOnReserve)
	if err != nil || len(actualReserves) < 1 {
		err = errors.New("За последние 10 дней не найдено ни одного запроса на резерв")
		return
	}

	actualReserve := actualReserves[len(actualReserves)-1]

	if actualReserve == (model.Reserve{}) {
		err = errors.New("Данные в резерве не заполнены")
		return
	}

	if actualReserve.ClientID == 0 {
		missedFields = append(missedFields, "клиент в резерве")
	}

	if len(missedFields) > 0 {
		err = errors.New(strings.Join(missedFields, ","))
	}

	return
}

func IsAssessmentReadyForCloseSale(service *service.Service, assessment *model.Assessment) (assessmentToReturn model.Assessment, err error) {
	var missedFields []string
	if assessment.Sale == nil {
		err = errors.New("нет данных о продаже")
		return
	}

	assessmentSalePrice := utils.TryGetPrice(assessment.Sale.Price)
	if assessmentSalePrice == 0 {
		missedFields = append(missedFields, "стоимость продажи") //ok
	}

	if assessment.Sale.ContractNumber == "" {
		missedFields = append(missedFields, "номер договора в продаже")
	}

	if assessment.Sale.AssessmentID == 0 {
		missedFields = append(missedFields, "ID оценке в продаже")
	}

	if assessment.Sale.ClientID == 0 {
		missedFields = append(missedFields, "ID клиента в продаже")
	}

	if assessment.Sale.UserID == 0 {
		missedFields = append(missedFields, "ID специалиста  в продаже")
	}

	if assessment.Sale.IsCASCO == nil {
		missedFields = append(missedFields, "КАСКО в продаже")
	}

	if assessment.Sale.IsCredit == nil {
		missedFields = append(missedFields, "оформление в кредит в продаже")
	}

	if assessment.Sale.SaleAt == nil {
		missedFields = append(missedFields, "дата продажи")
	}

	if len(missedFields) > 0 {
		err = errors.New(strings.Join(missedFields, ","))
	}

	return
}
