package useCases

import (
	"time"

	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/service"
	"gitlab.com/tradeincar/backend/api/utils"
)

func CreateDealCarEntity(s *service.Service, carRequest *scheme.CarInCreate) (createDealCar model.Car, err error) {

	createDealCar, err = CreateCarEntity(s, carRequest)
	if err != nil {
		return
	}

	carCustomCharacteristicValues := []model.CarCustomCharacteristicValue{}

	if carRequest.EngineType != "" {
		if engineType, err := s.Dict.GetEngineTypeByKey(carRequest.EngineType); err == nil {
			engineTypeCharacteristicValue := model.CarCustomCharacteristicValue{
				CarCharacteristicID:      engineType.CarCharacteristicID,
				CarCharacteristicValueID: engineType.ID,
			}
			carCustomCharacteristicValues = append(carCustomCharacteristicValues, engineTypeCharacteristicValue)
		}
	}

	if carRequest.EngineVolume != "" {
		engineVolumeCharacteristicValue := model.CarCustomCharacteristicValue{
			Value:               carRequest.EngineVolume,
			Unit:                "см3",
			CarCharacteristicID: 13, // EngineVolume ID in CarCharacteristic table
		}

		carCustomCharacteristicValues = append(carCustomCharacteristicValues, engineVolumeCharacteristicValue)
	}

	if carRequest.KppType != "" {
		if kppType, err := s.Dict.GetKppTypeByKey(carRequest.KppType); err == nil {
			kppTypeCharacteristicValue := model.CarCustomCharacteristicValue{
				CarCharacteristicID:      kppType.CarCharacteristicID,
				CarCharacteristicValueID: kppType.ID,
			}
			carCustomCharacteristicValues = append(carCustomCharacteristicValues, kppTypeCharacteristicValue)
		}
	}

	if carRequest.DriveWheelType != "" {
		if driveWheelType, err := s.Dict.GetDriveWheelTypeByKey(carRequest.DriveWheelType); err == nil {
			driveWheelTypeCharacteristicValue := model.CarCustomCharacteristicValue{
				CarCharacteristicID:      driveWheelType.CarCharacteristicID,
				CarCharacteristicValueID: driveWheelType.ID,
			}
			carCustomCharacteristicValues = append(carCustomCharacteristicValues, driveWheelTypeCharacteristicValue)
		}
	}

	createDealCar.CarCustomCharacteristicValues = carCustomCharacteristicValues

	return
}

func CreateAssessmentCarEntity(s *service.Service, carRequest *scheme.CarInCreate) (createAssessmentCar model.Car, err error) {

	existCar, _ := s.Manager.GetCarByVIN(carRequest.VIN)

	if carRequest.CarStatus == "ON_ASSESSMENT" && existCar.ID > 1 {
		createAssessmentCar = existCar
		return
	}

	createAssessmentCar, err = CreateCarEntity(s, carRequest)
	if err != nil {
		return
	}

	engineType, err := s.Dict.GetEngineTypeByKey(carRequest.EngineType)
	if err != nil {
		return
	}

	kppType, err := s.Dict.GetKppTypeByKey(carRequest.KppType)
	if err != nil {
		return
	}

	driveWheelType, err := s.Dict.GetDriveWheelTypeByKey(carRequest.DriveWheelType)
	if err != nil {
		return
	}

	carBodyStyleType, err := s.Dict.GetCarBodyStyleTypeByKey(carRequest.CarBodyStyleType)
	if err != nil {
		return
	}

	steeringType, err := s.Dict.GetSteeringTypeByKey(carRequest.SteeringType)
	if err != nil {
		return
	}

	engineTypeCharacteristicValue := model.CarCustomCharacteristicValue{
		CarCharacteristicID:      engineType.CarCharacteristicID,
		CarCharacteristicValueID: engineType.ID,
	}

	engineVolumeCharacteristicValue := model.CarCustomCharacteristicValue{
		Value:               carRequest.EngineVolume,
		Unit:                "см3",
		CarCharacteristicID: 13, // EngineVolume ID in CarCharacteristic table
	}

	enginePowerCharacteristicValue := model.CarCustomCharacteristicValue{
		Value:               carRequest.EnginePower,
		Unit:                "л.с.",
		CarCharacteristicID: 14, // EnginePower ID in CarCharacteristic table
	}

	kppTypeCharacteristicValue := model.CarCustomCharacteristicValue{
		CarCharacteristicID:      kppType.CarCharacteristicID,
		CarCharacteristicValueID: kppType.ID,
	}

	driveWheelTypeCharacteristicValue := model.CarCustomCharacteristicValue{
		CarCharacteristicID:      driveWheelType.CarCharacteristicID,
		CarCharacteristicValueID: driveWheelType.ID,
	}

	carBodyStyleTypeCharacteristicValue := model.CarCustomCharacteristicValue{
		CarCharacteristicID:      carBodyStyleType.CarCharacteristicID,
		CarCharacteristicValueID: carBodyStyleType.ID,
	}

	doorCountCharacteristicValue := model.CarCustomCharacteristicValue{
		Value:               carRequest.DoorCount,
		CarCharacteristicID: 3, // DoorCount ID in CarCharacteristic table
	}

	steeringTypeCharacteristicValue := model.CarCustomCharacteristicValue{
		CarCharacteristicID:      steeringType.CarCharacteristicID,
		CarCharacteristicValueID: steeringType.ID,
	}

	carCustomCharacteristicValues := []model.CarCustomCharacteristicValue{
		engineTypeCharacteristicValue,
		engineVolumeCharacteristicValue,
		enginePowerCharacteristicValue,
		kppTypeCharacteristicValue,
		driveWheelTypeCharacteristicValue,
		carBodyStyleTypeCharacteristicValue,
		doorCountCharacteristicValue,
		steeringTypeCharacteristicValue,
	}

	createAssessmentCar.CarCustomCharacteristicValues = carCustomCharacteristicValues
	createAssessmentCar.CarModificationID = carRequest.CarModificationID

	err = s.Manager.CreateCar(&createAssessmentCar)
	if err != nil {
		return
	}

	return
}

func CreateCarEntity(s *service.Service, carRequest *scheme.CarInCreate) (createCar model.Car, err error) {
	carStatus, err := s.Dict.GetCarStatusByName(carRequest.CarStatus)
	if err != nil {
		return
	}

	createCar = model.Car{
		VIN:         carRequest.VIN,
		CarStatusID: carStatus.ID,
		CarModelID:  carRequest.CarModelID,
	}

	if carRequest.Year != "" {
		carYear := time.Time{}
		if carYear, err = utils.ParseTime(carRequest.Year); err != nil {
			if carYear, err = time.Parse("2006", carRequest.Year); err != nil {
				return
			} else {
				createCar.Year = &carYear
			}
		} else {
			createCar.Year = &carYear
		}
	}

	return
}

func CreateCarInfoEntity(service *service.Service, carInfo *model.CarInfo) (err error) {
	err = service.Manager.CreateCarInfo(carInfo)
	if err != nil {
		return
	}
	return
}
