package useCases

import (
	"errors"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/service"
)

func SendServiceCenterRemoval(service *service.Service, assessment *model.Assessment) (assessmentToReturn model.Assessment, err error) {
	if assessment.AssessmentStatus.Name != "DIAGNOSTICS_APPROVED" {
		//154
		err = errors.New("Not satisfied with status")
		return
	}

	// change status
	destStatus, err := service.Dict.GetAssessmentStatusByName("SERVICE_CENTER_REMOVAL")
	if err != nil {
		return
	}

	assessmentToReturn = model.Assessment{ID: assessment.ID, AssessmentStatusID: destStatus.ID}
	err = service.Manager.UpdateAssessment(&assessmentToReturn)
	if err != nil {
		return
	}

	return
}