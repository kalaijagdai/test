package useCases

import (
	"encoding/json"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/scheme"
)

func ConvertInspectionToOutput(inspection []model.Inspection) (insToOutput []scheme.InspectionResponse, err error) {
	data, err := json.Marshal(inspection)
	if err != nil {
		return
	}

	err = json.Unmarshal(data, &insToOutput)
	if err != nil {
		return
	}
	
	return
}
