package useCases

import (
	"fmt"
	"gitlab.com/tradeincar/backend/api/service"
)

func ManangeExpiredAssessmentsOnReserve(service *service.Service) (err error) {
	fromStatus, _ := service.Dict.GetAssessmentStatusByName("RESERVE")
	toStatus, _ := service.Dict.GetAssessmentStatusByName("ON_SALE")

	assessmentIDs, err := service.Manager.GetExpiredAssessmentIDsOnStatus(fromStatus.ID, service.Configuration.DaysOnReserve)
	if err != nil {
		return
	}

	//if no expired assessment found
	if assessmentIDs == nil {
		return
	}

	err = service.Manager.UpdateAssessmentStatusBulk(assessmentIDs, "Период резерва истек", fromStatus.ID, toStatus.ID)
	if err != nil {
		return
	}
	return
}

func ManangeNotChangedAssessmentsOnSale(service *service.Service) (err error) {
	status, _ := service.Dict.GetAssessmentStatusByName("ON_SALE")

	assessmentIDs, err := service.Manager.GetNotChangedAssessmentsOnStatus(status.ID, service.Configuration.DaysWithoutChangesOnSale)
	if err != nil {
		return
	}

	fmt.Println("ManangeNotChangedAssessmentsOnSale: ", assessmentIDs)
	return
}


