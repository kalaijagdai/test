package useCases

import (
	"fmt"
	"strings"
	"text/template"
	"time"

	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/service"
	"gitlab.com/tradeincar/backend/api/utils"
)

func CreateAssessmentClientEntity(service *service.Service, clientRequest *scheme.AssessmentClientInCreate, user *model.User) (createClient model.Client, err error) {
	createClient = model.Client{
		Name:      clientRequest.Name,
		Telephone: clientRequest.Telephone,
		UserID:    user.ID,
	}
	err = service.Manager.CreateClient(&createClient)
	if err != nil {
		return
	}
	return
}

func CreateClientEntity(service *service.Service, clientRequest *scheme.ClientInCreate, user *model.User) (createClient model.Client, err error) {
	// get Enum values
	clientType := model.ClientType{}
	if clientRequest.ClientType != "" {
		clientType, err = service.Dict.GetClientTypeByName(clientRequest.ClientType)
		if err != nil {
			return
		}
	}

	// check if data valid for client type
	if clientType.ID == 2 && clientRequest.Company == nil {
		err = fmt.Errorf("Company info is required for %v type client", clientType.Name)
		return

	} else if clientType.ID == 1 && clientRequest.Company != nil {
		err = fmt.Errorf("%v type client can not have a company field", clientType.Name)
		return
	}

	clientReferral := model.Referral{}
	if clientRequest.Referral != "" {
		clientReferral, err = service.Dict.GetReferralByName(clientRequest.Referral)
		if err != nil {
			return
		}
	}

	clientGender := model.Gender{}
	if clientRequest.Gender != "" {
		clientGender, err = service.Dict.GetGenderByName(clientRequest.Gender)
		if err != nil {
			return createClient, err
		}
	}

	// create model.Client entity
	createClient = model.Client{
		Name:              clientRequest.Name,
		Surname:           clientRequest.Surname,
		Patronymic:        clientRequest.Patronymic,
		IIN:               clientRequest.IIN,
		Gender:            clientGender,
		GenderID:          clientGender.ID,
		UserID:            user.ID,
		ClientType:        clientType,
		ClientTypeID:      clientType.ID,
		Referral:          clientReferral,
		ReferralID:        clientReferral.ID,
		Telephone:         clientRequest.Telephone,
		WorkPhone:         clientRequest.WorkPhone,
		Email:             clientRequest.Email,
		PassportID:        clientRequest.PassportID,
		PassportAuthority: clientRequest.PassportAuthority,
		Agreement:         clientRequest.Agreement,
		Address:           clientRequest.Address,
		Company:           clientRequest.Company,
	}

	dealGoalID := clientRequest.DealGoalID.ParseUint()
	if dealGoalID > 0 {
		createClient.DealGoalID = &dealGoalID
	}

	// format time for birthday
	if clientRequest.Birthday != "" {
		clientBirthday := time.Time{}
		if clientBirthday, err = utils.ParseTime(clientRequest.Birthday); err != nil {
			return
		} else {
			createClient.Birthday = &clientBirthday
		}
	}

	// format time for PassportIssueDate
	if clientRequest.PassportIssueDate != "" {
		clientPassportIssueDate := time.Time{}
		if clientPassportIssueDate, err = utils.ParseTime(clientRequest.PassportIssueDate); err != nil {
			return
		} else {
			createClient.PassportIssueDate = &clientPassportIssueDate
		}
	}

	// save to DB
	err = service.Manager.CreateClient(&createClient)
	if err != nil {
		return
	}

	if createClient.UserID < 1 {
		return
	}

	// notify user about new client
	emailTmpl := `Здравствуйте, {{.User.FirstName}}! 

К Вам привязан новый клиент
ID: {{.Client.ID}}
Name: {{.Client.Name}} {{.Client.Surname}}
Telephone: {{.Client.Telephone}}

С уважением,
Команда MycarPro
`
	temp := template.Must(template.New("email").Parse(emailTmpl))

	//send emails to dealership users
	notificationModel := scheme.NewClientNotification{
		User:   user,
		Client: &createClient,
	}
	builder := &strings.Builder{}
	if err := temp.Execute(builder, notificationModel); err != nil {
		fmt.Println(err)
	}

	to := user.Email
	msg := builder.String()

	if to != nil {
		go utils.SendMail(*to, msg)
	}

	return
}
