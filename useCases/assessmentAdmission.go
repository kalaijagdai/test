package useCases

import (
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/service"
	"gitlab.com/tradeincar/backend/api/utils"
)

func CreateAssessmentAdmissionEntity(service *service.Service, assessmentID uint) (assessmentAdmissionToCreate model.AssessmentAdmission, err error) {
	assessment, err := service.Manager.GetAssessmentByID(assessmentID)
	if err != nil { //102
		return
	}

	diagnosticStatus, err := service.Dict.GetAssessmentStatusByName("DIAGNOSTICS")
	if err != nil {
		return //109
	}

	onWorkStatus, err := service.Dict.GetAssessmentStatusByName("ON_WORK")
	if err != nil {
		return //109
	}

	// get AssessmentPrice from Assessment
	var assessmentPriceOnDiagnostics model.AssessmentPricing
	var assessmentPriceOnWork model.AssessmentPricing
	for _, assessmentPricing := range assessment.AssessmentPricing {
		if assessmentPricing.AssessmentStatusID == diagnosticStatus.ID {
			assessmentPriceOnDiagnostics = assessmentPricing
		}
		if assessmentPricing.AssessmentStatusID == onWorkStatus.ID {
			assessmentPriceOnWork = assessmentPricing
		}
	}

	// if no value for assessmentPrice found fetch from on_work and update
	assessmentPriceOnDiagnosticsAssessmentPrice := utils.TryGetPrice(assessmentPriceOnDiagnostics.AssessmentPrice)
	if assessmentPriceOnDiagnosticsAssessmentPrice == 0 {
		assessmentPriceOnDiagnostics = model.AssessmentPricing{
			AssessmentPrice:     assessmentPriceOnWork.AssessmentPrice,
			PlannedSellingPrice: assessmentPriceOnWork.PlannedSellingPrice,
			Margin:              assessmentPriceOnWork.Margin,
			MarginPercent:       assessmentPriceOnWork.MarginPercent,
			AssessmentStatusID:  diagnosticStatus.ID,
			AssessmentID:        assessmentID,
		}
		err = service.Manager.CreateAssessmentPricing(&assessmentPriceOnDiagnostics)
		if err != nil {
			return
		}
	}

	assessmentAdmissionToCreate = model.AssessmentAdmission{
		AssessmentPrice: assessmentPriceOnDiagnostics.AssessmentPrice,
	}

	err = service.Manager.AddAssessmentAdmission(assessment.ID, &assessmentAdmissionToCreate)
	if err != nil { //406
		return
	}

	return
}
