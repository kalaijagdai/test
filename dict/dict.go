package dict

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/tradeincar/backend/api/db"
	"gitlab.com/tradeincar/backend/api/model"
)

type Dict struct {
	Referrals               []model.Referral
	Genders                 []model.Gender
	ClientTypes             []model.ClientType
	DealershipStatuses      []model.DealershipStatus
	UserStatuses            []model.UserStatus
	Communications          []model.Communication
	DealGoals               []model.DealGoal
	DealStatuses            []model.DealStatus
	CarInterests            []model.CarInterest
	Relevances              []model.Relevance
	CarStatuses             []model.CarStatus
	Roles                   []model.Role
	AssessmentStatuses      []model.AssessmentStatus
	AssessmentStages        []model.AssessmentStage
	EngineTypes             []model.CarCharacteristicValue
	KppTypes                []model.CarCharacteristicValue
	DriveWheelTypes         []model.CarCharacteristicValue
	CarBodyStyleTypes       []model.CarCharacteristicValue
	SteeringTypes           []model.CarCharacteristicValue
	AssessmentDocumentTypes []model.AssessmentDocumentType
	AssessmentPhotoTypes    []model.AssessmentPhotoType
	VinReportStatuses      []model.VinReportStatus
}

func NewDict(m *db.Manager) *Dict {
	return &Dict{
		Referrals:               m.GetReferrals(),
		Genders:                 m.GetGenders(),
		ClientTypes:             m.GetClientTypes(),
		DealershipStatuses:      m.GetDealershipStatuses(),
		UserStatuses:            m.GetUserStatuses(),
		Communications:          m.GetCommunications(),
		DealGoals:               m.GetDealGoals(),
		DealStatuses:            m.GetDealStatuses(),
		CarInterests:            m.GetCarInterests(),
		Relevances:              m.GetRelevances(),
		CarStatuses:             m.GetCarStatuses(),
		Roles:                   m.GetRoles(),
		AssessmentStatuses:      m.GetAssessmentStatuses(),
		AssessmentStages:        m.GetAssessmentStages(),
		EngineTypes:             m.GetTypesByCarCharacteristicID(12),
		KppTypes:                m.GetTypesByCarCharacteristicID(24),
		DriveWheelTypes:         m.GetTypesByCarCharacteristicID(27),
		CarBodyStyleTypes:       m.GetTypesByCarCharacteristicID(2),
		SteeringTypes:           m.GetTypesByCarCharacteristicID(53),
		AssessmentDocumentTypes: m.GetAssessmentDocumentTypes(),
		AssessmentPhotoTypes:    m.GetAssessmentPhotoTypes(),
		VinReportStatuses:		 m.GetVinReportStatuses(),
	}
}

func (dict *Dict) GetClientTypeByName(name string) (entity model.ClientType, err error) {
	for _, v := range dict.ClientTypes {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("ClientType must be one of that: %v", dict.ClientTypes))
	return
}

func (dict *Dict) GetGenderByName(name string) (entity model.Gender, err error) {
	for _, v := range dict.Genders {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("Gender must be one of that: %v", dict.Genders))
	return
}

func (dict *Dict) GetReferralByName(name string) (entity model.Referral, err error) {
	for _, v := range dict.Referrals {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("Referral must be one of that: %v", dict.Referrals))
	return
}

func (dict *Dict) GetDealershipStatusByName(name string) (entity model.DealershipStatus, err error) {
	for _, v := range dict.DealershipStatuses {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("DealershipStatus must be one of that: %v", dict.DealershipStatuses))
	return
}

func (dict *Dict) GetUserStatusByName(name string) (entity model.UserStatus, err error) {
	for _, v := range dict.UserStatuses {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("UserStatus must be one of that: %v", dict.UserStatuses))
	return
}

func (dict *Dict) GetDealGoalByName(name string) (entity model.DealGoal, err error) {
	for _, v := range dict.DealGoals {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("DealGoal must be one of that: %v", dict.DealGoals))
	return
}

func (dict *Dict) GetDealStatusByName(name string) (entity model.DealStatus, err error) {
	for _, v := range dict.DealStatuses {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("DealStatus must be one of that: %v", dict.DealStatuses))
	return
}

func (dict *Dict) GetCommunicationByName(name string) (entity model.Communication, err error) {
	for _, v := range dict.Communications {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("Communication must be one of that: %v", dict.Communications))
	return
}

func (dict *Dict) GetCarInterestByName(name string) (entity model.CarInterest, err error) {
	for _, v := range dict.CarInterests {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("CarInterest must be one of that: %v", dict.CarInterests))
	return
}

func (dict *Dict) GetRelevanceByName(name string) (entity model.Relevance, err error) {
	for _, v := range dict.Relevances {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("Relevance must be one of that: %v", dict.Relevances))
	return
}

func (dict *Dict) GetCarStatusByName(name string) (entity model.CarStatus, err error) {
	for _, v := range dict.CarStatuses {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("CarStatus must be one of that: %v", dict.CarStatuses))
	return
}

func (dict *Dict) GetRoleByName(name string) (entity model.Role, err error) {
	for _, v := range dict.Roles {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("Role must be one of that: %v", dict.Roles))
	return
}

func (dict *Dict) GetAssessmentStatusByName(name string) (entity model.AssessmentStatus, err error) {
	for _, v := range dict.AssessmentStatuses {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("AssessmentStatus must be one of that: %v", dict.AssessmentStatuses))
	return
}

func (dict *Dict) GetAssessmentStatusByID(id uint) (entity model.AssessmentStatus, err error) {
	for _, v := range dict.AssessmentStatuses {
		if v.ID == id {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("AssessmentStatus must be one of that: %v", dict.AssessmentStatuses))
	return
}

func (dict *Dict) GetAssessmentStageByName(name string) (entity model.AssessmentStage, err error) {
	for _, v := range dict.AssessmentStages {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("AssessmentStage must be one of that: %v", dict.AssessmentStages))
	return
}

func (dict *Dict) GetEngineTypeByKey(key string) (entity model.CarCharacteristicValue, err error) {
	for _, v := range dict.EngineTypes {
		if strings.ToUpper(v.Key) == strings.ToUpper(key) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("EngineType must be one of that: %v", dict.EngineTypes))
	return
}

func (dict *Dict) GetKppTypeByKey(key string) (entity model.CarCharacteristicValue, err error) {
	for _, v := range dict.KppTypes {
		if strings.ToUpper(v.Key) == strings.ToUpper(key) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("KppType must be one of that: %v", dict.KppTypes))
	return
}

func (dict *Dict) GetDriveWheelTypeByKey(key string) (entity model.CarCharacteristicValue, err error) {
	for _, v := range dict.DriveWheelTypes {
		if strings.ToUpper(v.Key) == strings.ToUpper(key) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("DriveWheelType must be one of that: %v", dict.DriveWheelTypes))
	return
}

func (dict *Dict) GetCarBodyStyleTypeByKey(key string) (entity model.CarCharacteristicValue, err error) {
	for _, v := range dict.CarBodyStyleTypes {
		if strings.ToUpper(v.Key) == strings.ToUpper(key) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("CarBodyStyleType must be one of that: %v", dict.CarBodyStyleTypes))
	return
}

func (dict *Dict) GetSteeringTypeByKey(key string) (entity model.CarCharacteristicValue, err error) {
	for _, v := range dict.SteeringTypes {
		if strings.ToUpper(v.Key) == strings.ToUpper(key) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("SteeringType must be one of that: %v", dict.SteeringTypes))
	return
}

func (dict *Dict) GetEngineTypeByValue(value string) (key string) {
	for _, v := range dict.EngineTypes {
		if strings.EqualFold(v.Value, value) {
			key = v.Key
			return
		}
	}
	return
}

func (dict *Dict) GetKppTypeByValue(value string) (key string) {
	for _, v := range dict.KppTypes {
		if strings.EqualFold(v.Value, value) {
			key = v.Key
			return
		}
	}
	return
}

func (dict *Dict) GetDriveWheelTypeByValue(value string) (key string) {
	for _, v := range dict.DriveWheelTypes {
		if strings.EqualFold(v.Value, value) {
			key = v.Key
			return
		}
	}
	return
}

func (dict *Dict) GetCarBodyStyleTypeByValue(value string) (key string) {
	for _, v := range dict.CarBodyStyleTypes {
		if strings.EqualFold(v.Value, value) {
			key = v.Key
			return
		}
	}
	return
}

func (dict *Dict) GetSteeringTypeByValue(value string) (key string) {
	for _, v := range dict.SteeringTypes {
		if strings.EqualFold(v.Value, value) {
			key = v.Key
			return
		}
	}
	return
}

func (dict *Dict) GetAssessmentDocumentTypeByName(name string) (entity model.AssessmentDocumentType, err error) {
	for _, v := range dict.AssessmentDocumentTypes {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("AssessmentDocumentType must be one of that: %v", dict.AssessmentDocumentTypes))
	return
}

func (dict *Dict) GetAssessmentDocumentTypeByID(id uint) (entity model.AssessmentDocumentType, err error) {
	for _, v := range dict.AssessmentDocumentTypes {
		if v.ID == id {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("AssessmentDocumentType must be one of that: %v", dict.AssessmentDocumentTypes))
	return
}

func (dict *Dict) GetAssessmentPhotoTypeByName(name string) (entity model.AssessmentPhotoType, err error) {
	for _, v := range dict.AssessmentPhotoTypes {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("AssessmentPhotoType must be one of that: %v", dict.AssessmentPhotoTypes))
	return
}

func (dict *Dict) GetVinReportStatusByName(name string) (entity model.VinReportStatus, err error) {
	for _, v := range dict.VinReportStatuses {
		if strings.ToUpper(v.Name) == strings.ToUpper(name) {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("VinReportStatus must be one of that: %v", dict.VinReportStatuses))
	return
}

func (dict *Dict) GetVinReportStatusByID(id uint) (entity model.VinReportStatus, err error) {
	for _, v := range dict.VinReportStatuses {
		if v.ID == id {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("VinReportStatus must be one of that: %v", dict.VinReportStatuses))
	return
}

func (dict *Dict) GetRoleByID(id uint) (entity model.Role, err error) {
	for _, v := range dict.Roles {
		if v.ID == id {
			entity = v
			return
		}
	}

	err = errors.New(fmt.Sprintf("Role must be one of that: %v", dict.Roles))
	return
}