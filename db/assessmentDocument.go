package db

import (
	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/gorm/clause"
)

func (m *Manager) AddAssessmentDocument(document *model.AssessmentDocument) error {
	return m.DB.Create(document).Error
}

func (m *Manager) GetDocumentsByAssessmentID(assessmentID uint) (documents []model.AssessmentDocument, err error) {
	err = m.DB.Statement.Preload("AssessmentDocumentType").Preload("AssessmentDocumentFiles").Find(&documents, "assessment_id", assessmentID).Error
	return
}

func (m *Manager) DeleteAssessmentDocument(documentID uint) error {
	document := model.AssessmentDocument{ID: documentID}
	return m.DB.Select(clause.Associations).Delete(&document).Error
}
