package db

import (
	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/gorm"
)

func (m *Manager) AddAssessmentAdmission(assessmentID uint, assessmentAdmission *model.AssessmentAdmission) error {
	return m.DB.Model(&model.Assessment{ID: assessmentID}).Association("AssessmentAdmission").Replace(assessmentAdmission)
}

func (m *Manager) UpdateAssessmentAdmission(assessmentAdmission *model.AssessmentAdmission) error {
	return m.DB.Session(&gorm.Session{FullSaveAssociations: true}).Omit("assessment_price").Save(assessmentAdmission).Error
}

func (m *Manager) DeleteAssessmentAdmission(assessmentAdmission *model.AssessmentAdmission) error {
	return m.DB.Delete(assessmentAdmission).Error
}
