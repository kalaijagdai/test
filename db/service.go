package db

import (
	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/gorm"
)

func (m *Manager) CreateService(service *model.Service) error {
	return m.DB.Create(&service).Error
}

func (m *Manager) GetServices() (services []model.Service, err error) {
	err = m.DB.Preload("Dealership").Order("created_at").Find(&services).Error
	return
}

func (m *Manager) GetDealershipServices(dealershipID uint) (services []model.Service, err error) {
	err = m.DB.Statement.Preload("Dealership").Find(&services, "dealership_id", dealershipID).Error
	return
}

func (m *Manager) GetServiceByID(serviceID uint) (service model.Service, err error) {
	err = m.DB.Preload("Dealership").First(&service, serviceID).Error
	return
}

func (m *Manager) UpdateService(service *model.Service) error {
	return m.DB.Transaction(func(tx *gorm.DB) error {
		if service.Disabled == false {
			if err := tx.Model(service).Updates(map[string]interface{}{"Disabled": service.Disabled}).Error; err != nil {
				return err
			}
		}

		if err := tx.Session(&gorm.Session{FullSaveAssociations: true}).Updates(service).Error; err != nil {
			return err
		}

		return nil
	})
}

func (m *Manager) DeleteService(serviceID uint) error {
	return m.DB.Delete(&model.Service{}, serviceID).Error
}

