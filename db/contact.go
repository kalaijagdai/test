package db

import (
	"gitlab.com/tradeincar/backend/api/model"
)

func (m *Manager) CreateContact(contact *model.Contact) error {
	return m.DB.Create(contact).Error
}

func (m *Manager) GetContactByID(contact_id uint) (contact model.Contact, err error) {
	err = m.DB.First(&contact, contact_id).Error
	return
}

func (m *Manager) GetContacts() (contacts []model.Contact, err error) {
	err = m.DB.Preload("Deals").Preload("Communication").Order("created_at").Find(&contacts).Error
	return
}
