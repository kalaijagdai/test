package db

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/utils"
	"gorm.io/gorm"
)

func (m *Manager) CreateCar(car *model.Car) error {
	return m.DB.Create(car).Error
}

func (m *Manager) GetCarByID(car_id uint) (car model.Car, err error) {
	err = m.DB.First(&car, car_id).Error
	return
}

func (m *Manager) GetCarByVIN(VIN string) (car model.Car, err error) {
	err = m.DB.Preload("CarModel.CarMark").Preload("CarModification.CarSerie.CarGeneration").Preload("CarCustomCharacteristicValues.CarCharacteristic").Preload("CarCustomCharacteristicValues.CarCharacteristicValue").First(&car, "vin = ?", VIN).Error
	return
}

func (m *Manager) GetCars(queries map[string]interface{}, user model.User, limit, offset uint64) (cars []scheme.CarInResponse, err error) {
	sql := `
		SELECT
			distinct ON (assessment.id)
			assessment.id "AssessmentID",
			assessment.created_at "CreatedAt",
			assessment_status."description" "AssessmentStatusDescription",
			assessment_stage."description" "AssessmentStageDescription",
			assessment_status_log."reason" "AssessmentStatusReason",
			assessment_types."description" "AssessmentTypeDescription",
			car.id "CarID",
			fav_photo.url "FavoritePhoto",
			assessment_pricing.assessment_price "Price",
			assessment_pricing.planned_selling_price "PlannedSellingPrice",
			car_info.car_body_color "CarBodyColor",
			car_info.mileage "Mileage",
			car_info.mileage_type "MileageType",
			car_marks."name" "CarMark",
			car_models.id "CarModelID",
			car_models."name" "CarModel",
			car_modifications."name" "CarModification",
			car.vin "VIN",
			car.year "Year",
			engineType.value "EngineTypeName",
			engineVolume.value "EngineVolume",
			enginePower.value "EnginePower",
			driveWheelType.value "DriveWheelTypeName",
			kppType.value "KppTypeName",
			carBodyStyleType.value "CarBodyStyleTypeName",
			doorCount.value "DoorCount",
			sale.sale_at "SaleAt"
		FROM assessment
		LEFT JOIN assessment_pricing ON assessment_pricing.assessment_id = assessment.id
		LEFT JOIN assessment_status ON assessment_status.id = assessment.assessment_status_id
		LEFT JOIN assessment_stage ON assessment_stage.id = assessment_status.assessment_stage_id
		LEFT JOIN assessment_types ON assessment_types.id = assessment.assessment_type_id
		LEFT JOIN client ON client.id = assessment.client_id
		JOIN car ON car.id = assessment.car_id
		JOIN car_models ON car_models.id = car.car_model_id
		JOIN car_marks ON car_marks.id = car_models.car_mark_id
		LEFT JOIN car_modifications ON car_modifications.id = car.car_modification_id
		JOIN car_info ON car_info.id = assessment.car_info_id
		LEFT JOIN sale ON sale.assessment_id = assessment.id
		LEFT JOIN (
			SELECT
				char_val.value,
				char_val."key",
				custom_char_val.car_characteristic_id,
				custom_char_val.car_id
			FROM car_custom_characteristic_values custom_char_val
			LEFT JOIN car_characteristic_values char_val ON char_val.id = custom_char_val.car_characteristic_value_id
			WHERE custom_char_val.car_characteristic_id = 12
		) as engineType ON engineType.car_id = car.id
		LEFT JOIN (
			SELECT * FROM car_custom_characteristic_values WHERE car_characteristic_id = 13
		) as engineVolume ON engineVolume.car_id = car.id
		LEFT JOIN (
			SELECT * FROM car_custom_characteristic_values WHERE car_characteristic_id = 14
		) as enginePower ON enginePower.car_id = car.id
		LEFT JOIN (
			SELECT
				char_val.value,
				char_val."key",
				custom_char_val.car_characteristic_id,
				custom_char_val.car_id
				FROM car_custom_characteristic_values custom_char_val
			LEFT JOIN car_characteristic_values char_val ON char_val.id = custom_char_val.car_characteristic_value_id
			WHERE custom_char_val.car_characteristic_id = 24
		) as kppType ON kppType.car_id = car.id
		LEFT JOIN (
			SELECT
				char_val.value,
				char_val."key",
				custom_char_val.car_characteristic_id,
				custom_char_val.car_id
			FROM car_custom_characteristic_values custom_char_val
			LEFT JOIN car_characteristic_values char_val ON char_val.id = custom_char_val.car_characteristic_value_id
			WHERE char_val.car_characteristic_id = 27
		) as driveWheelType ON driveWheelType.car_id = car.id
		LEFT JOIN (
			SELECT
				char_val.value,
				char_val."key",
				custom_char_val.car_characteristic_id,
				custom_char_val.car_id
				FROM car_custom_characteristic_values custom_char_val
			LEFT JOIN car_characteristic_values char_val ON char_val.id = custom_char_val.car_characteristic_value_id
			WHERE custom_char_val.car_characteristic_id = 2
		) as carBodyStyleType ON carBodyStyleType.car_id = car.id
		LEFT JOIN (
			SELECT * FROM car_custom_characteristic_values custom_char_val WHERE car_characteristic_id = 3
		) as doorCount ON doorCount.car_id = car.id
		LEFT JOIN (
			SELECT distinct on (assessment_id) * FROM (
				SELECT
					*
				FROM assessment_photos
				order by assessment_id, is_favorite desc
			) ap
		) as fav_photo ON fav_photo.assessment_id = assessment.id
	LEFT JOIN (
		SELECT reason, assessment_id
		FROM assessment_status_log
		WHERE id in (SELECT max(id) FROM assessment_status_log GROUP BY assessment_id)
		)
	as assessment_status_log ON assessment_status_log.assessment_id = assessment.id
	`

	var conditions []string

	var regexSQLProtector = regexp.MustCompile(utils.SQLInjectPattern)

	for key, val := range queries {
		var strFmt string
		arrayOfString := strings.Split(val.(string), ",")
		columnName := toColumnNameFmt(key)
		if columnName == "" {
			continue
		}

		if len(arrayOfString) >= 2 {
			arrayOfString[0] = regexSQLProtector.ReplaceAllLiteralString(arrayOfString[0], "")
			arrayOfString[1] = regexSQLProtector.ReplaceAllLiteralString(arrayOfString[1], "")
			strFmt = fmt.Sprintf("%s BETWEEN '%s' AND '%s'", columnName, arrayOfString[0], arrayOfString[1])
		} else {
			queries[key] = regexSQLProtector.ReplaceAllLiteralString(arrayOfString[0], "")
			strFmt = fmt.Sprintf("%s = @%s", columnName, key)
		}
		conditions = append(conditions, strFmt)
	}

	if onlyMy, ok := queries["onlyMy"]; ok {
		if isOnlyMy, _ := strconv.ParseBool(onlyMy.(string)); isOnlyMy == true {
			queries["userID"] = user.ID
			conditions = append(conditions, "assessment.user_id = @userID")
		}
	}

	if statusKeys, ok := queries["assessmentStatusKeys"]; ok {
		queries["assessmentStatusKeys"] = strings.Split(statusKeys.(string), ",")
		conditions = append(conditions, "assessment_status.name in @assessmentStatusKeys")
	}

	var dealerships []uint
	for _, value := range user.Dealerships {
		dealerships = append(dealerships, value.ID)
	}

	if user.Role.Name != "OWNER" && len(dealerships) > 0 {
		queries["dealerships"] = dealerships
		conditions = append(conditions, "assessment.dealership_id IN @dealerships")
	}

	if val, ok := queries["vin"]; ok {
		queries["vin"] = fmt.Sprintf("%%%s%%", val)
		strFmt := `LOWER(car.vin) LIKE LOWER(@vin)`
		conditions = append(conditions, strFmt)
	}

	if len(conditions) > 0 {
		sql += fmt.Sprintf("WHERE %s ", strings.Join(conditions, " AND "))
	}

	sql += fmt.Sprintf("ORDER BY assessment.id desc LIMIT %d OFFSET %d", limit, offset)
	// sql = fmt.Sprintf(`SELECT * FROM (%s) assessments ORDER BY "CreatedAt" DESC`, sql)

	err = m.DB.Raw(sql, queries).Find(&cars).Error

	return
}

// update exact fields in Car
func (m *Manager) UpdateCar(carID uint, data map[string]interface{}) error {
	return m.DB.First(&model.Car{}, carID).Updates(data).Error
}

func (m *Manager) GetValuesByCharacteristicID(characteristicID uint) (CarCharacteristicValue []model.CarCharacteristicValue, err error) {
	err = m.DB.Order("id").Find(&CarCharacteristicValue, "is_from_basebuy = false AND car_characteristic_id = ?", characteristicID).Error
	return
}

func (m *Manager) GetEquipmentsByModelID(carModelID uint) (carEquipment []model.CarEquipment, err error) {
	err = m.DB.Joins("JOIN car_modifications ON car_modifications.id = car_equipments.car_modification_id").Find(&carEquipment, "car_model_id = ?", carModelID).Error
	return
}

func (m *Manager) GetYearBeginGenerationByModelID(carModelID uint) (carEquipment model.CarGeneration, err error) {
	err = m.DB.Raw(`
		SELECT
			year_begin as "YearBegin"
		FROM car_generations
		WHERE car_model_id = ? AND year_begin IS NOT NULL
		ORDER BY year_begin asc LIMIT 1
	`, carModelID).First(&carEquipment).Error
	return
}

func (m *Manager) GetYearEndGenerationByModelID(carModelID uint) (carEquipment model.CarGeneration, err error) {
	err = m.DB.Raw(`
		SELECT
			year_end as "YearEnd"
		FROM car_generations
		WHERE car_model_id = ? AND year_end IS NOT NULL
		ORDER BY year_end desc LIMIT 1
	`, carModelID).First(&carEquipment).Error
	return
}

func (m *Manager) GetGenerationsByModelIDAndYear(carModelID uint, year string) (carGeneration []model.CarGeneration, err error) {
	values := map[string]interface{}{
		"carModelID": carModelID,
		"year":       year,
	}
	conditions := "car_model_id = @carModelID"
	if year != "" {
		conditions += " AND (EXTRACT(year from year_begin) <= @year AND EXTRACT(year from year_end) >= @year)"
	}
	err = m.DB.Preload("CarSeries").Preload("CarSeries.CarModifications").Order("year_begin").Find(&carGeneration, conditions, values).Error
	return
}

func (m *Manager) GetModificationByID(id uint) (carModifications model.CarModification, err error) {
	db := m.DB
	db = db.Preload("CarCharacteristicValues", func(db *gorm.DB) *gorm.DB {
		standartCarCharacteristicIds := []uint{12, 13, 14, 24, 27, 2, 3, 53}
		return db.Order("id").Where("car_characteristic_id IN ?", standartCarCharacteristicIds).Joins("CarCharacteristic")
	})
	err = db.First(&carModifications, "id = ?", id).Error
	return
}

func (m *Manager) CreateCarInfo(carInfo *model.CarInfo) error {
	return m.DB.Create(carInfo).Error
}

func toColumnNameFmt(name string) (colName string) {
	switch name {
	case "carMark":
		colName = "car_marks.name"
	case "carModel":
		colName = "car_models.name"
	case "price":
		colName = "assessment_pricing.assessment_price"
	case "engineType":
		colName = "engineType.key"
	case "engineVolume":
		colName = "COALESCE(engineVolume.value, '0')::integer"
	case "enginePower":
		colName = "COALESCE(enginePower.value, '0')::integer"
	case "kppType":
		colName = "kppType.key"
	case "driveWheelType":
		colName = "driveWheelType.key"
	case "carBodyStyleType":
		colName = "carBodyStyleType.key"
	case "carBodyColor":
		colName = "car_info.car_body_color"
	case "year":
		colName = "EXTRACT(year from car.year)"
	case "mileage":
		colName = "car_info.mileage"
	case "assessmentStage":
		colName = "assessment_stage.name"
	case "assessmentStatus":
		colName = "assessment_status.name"
	case "assessmentType":
		colName = "assessment_types.name"
	case "dealership":
		colName = "assessment.dealership_id"
	case "createdAt":
		colName = "assessment.created_at"
	// case "inspectionAt":
	// 	colName = ""
	case "saleAt":
		colName = "sale.sale_at"
	// case "daysInWarehouse":
	// 	colName = "sale.sale_at"
	default:
		colName = ""
	}
	return
}
