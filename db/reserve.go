package db

import (
	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/gorm/clause"
)

func (m *Manager) UpdateReserve(reserve *model.Reserve) (err error) {
	return m.DB.Model(&reserve).Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(&reserve).Error
}

func (m *Manager) GetActualReserveRequestsByAssessmentID(assessmentID uint, daysOnReserve uint) (reserves []model.Reserve, err error) {
	err = m.DB.Preload("Client").Where("assessment_id = ? AND DATE_PART('day', now() - created_at) <= ?", assessmentID, daysOnReserve).Find(&reserves).Error
	return
}