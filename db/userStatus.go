package db

import (
	"gitlab.com/tradeincar/backend/api/model"
)

func (m *Manager) GetUserStatusByName(name string) (userStatus model.UserStatus, err error) {
	err = m.DB.Where("name = ?", name).First(&userStatus).Error
	return
}

func (m *Manager) GetAllUserStatuses() (statuses []model.UserStatus, err error) {
	err = m.DB.Order("created_at").Find(&statuses).Error
	return
}
