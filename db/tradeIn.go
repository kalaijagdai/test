package db

import (
	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/gorm/clause"
)

func (m *Manager) UpdateTradeIn(tradeIn *model.TradeIn) (err error) {
	return m.DB.Model(&tradeIn).Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(&tradeIn).Error
}
