package db

import (
	"gitlab.com/tradeincar/backend/api/model"
)

// interface for enum
type DictEnum interface {
	GetName() (string, error)
}

// load enum from db
func (m *Manager) GetReferrals() (referrals []model.Referral) {
	m.DB.Find(&referrals)
	return
}

func (m *Manager) GetClientTypes() (clientTypes []model.ClientType) {
	m.DB.Find(&clientTypes)
	return
}

func (m *Manager) GetGenders() (genders []model.Gender) {
	m.DB.Find(&genders)
	return
}

func (m *Manager) GetDealershipStatuses() (dealershipStatuses []model.DealershipStatus) {
	m.DB.Find(&dealershipStatuses)
	return
}

func (m *Manager) GetUserStatuses() (userStatuses []model.UserStatus) {
	m.DB.Find(&userStatuses)
	return
}

func (m *Manager) GetDealGoals() (dealGoals []model.DealGoal) {
	m.DB.Find(&dealGoals)
	return
}

func (m *Manager) GetDealStatuses() (dealStatuses []model.DealStatus) {
	m.DB.Find(&dealStatuses)
	return
}

func (m *Manager) GetCommunications() (communications []model.Communication) {
	m.DB.Find(&communications)
	return
}

func (m *Manager) GetCarInterests() (carInterests []model.CarInterest) {
	m.DB.Find(&carInterests)
	return
}

func (m *Manager) GetRelevances() (relevances []model.Relevance) {
	m.DB.Find(&relevances)
	return
}

func (m *Manager) GetCarStatuses() (carStatuses []model.CarStatus) {
	m.DB.Find(&carStatuses)
	return
}

func (m *Manager) GetRoles() (roles []model.Role) {
	m.DB.Find(&roles)
	return
}

func (m *Manager) GetAssessmentStatuses() (assessmentStatuses []model.AssessmentStatus) {
	m.DB.Preload("AssessmentStage").Find(&assessmentStatuses)
	return
}

func (m *Manager) GetAssessmentStages() (assessmentStages []model.AssessmentStage) {
	m.DB.Find(&assessmentStages)
	return
}

func (m *Manager) GetTypesByCarCharacteristicID(characteristicTypeID uint) (characteristicValues []model.CarCharacteristicValue) {
	characteristicValues, _ = m.GetValuesByCharacteristicID(characteristicTypeID)
	return
}

func (m *Manager) GetAssessmentDocumentTypes() (assessmentDocumentTypes []model.AssessmentDocumentType) {
	m.DB.Find(&assessmentDocumentTypes)
	return
}

func (m *Manager) GetAssessmentPhotoTypes() (assessmentPhotoTypes []model.AssessmentPhotoType) {
	m.DB.Find(&assessmentPhotoTypes)
	return
}

func (m *Manager) GetVinReportStatuses() (assessmentStatuses []model.VinReportStatus) {
	m.DB.Find(&assessmentStatuses)
	return
}