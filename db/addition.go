package db

import (
	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/gorm"
)

func (m *Manager) CreateAddition(addition *model.Addition) error {
	return m.DB.Create(&addition).Error
}

func (m *Manager) GetAdditions() (additions []model.Addition, err error) {
	err = m.DB.Preload("Dealership").Order("created_at").Find(&additions).Error
	return
}

func (m *Manager) GetDealershipAdditions(dealershipID uint) (additions []model.Addition, err error) {
	err = m.DB.Statement.Preload("Dealership").Find(&additions, "dealership_id", dealershipID).Error
	return
}

func (m *Manager) GetAdditionByID(additionID uint) (addition model.Addition, err error) {
	err = m.DB.Preload("Dealership").First(&addition, additionID).Error
	return
}

func (m *Manager) UpdateAddition(addition *model.Addition) error {
	return m.DB.Transaction(func(tx *gorm.DB) error {
		if addition.Disabled == false {
			if err := tx.Model(addition).Updates(map[string]interface{}{"Disabled": addition.Disabled}).Error; err != nil {
				return err
			}
		}

		if err := tx.Session(&gorm.Session{FullSaveAssociations: true}).Updates(addition).Error; err != nil {
			return err
		}

		return nil
	})
}

func (m *Manager) DeleteAddition(additionID uint) error {
	return m.DB.Delete(&model.Addition{}, additionID).Error
}
