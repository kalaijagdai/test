package db

import (
	"fmt"

	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/utils"
	"gorm.io/gorm"
)

func (m *Manager) CreateAssessment(assessment *model.Assessment) error {
	return m.DB.Create(&assessment).Error
}

func (m *Manager) GetAssessments() (assessments []model.Assessment, err error) {
	err = m.DB.Order("created_at").Preload("AssessmentAdmission.Client").Preload("AssessmentStatus").Preload("Car").Preload("Client").Find(&assessments).Error
	return
}

func (m *Manager) GetAssessmentWithStatusByID(assessmentID uint) (assessment model.Assessment, err error) {
	err = m.DB.Joins("AssessmentStatus").First(&assessment, assessmentID).Error
	return
}

func (m *Manager) GetAssessmentByID(assessmentID uint) (assessment model.Assessment, err error) {
	db := m.DB
	db = db.Unscoped()
	db = db.Preload("AssessmentReport")
	db = db.Preload("AssessmentReport.AssessmentStatus.AssessmentStage")
	db = db.Preload("AssessmentAdmission.Client", utils.Unscopedizer())
	db = db.Preload("Car.CarModification.CarSerie.CarGeneration")
	db = db.Preload("Car.CarModel.CarMark")
	db = db.Preload("Car.CarCustomCharacteristicValues.CarCharacteristic")
	db = db.Preload("Car.CarCustomCharacteristicValues.CarCharacteristicValue")
	db = db.Joins("Client")
	db = db.Joins("CarInfo")
	db = db.Joins("AssessmentType")
	db = db.Preload("Dealership", utils.Unscopedizer())
	db = db.Preload("Dealership.Users", utils.Unscopedizer())
	db = db.Preload("AssessmentPricingHistory.AssessmentStatus.AssessmentStage")
	db = db.Preload("AssessmentPricing.AssessmentStatus.AssessmentStage")
	db = db.Preload("AssessmentStatus.AssessmentStage")
	db = db.Preload("AssessmentAdditions.AssessmentStatus.AssessmentStage")
	db = db.Preload("AssessmentServices.AssessmentStatus.AssessmentStage")
	db = db.Preload("AssessmentGear.SecurityGear")
	db = db.Preload("AssessmentGear.ParktronicGear")
	db = db.Preload("AssessmentGear.AirbagGear")
	db = db.Preload("AssessmentGear.AssistiveSystemGear")
	db = db.Preload("AssessmentGear.SalonGear")
	db = db.Preload("AssessmentGear.HeatedSeatGear")
	db = db.Preload("AssessmentGear.SeatVentilationGear")
	db = db.Preload("AssessmentGear.ExteriorGear")
	db = db.Preload("AssessmentGear.ReviewGear")
	db = db.Preload("AssessmentGear.MirrorGear")
	db = db.Preload("AssessmentGear.ElectricHeaterGear")
	db = db.Preload("AssessmentGear.MatSetGear")
	db = db.Preload("AssessmentGear.ComfortGear")
	db = db.Preload("AssessmentGear.SteeringWheelGear")
	db = db.Preload("AssessmentGear.MultimediaGear")
	db = db.Preload("AssessmentGear.RadioGear")
	db = db.Preload("AssessmentGear.TheftProtectionGear")
	db = db.Preload("AssessmentGear.TheftProtectionGear")
	db = db.Preload("AssessmentPhotos", func(db *gorm.DB) *gorm.DB {
		return db.Order("is_favorite desc")
	})
	db = db.Preload("AssessmentPhotos.AssessmentPhotoType")
	db = db.Preload("AssessmentDocuments.AssessmentDocumentType")
	db = db.Preload("AssessmentDocuments.AssessmentDocumentFiles")
	db = db.Preload("TradeIn.Car.CarModel.CarMark")
	db = db.Preload("TradeIn.Car.CarModification")
	db = db.Preload("TradeIn.Client", utils.Unscopedizer())
	db = db.Preload("TradeIn.User", utils.Unscopedizer())
	db = db.Preload("TradeIn.CarModel.CarMark")
	db = db.Preload("Sale.SalesChannel")
	db = db.Preload("Sale.Client", utils.Unscopedizer())
	db = db.Preload("Sale.User", utils.Unscopedizer())
	db = db.Preload("Reserves", func(db *gorm.DB) *gorm.DB {
		return db.Order("created_at desc")
	})
	db = db.Preload("Reserves.Client", utils.Unscopedizer())
	db = db.Preload("VinReport", func(db *gorm.DB) *gorm.DB {
		return db.Select(`vin_report.id`, `"Status".id AS Status__id`, `"Status".name AS Status__name`, "assessment_id", "ownership_count", "has_restrict", "is_search", "dtp_count", "created_at", "updated_at").Joins("Status").Order("created_at desc")
	})
	err = db.First(&assessment, assessmentID).Error

	return
}

func (m *Manager) UpdateAssessment(assesment *model.Assessment) (err error) {
	return m.DB.Model(&assesment).Updates(&assesment).Error
}

func (m *Manager) UpdateAssessmentStatus(assessmentToUpdate *model.Assessment, reason string, fromStatus uint) (err error) {
	return m.DB.Transaction(func(tx *gorm.DB) error {
		if err := m.DB.Model(&assessmentToUpdate).Updates(&assessmentToUpdate).Error; err != nil {
			return err
		}

		AssessmentStatusLogToCreate := model.AssessmentStatusLog{
			AssessmentID:          assessmentToUpdate.ID,
			SrcAssessmentStatusID: fromStatus,
			DstAssessmentStatusID: assessmentToUpdate.AssessmentStatusID,
			Reason:                reason,
		}
		if err := m.DB.Create(&AssessmentStatusLogToCreate).Error; err != nil {
			return err
		}

		return nil
	})
}

func (m *Manager) UpdateAssessmentStatusBulk(assessmentIDs []uint, reason string, fromStatus uint, toStatus uint) (err error) {
	return m.DB.Transaction(func(tx *gorm.DB) error {
		fmt.Println(assessmentIDs)
		if err := m.DB.Model(&model.Assessment{}).Where("id IN ?", assessmentIDs).Update("assessment_status_id", toStatus).Error; err != nil {
			return err
		}

		// save logs
		var assessmentStatusLogs []model.AssessmentStatusLog
		for _, assessmentID := range assessmentIDs {
			assessmentStatusLog := model.AssessmentStatusLog{
				AssessmentID:          assessmentID,
				SrcAssessmentStatusID: fromStatus,
				DstAssessmentStatusID: toStatus,
				Reason:                reason,
			}
			assessmentStatusLogs = append(assessmentStatusLogs, assessmentStatusLog)
		}

		if err := m.DB.Create(&assessmentStatusLogs).Error; err != nil {
			return err
		}

		return nil
	})
}

func (m *Manager) ReplaceAssessmentPricing(assessmentID uint, assessmentPricing []model.AssessmentPricing) error {
	return m.DB.Model(&model.Assessment{ID: assessmentID}).Association("AssessmentPricing").Replace(assessmentPricing)
}

func (m *Manager) ReplaceAssessmentAdditions(assessmentID uint, assessmentAdditions []model.AssessmentAddition) error {
	return m.DB.Model(&model.Assessment{ID: assessmentID}).Association("AssessmentAdditions").Replace(assessmentAdditions)
}

func (m *Manager) ReplaceAssessmentServices(assessmentID uint, assessmentServices []model.AssessmentService) error {
	return m.DB.Model(&model.Assessment{ID: assessmentID}).Association("AssessmentServices").Replace(assessmentServices)
}

//func (m *Manager) UpdateAssessment(assessment *model.Assessment) error {
//	return m.DB.Transaction(func(tx *gorm.DB) error {
//		if assessment.Disabled == false {
//			if err := tx.Model(assessment).Updates(map[string]interface{}{"Disabled": assessment.Disabled}).Error; err != nil {
//				return err
//			}
//		}
//
//		if err := tx.Session(&gorm.Session{FullSaveAssociations: true}).Updates(assessment).Error; err != nil {
//			return err
//		}
//
//		return nil
//	})
//}

func (m *Manager) DeleteAssessment(assessmentID uint) error {
	return m.DB.Delete(&model.Assessment{}, assessmentID).Error
}

func (m *Manager) UpdateAssessmentCarInfo(carInfo *model.CarInfo) (err error) {
	return m.DB.Model(&carInfo).Updates(&carInfo).Error
}

func (m *Manager) CreateAssessmentPricing(price *model.AssessmentPricing) error {
	return m.DB.Create(&price).Error
}

func (m *Manager) UpdateAssessmentPricing(price *model.AssessmentPricing) error {
	return m.DB.Model(&price).Updates(&price).Error
}

func (m *Manager) CreateAssessmentPricingHistory(price *model.AssessmentPricingHistory) error {
	return m.DB.Create(&price).Error
}

func (m *Manager) GetLastAssessmentPricingByIdAndStatusId(id uint, statusId uint) (pricing model.AssessmentPricing, err error) {
	err = m.DB.Where("assessment_id = ? and assessment_status_id = ?", id, statusId).Order("created_at desc").First(&pricing).Error
	return
}

func (m *Manager) GetPricingByAssessmentID(assessmentID uint) (pricing []model.AssessmentPricing, err error) {
	err = m.DB.Statement.Preload("AssessmentStatus.AssessmentStage").Find(&pricing, "assessment_id", assessmentID).Error
	return
}

func (m *Manager) GetAssessmentPricingHistory(assessmentID uint) (pricingHistory []model.AssessmentPricingHistory, err error) {
	err = m.DB.Statement.Preload("AssessmentStatus.AssessmentStage").Find(&pricingHistory, "assessment_id", assessmentID).Error
	return
}

func (m *Manager) GetAssessmentStatusByID(assessmentStatusID uint) (assessmentStatus model.AssessmentStatus, err error) {
	err = m.DB.Preload("AssessmentStage").First(&assessmentStatus, assessmentStatusID).Error
	return
}
