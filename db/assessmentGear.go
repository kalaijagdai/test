package db

import (
	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/gorm"
)

func (m *Manager) AddAssessmentGear(assessmentID uint, assessmentGear *model.AssessmentGear) error {
	return m.DB.Model(&model.Assessment{ID: assessmentID}).Association("AssessmentGear").Replace(assessmentGear)
}

func (m *Manager) UpdateAssessmentGear(assessmentGear *model.AssessmentGear) error {
	return m.DB.Session(&gorm.Session{FullSaveAssociations: true}).Save(assessmentGear).Error
}

func (m *Manager) DeleteAssessmentGear(assessmentGear *model.AssessmentGear) error {
	return m.DB.Delete(assessmentGear).Error
}
