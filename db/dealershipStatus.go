package db

import (
	"gitlab.com/tradeincar/backend/api/model"
)

func (m *Manager) GetDealershipStatusByName(name string) (dealershipStatus model.DealershipStatus, err error) {
	err = m.DB.Where("name = ?", name).First(&dealershipStatus).Error
	return
}

func (m *Manager) GetAllDealershipStatuses() (statuses []model.DealershipStatus, err error) {
	err = m.DB.Order("created_at").Find(&statuses).Error
	return
}
