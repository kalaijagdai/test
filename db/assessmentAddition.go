package db

import (
	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/gorm"
)

func (m *Manager) AddAssessmentAddition(assessmentID uint, assessmentAddition *model.AssessmentAddition) error {
	return m.DB.First(&model.Assessment{}, assessmentID).Association("AssessmentAdditions").Append(assessmentAddition)
}

func (m *Manager) GetAssessmentAdditions() (assessmentAdditions []model.AssessmentAddition, err error) {
	err = m.DB.Order("created_at").Find(&assessmentAdditions).Error
	return
}

func (m *Manager) GetAssessmentAdditionByID(assessmentAdditionID uint) (assessmentAddition model.AssessmentAddition, err error) {
	err = m.DB.First(&assessmentAddition, assessmentAdditionID).Error
	return
}

func (m *Manager) UpdateAssessmentAddition(assessmentAddition *model.AssessmentAddition) error {
	return m.DB.Transaction(func(tx *gorm.DB) error {
		if assessmentAddition.Visible == false || assessmentAddition.ByCompany == false {
			if err := tx.Model(assessmentAddition).Updates(map[string]interface{}{"ByCompany": assessmentAddition.ByCompany, "Visible": assessmentAddition.Visible}).Error; err != nil {
				return err
			}
		}

		if err := tx.Session(&gorm.Session{FullSaveAssociations: true}).Updates(assessmentAddition).Error; err != nil {
			return err
		}

		return nil
	})
}

func (m *Manager) DeleteAssessmentAddition(assessmentAddition *model.AssessmentAddition) error {
	return m.DB.Delete(assessmentAddition).Error
}
