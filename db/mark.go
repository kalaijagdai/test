package db

import (
	"gitlab.com/tradeincar/backend/api/model"
)

func (m *Manager) CreateMark(mark *model.CarMark) error {
	return m.DB.Create(mark).Error
}

func (m *Manager) GetAllMarks() (marks []model.CarMark, err error) {
	err = m.DB.Order("id").Find(&marks).Error
	return
}

func (m *Manager) UpdateMark(mark *model.CarMark) error {
	return m.DB.Model(&mark).Updates(&mark).Error
}
