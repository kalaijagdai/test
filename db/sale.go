package db

import (
	"gitlab.com/tradeincar/backend/api/model"
)

func (m *Manager) UpdateSale(sale *model.Sale) (err error) {
	//err = m.DB.Model(&sale).Clauses(clause.OnConflict{
	//	UpdateAll: true,
	//}).Create(&sale).Error
	////if err !=err nil {
	//	return err
	////}
	if sale.ID > 0 {
		return m.DB.Model(&sale).Updates(&sale).Error
	} else {
		return m.DB.Model(&sale).Create(&sale).Error
	}
}
