package db

import (
	"fmt"

	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/gorm"
)

// Paginate function
func Paginate(offset, limit int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {

		switch {
		case limit > 100:
			limit = 100
		case limit <= 0:
			limit = 20
		}

		return db.Offset(offset).Limit(limit)
	}
}

// CreateReport service
func (m *Manager) CreateReport(report *model.Report) (*model.Report, error) {

	err := m.DB.Preload("Status").Create(report).Error

	if err != nil {
		return nil, err
	}

	return report, nil
}

// CreateSells service
func (m *Manager) CreateSellByReportID(sell *model.Sell) (*model.Sell, error) {

	err := m.DB.Create(sell).Error

	if err != nil {
		return nil, err
	}

	return sell, nil
}

// CreateReports service
func (m *Manager) CreateReports(reports []model.Report) ([]model.Report, error) {
	return reports, nil
}

// GetReports service
func (m *Manager) GetReports(query *model.QueryGetReports, roleID uint) (*[]model.Report, error) {
	reports := new([]model.Report)
	db := m.DB.Scopes(Paginate(query.Offset, query.Limit))
	if roleID != 1 {
		db.Where("user_id", query.UserID)
	}
	if query.DealershipID != 0 {
		db = db.Where("dealership_id = ?", query.DealershipID)
	}
	if query.FromDate != "" && query.ToDate != "" {
		db = db.Where("date(created_at) >= ? AND date(created_at) <= ?", query.FromDate, query.ToDate)
	}

	sql := `select distinct r.* from reports r
	join sells s on s.report_id=r.id
	order by r.id desc`
	err := db.Raw(sql).Preload("Sells").Preload("Status").Preload("DealershipData").Find(&reports).Error

	if err != nil {
		return nil, err
	}

	//err := db.Where("id in (select distinct report_id from sells)").Preload("Sells").Preload("Status").Order("id desc").Find(&reports).Error

	return reports, nil
}

func (m *Manager) AddReportLabelByUser(sell *model.ColumnMap) (*model.ColumnMap, error) {
	err := m.DB.Create(sell).Error

	if err != nil {
		return nil, err
	}

	return sell, nil
}

func (m *Manager) UpdateReportLabelByUser(userId int, sell *model.ColumnMap) (*model.ColumnMap, error) {
	err := m.DB.Where("user_id = ?", userId).Updates(&sell).Error

	if err != nil {
		return nil, err
	}

	return sell, nil
}

func (m *Manager) GetColumnMaps(userID int) (*model.ColumnMap, error) {
	columnMaps := new(model.ColumnMap)
	db := m.DB
	db = db.Where("user_id", userID)

	err := db.Find(&columnMaps).Error

	if err != nil {
		return nil, err
	}

	return columnMaps, nil
}

func (m *Manager) GetSellsByReportID(query *model.QueryGetReports, reportID int) (*[]model.Sell, int, error) {
	sells := new([]model.Sell)
	db := m.DB.Scopes(Paginate(query.Offset, query.Limit)).Unscoped()

	if query.VinCode != "" {
		db = db.Where("LOWER(sells.vin) LIKE LOWER(?)", "%"+query.VinCode+"%")
	}
	if query.CarMark != "" {
		db = db.Where("car_mark", query.CarMark)
	}
	if query.CarModel != "" {
		db = db.Where("car_model", query.CarModel)
	}

	err := db.Where("report_id", reportID).Order("id desc").Find(&sells).Error

	if err != nil {
		return nil, 0, err
	}

	var totalCount int
	err = m.DB.Select(`count(*)`).Where("report_id = ?", reportID).Table("sells").Find(&totalCount).Error

	if err != nil {
		return nil, 0, err
	}

	return sells, totalCount, nil
}

func (m *Manager) GetReportByID(reportID int) (*model.Report, error) {
	report := new(model.Report)
	db := m.DB
	err := db.Where("id", reportID).Preload("Status").Preload("DealershipData").Order("id desc").Find(&report).Error
	if err != nil {
		return nil, err
	}

	return report, nil
}

func (m *Manager) GetStatuses() (*[]model.AkabStatus, error) {
	status := new([]model.AkabStatus)
	db := m.DB
	err := db.Find(&status).Error
	if err != nil {
		return nil, err
	}

	return status, nil
}

func (m *Manager) GetSell(sellID int) (*model.Sell, error) {
	sell := new(model.Sell)

	err := m.DB.Where("id", sellID).Find(&sell).Error

	if err != nil {
		return nil, err
	}

	return sell, nil
}

// CreateSellUpdateHistory service
func (m *Manager) CreateSellUpdateHistory(sell *model.SellsUpdateHistory) (*model.SellsUpdateHistory, error) {

	err := m.DB.Create(sell).Error

	if err != nil {
		return nil, err
	}

	return sell, nil
}

func (m *Manager) UpdateSell(sellID int, sell *model.Sell) (*model.Sell, error) {
	err := m.DB.Where("id", sellID).Updates(&sell).Error

	if err != nil {
		return nil, err
	}

	return sell, nil
}

func (m *Manager) DeleteSell(sellID int) error {
	sell := model.Sell{ID: uint(sellID)}

	err := m.DB.First(&model.Sell{}, sellID).Delete(&sell).Error

	if err != nil {
		return err
	}

	return nil
}

func (m *Manager) GetTopModels(query *model.QueryGetReportsInfoGraph) ([]model.TopSells, error) {
	topSells := []model.TopSells{}

	result := m.DB.Raw(`select one.car_mark as mark, one.car_model model, one.one_c as amount, one.one_c/coalesce(NULLIF(two.two_c, 0), coalesce(NULLIF(one.one_c, 0))) as ratio from
	(select distinct p.car_mark, p.car_model, t.one_c
	from (
	select car_model, cast(count(price) as float) as one_c from public.sells where date_of_sale LIKE ? and selling_distance='РК' group by car_model
	) as t join public.sells p on p.car_model=t.car_model
	 order by t.one_c desc) as one
	join (select distinct p.car_mark, p.car_model, t.two_c
	from (
	select car_model, cast(count(price) as float) as two_c from public.sells where date_of_sale LIKE ? and selling_distance='РК' group by car_model
	) as t join public.sells p on p.car_model=t.car_model
	 order by t.two_c desc) as two
	on one.car_mark=two.car_mark and one.car_model=two.car_model order by one.one_c desc limit 10`, query.Format()+"%", query.FormatLast()+"%")

	if result.Error != nil {
		return nil, result.Error
	}

	rows, err := result.Rows()

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		sell := model.TopSells{}
		rows.Scan(&sell.Mark, &sell.Model, &sell.Amount, &sell.Ratio)
		topSells = append(topSells, sell)
	}

	return topSells, nil
}

func (m *Manager) GetTopMarks(query *model.QueryGetReportsInfoGraph) ([]model.TopSells, error) {
	topSells := []model.TopSells{}
	fmt.Println(query.Format(), query.FormatLast())
	result := m.DB.Raw(`select one.car_mark as mark, one.one_c as amount, one.one_c/coalesce(NULLIF(two.two_c, 0), coalesce(NULLIF(one.one_c, 0))) as ratio from
	(select car_mark, cast(count(price) as float) as one_c from public.sells where date_of_sale LIKE ? and selling_distance='РК' group by car_mark order by one_c) as one
	join (select car_mark, cast(count(price) as float) as two_c from public.sells where date_of_sale LIKE ? and selling_distance='РК' group by car_mark) as two
	on one.car_mark=two.car_mark order by one.one_c desc limit 5`, query.Format()+"%", query.FormatLast()+"%")

	if result.Error != nil {
		return nil, result.Error
	}

	rows, err := result.Rows()

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		sell := model.TopSells{}
		rows.Scan(&sell.Mark, &sell.Amount, &sell.Ratio)
		topSells = append(topSells, sell)
	}

	return topSells, nil
}

func (m *Manager) GetTotalSells(query *model.QueryGetReportsInfoGraph) (*model.TopSells, error) {
	topSells := model.TopSells{}
	result := m.DB.Raw(`select one.one_c as amount, one.one_c/coalesce(NULLIF(two.two_c, 0), coalesce(NULLIF(one.one_c, 0))) as ratio, (one.one_c - two.two_c) as difference from
	(select cast(count(id) as float) as one_c from public.sells where date_of_sale LIKE ? and selling_distance='РК') as one,
	(select cast(count(id) as float) as two_c from public.sells where date_of_sale LIKE ? and selling_distance='РК') as two`, query.Format()+"%", query.FormatLast()+"%")

	if result.Error != nil {
		return nil, result.Error
	}

	rows, err := result.Rows()

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		rows.Scan(&topSells.Amount, &topSells.Ratio, &topSells.Difference)
	}

	return &topSells, nil
}

func (m *Manager) UpdateReportStatusByReport(reportID int, status int) (*model.Report, error) {

	report := new(model.Report)
	db := m.DB

	err := m.DB.Where("id = ?", reportID).Model(&report).Update("status_id", status).Error

	if err != nil {
		return nil, err
	}

	err = db.Where("id", reportID).Preload("Status").Order("id desc").Find(&report).Error

	if err != nil {
		return nil, err
	}

	return report, nil
}

// ErrorHiddenLog service
func (m *Manager) ErrorHiddenLog(log *model.AkabErrorHiddenLog) (*model.AkabErrorHiddenLog, error) {

	err := m.DB.Create(log).Error

	if err != nil {
		return nil, err
	}

	return log, nil
}

func (m *Manager) GetSalesAmountForDashboard(query *model.QueryGetReportsInfoGraph) (*model.TopSells, error) {

	salesAmount := model.TopSells{}

	err := m.DB.Select(`SUM(sells.price)`).Where("lower(date_of_sale) LIKE ?", query.Format()+"%").Table("sells").Find(&salesAmount.Amount).Error

	if err != nil {
		return nil, err
	}

	return &salesAmount, nil

}

func (m *Manager) GetLocalSalesRatioForDashboard(query *model.QueryGetReportsInfoGraph) (*model.TopSells, error) {

	salesAmount := model.TopSells{}

	err := m.DB.Select(`(select cast(count(*) as float)  from sells where (country_id = 97 or selling_distance='РК') and lower(date_of_sale) LIKE ?)/count(*)`, query.Format()+"%").Where("lower(date_of_sale) LIKE ?", query.Format()+"%").Table("sells").Find(&salesAmount.Ratio).Error

	if err != nil {
		return nil, err
	}

	return &salesAmount, nil

}
