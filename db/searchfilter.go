package db

import (
	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/gorm/clause"
)

func (m *Manager) CreateFilterCarByUser(car *model.FilterCarByUser) error {
	return m.DB.Table("FilterCarByUser").Create(&car).Error
}

// func (m *Manager) GetAllFilterCarByUser(car model.FilterCarByUser) ([]model.FilterCarByUser) {
// 	db := m.DB
// 	var filters []model.FilterCarByUser
// 	for _, f := range filters {
// 		if car.Mark.ID == f.MarkID {

// 		}
// 	}
// 	result := db.Where("userID = ?", user.ID).Find(&filters)
// 	return m.DB.Sele
// }

func (m *Manager) GetAllFilterCarByUser() (filterCars []model.FilterCarByUser, err error) {
	err = m.DB.Order("id").Find(&filterCars).Error
	return
}
func (m *Manager) CountFilterCarByUser(userID uint) (count int64, err error) {
	err = m.DB.Table("FilterCarByUser").Where("user_id = ?", userID).Count(&count).Error
	return
}

func (m *Manager) UpdateFilterCarByUser(filterCar *model.FilterCarByUser) error {
	return m.DB.Model(&filterCar).Updates(&filterCar).Error
}

func (m *Manager) DeleteFilterCarByUser(filterCar *model.FilterCarByUser) error {
	return m.DB.Select(clause.Associations).Delete(&filterCar).Error
}
