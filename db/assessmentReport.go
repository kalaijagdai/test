package db

import (
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/utils"
	"gorm.io/gorm"
)

func (m *Manager) CreateAssessmentReport(report *model.AssessmentReport) error {
	return m.DB.Create(&report).Error
}

func (m *Manager) GetAssessmentReportByID(reportID uint) (report model.AssessmentReport, err error) {
	err = m.DB.Unscoped().Preload("User", utils.Unscopedizer()).Preload("AssessmentStatus.AssessmentStage").Preload("AssessmentInspection.AssessmentInnerInspection.AssessmentInnerInspectionView").First(&report, reportID).Error
	return
}

func (m *Manager) UpdateAssessmentReport(assessmentReport *model.AssessmentReport) error {
	return m.DB.Session(&gorm.Session{FullSaveAssociations: true}).Updates(assessmentReport).Error
}
