package db

import (
	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/gorm/clause"
)

func (m *Manager) AddAssessmentPhoto(photo *model.AssessmentPhoto) error {
	return m.DB.Model(&photo).Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(&photo).Error
}

func (m *Manager) GetPhotosByAssessmentID(assessmentID uint) (photos []model.AssessmentPhoto, err error) {
	err = m.DB.Statement.Preload("AssessmentPhotoType").Find(&photos, "assessment_id", assessmentID).Error
	return
}

func (m *Manager) DeleteAssessmentPhoto(photoID uint) error {
	photo := model.AssessmentPhoto{ID: photoID}
	return m.DB.Select(clause.Associations).Delete(&photo).Error
}

func (m *Manager) UnfavoritePhotosByAssesmentID(assessmentID uint) error {
	return m.DB.Model(
		&model.AssessmentPhoto{},
	).Where(
		"assessment_id = ? AND is_favorite = true", assessmentID,
	).Updates(
		map[string]interface{}{"IsFavorite": false},
	).Error
}

func (m *Manager) UpdateAssessmentPhoto(photo *model.AssessmentPhoto) error {
	return m.DB.Model(&photo).Updates(&photo).Error
}

func (m *Manager) GetSametypePhotosByAssessmentID(assessmentID, assessment_photo_type_id uint) (photos model.AssessmentPhoto, err error) {
	err = m.DB.Joins("AssessmentPhotoType").First(&photos, "assessment_id = ? AND assessment_photo_type_id = ?", assessmentID, assessment_photo_type_id).Error
	return
}
