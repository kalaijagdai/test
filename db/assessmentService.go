package db

import (
	"fmt"
	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/gorm"
)

func (m *Manager) AddAssessmentService(assessmentID uint, assessmentService *model.AssessmentService) error {
	return m.DB.First(&model.Assessment{}, assessmentID).Association("AssessmentServices").Append(assessmentService)
}

func (m *Manager) GetAssessmentServices() (assessmentServices []model.AssessmentService, err error) {
	err = m.DB.Order("created_at").Find(&assessmentServices).Error
	return
}

func (m *Manager) GetAssessmentServiceByID(assessmentServiceID uint) (assessmentService model.AssessmentService, err error) {
	err = m.DB.First(&assessmentService, assessmentServiceID).Error
	return
}

func (m *Manager) UpdateAssessmentService(assessmentService *model.AssessmentService) error {
	return m.DB.Transaction(func(tx *gorm.DB) error {
		fmt.Println("HERE")
		if assessmentService.Visible == false || assessmentService.ByCompany == false {
			if err := tx.Model(assessmentService).Updates(map[string]interface{}{"ByCompany": assessmentService.ByCompany, "Visible": assessmentService.Visible}).Error; err != nil {
				return err
			}
		}

		if err := tx.Session(&gorm.Session{FullSaveAssociations: true}).Updates(assessmentService).Error; err != nil {
			return err
		}

		return nil
	})
}

func (m *Manager) DeleteAssessmentService(assessmentService *model.AssessmentService) error {
	return m.DB.Delete(assessmentService).Error
}
