package db

import (
	"gitlab.com/tradeincar/backend/api/model"
)

func (m *Manager) UpdateDealCar(dealID uint, carID uint, data map[string]interface{}) error {
	return m.DB.Model(&model.DealCar{}).Where("deal_id = ? AND car_id = ?", dealID, carID).Updates(data).Error
}
