package db

import (
	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/gorm"
)

func (m *Manager) CreateInspection(ins *model.Inspection) error {
	return m.DB.Create(&ins).Error
}

func (m *Manager) GetAllInspections() (ins []model.Inspection, err error) {
	err = m.DB.Preload("InnerInspections.InnerInspectionViews").Find(&ins).Error
	return
}

func (m *Manager) CreateInnerInspection(ins *model.InnerInspection) error {
	return m.DB.Create(&ins).Error
}

func (m *Manager) GetAllInnerInspections() (ins []model.InnerInspection, err error) {
	err = m.DB.Preload("InnerInspectionViews").Find(&ins).Error
	return
}

func (m *Manager) CreateInnerInspectionView(ins *model.InnerInspectionView) error {
	return m.DB.Create(&ins).Error
}

func (m *Manager) GetAllInnerInspectionViews() (ins []model.InnerInspectionView, err error) {
	err = m.DB.Find(&ins).Error
	return
}

func (m *Manager) CreateInnerInspectionViewType(viewType *model.InnerInspectionViewType) error {
	return m.DB.Create(&viewType).Error
}

func (m *Manager) GetAllInnerInspectionViewTypes() (viewType []model.InnerInspectionViewType, err error) {
	err = m.DB.Find(&viewType).Error
	return
}

func (m *Manager) UpdateInspection(inspection *model.Inspection) error {
	return m.DB.Session(&gorm.Session{FullSaveAssociations: true}).Updates(inspection).Error
}

func (m *Manager) UpdateInnerInspection(innerInspection *model.InnerInspection) error {
	return m.DB.Session(&gorm.Session{FullSaveAssociations: true}).Updates(innerInspection).Error
}

func (m *Manager) UpdateInnerInspectionView(innerInspectionView *model.InnerInspectionView) error {
	return m.DB.Session(&gorm.Session{FullSaveAssociations: true}).Updates(innerInspectionView).Error
}

func (m *Manager) UpdateInnerInspectionViewType(innerInspectionViewType *model.InnerInspectionViewType) error {
	return m.DB.Session(&gorm.Session{FullSaveAssociations: true}).Updates(innerInspectionViewType).Error
}

