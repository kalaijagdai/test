package db

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/utils"
	"gorm.io/gorm"
)

func (m *Manager) CreateDeal(deal *model.Deal) error {
	return m.DB.Create(deal).Error
}

// which fields are we need for?
func (m *Manager) GetDealByID(deal_id uint) (deal model.Deal, err error) {
	db := m.DB
	db = db.Unscoped()
	db = db.Preload("Client", utils.Unscopedizer())
	db = db.Preload("Client.ClientType")
	db = db.Preload("Client.Referral")
	db = db.Preload("Contacts.Communication")
	db = db.Preload("Cars", func(db *gorm.DB) *gorm.DB {
		db = db.Joins("DealCar")
		db = db.Preload("DealCar.CarInterest")
		db = db.Preload("DealCar.Relevance")
		db = db.Preload("CarCustomCharacteristicValues.CarCharacteristic")
		db = db.Preload("CarCustomCharacteristicValues.CarCharacteristicValue")
		return db
	})
	db = db.Preload("Cars.CarModel.CarMark")

	db = db.Preload("User", utils.Unscopedizer())
	db = db.Preload("Dealership", utils.Unscopedizer())
	db = db.Preload("Dealership.Users", utils.Unscopedizer())
	db = db.Preload("DealGoal")
	db = db.Preload("DealStatus")
	db = db.Preload("Communication")
	err = db.First(&deal, deal_id).Error
	return
}

func (m *Manager) GetDeals(queries map[string]interface{}, user model.User, limit, offset uint64) (deals []model.Deal, err error) {
	db := m.DB

	var arrayOfConditions []string

	if val, ok := queries["name"]; ok {
		queries["name"] = fmt.Sprintf("%%%s%%", val)
		strFmt := `LOWER("Client".name) LIKE LOWER(@name)`
		arrayOfConditions = append(arrayOfConditions, strFmt)
	}

	if onlyMy, ok := queries["onlyMy"]; ok {
		if isOnlyMy, _ := strconv.ParseBool(onlyMy.(string)); isOnlyMy == true {
			queries["userID"] = user.ID
			strFmt := "deal.user_id = @userID"
			arrayOfConditions = append(arrayOfConditions, strFmt)
		}
	}

	var dealershipUserIDs []uint
	for _, dealership := range user.Dealerships {
		for _, user := range dealership.Users {
			dealershipUserIDs = append(dealershipUserIDs, user.ID)
		}
	}

	if user.Role.Name != "OWNER" && len(user.Dealerships) > 0 {
		queries["dealerships"] = dealershipUserIDs
		arrayOfConditions = append(arrayOfConditions, "deal.user_id IN @dealerships")
	}

	if len(arrayOfConditions) > 0 {
		conditions := fmt.Sprintf("%s", strings.Join(arrayOfConditions, " AND "))
		db = db.Where(conditions, queries)
	}

	db = db.Unscoped()
	db = db.Limit(int(limit))
	db = db.Offset(int(offset))
	db = db.Order("created_at desc")
	db = db.Joins("User")
	db = db.Joins("Dealership")
	db = db.Joins("Client")
	db = db.Preload("Cars", func(db *gorm.DB) *gorm.DB {
		return db.Joins("DealCar").Preload("CarModel.CarMark").Preload("DealCar.CarInterest").Preload("DealCar.Relevance")
	})
	db = db.Preload("DealGoal")
	db = db.Preload("DealStatus")
	db = db.Preload("Communication")

	err = db.Find(&deals).Error
	return
}

// update exact fields in Deal
func (m *Manager) UpdateDeal(dealID uint, data map[string]interface{}) error {
	return m.DB.First(&model.Deal{}, dealID).Updates(data).Error
}

func (m *Manager) AddContactToDeal(dealID uint, contact *model.Contact) error {
	return m.DB.First(&model.Deal{}, dealID).Association("Contacts").Append(contact)
	// Error #todo
}
