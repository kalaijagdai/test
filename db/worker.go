package db

import (
	"fmt"
	"gitlab.com/tradeincar/backend/api/model"
)

func (m *Manager) GetCarsReserveExpired() (cars []model.Car, err error) {
err = m.DB.Where("DATE_PART('day', now() - updated_at) >=3").Find(&cars).Error
return
}

func (m *Manager) GetExpiredAssessmentIDsOnStatus(statusID uint, preiodInDays uint) (assessmentIDs []uint, err error) {
	query := `select assessment.id
	from assessment, assessment_status_log log
	where log.id in (select max(id) from assessment_status_log group by assessment_id)
	and log.assessment_id = assessment.id
	and assessment.assessment_status_id = ?
	and log.dst_assessment_status_id = assessment.assessment_status_id
	and DATE_PART('day', now() - log.created_at) >= ?;`

	rows, err := m.DB.Raw(query, statusID, preiodInDays).Rows()
	defer rows.Close()
	for rows.Next() {
		var assessmentID uint
		rows.Scan(&assessmentID)
		assessmentIDs = append(assessmentIDs, assessmentID)
	}
	fmt.Println("assessmentIDs: ", assessmentIDs)
	return
}

func (m *Manager) GetNotChangedAssessmentsOnStatus(statusID uint, preiodInDays uint) (assessmentIDs []uint, err error) {
	query := `select assessment.id
	from assessment, assessment_pricing pricing
	where pricing.id in (select max(id) from assessment_pricing group by assessment_id)
	and pricing.assessment_id = assessment.id
	and assessment.assessment_status_id = ?
	and DATE_PART('day', now() - pricing.created_at) >= ?;`

	rows, err := m.DB.Raw(query, statusID, preiodInDays).Rows()
	defer rows.Close()
	for rows.Next() {
		var assessmentID uint
		rows.Scan(&assessmentID)
		assessmentIDs = append(assessmentIDs, assessmentID)
	}
	fmt.Println("assessmentIDs: ", assessmentIDs)
	return
}