package db

import (
	"fmt"
	"strings"

	"gitlab.com/tradeincar/backend/api/model"
)

func (m *Manager) CreateUser(user *model.User) error {
	return m.DB.Create(&user).Error
}

func (m *Manager) GetUserByID(userID uint) (user model.User, err error) {
	err = m.DB.Preload("UserStatus").Preload("Role").Preload("Dealerships").First(&user, userID).Error
	return
}

func (m *Manager) GetDealershipUsersByRole(dealershipID uint, role string) (users []model.User, err error) {
	query := `select *
	from user, dealership_users
	where dealership_users.dealership_id = ?
	and user.id = dealership_users.user.id`
	if role != "" {
		query += "and user.role = ?"
	}

	rows, err := m.DB.Raw(query, dealershipID, role).Rows()
	defer rows.Close()
	for rows.Next() {
		user := model.User{}
		rows.Scan(&user)
		users = append(users, user)
	}
	return
}

func (m *Manager) GetAllUsers(queries map[string]interface{}, user model.User, limit, offset uint64) (users []model.User, err error) {
	db := m.DB

	var arrayOfConditions []string

	if val, ok := queries["name"]; ok {
		queries["name"] = fmt.Sprintf("%%%s%%", val)
		strFmt := "LOWER(users.last_name || ' ' || users.first_name || ' ' || users.middle_name) LIKE LOWER(@name)"
		arrayOfConditions = append(arrayOfConditions, strFmt)
	}

	if _, ok := queries["role"]; ok {
		strFmt := `"Role".name = @role`
		arrayOfConditions = append(arrayOfConditions, strFmt)
	}

	var dealershipUserIDs []uint
	for _, dealership := range user.Dealerships {
		for _, user := range dealership.Users {
			dealershipUserIDs = append(dealershipUserIDs, user.ID)
		}
	}

	if user.Role.Name != "OWNER" && len(user.Dealerships) > 0 {
		queries["dealerships"] = dealershipUserIDs
		arrayOfConditions = append(arrayOfConditions, "users.id IN @dealerships")
	}

	if len(arrayOfConditions) > 0 {
		conditions := fmt.Sprintf("%s", strings.Join(arrayOfConditions, " AND "))
		db = db.Where(conditions, queries)
	}

	err = db.Limit(int(limit)).Offset(int(offset)).Joins("UserStatus").Preload("Dealerships").Joins("Role").Order("created_at desc").Find(&users).Error
	return
}

func (m *Manager) UpdateUser(user *model.User) error {
	return m.DB.Model(&user).Updates(&user).Error
}

func (m *Manager) DeleteEmailByID(userID uint) error {
	return m.DB.First(&model.User{}, userID).Update("email", nil).Error
}

func (m *Manager) RemoveDealershipFromUser(dealership *model.Dealership, user *model.User) error {
	return m.DB.Model(user).Association("Dealerships").Delete(dealership)
	// Error #todo
}

func (m *Manager) DeleteUser(userID uint) error {
	// get user ARCHIVED status
	var userStatus model.UserStatus
	userStatus, err := m.GetUserStatusByName("ARCHIVED")
	if err != nil {
		return err
	}

	// set user status
	var user model.User
	err = m.DB.First(&model.User{}, userID).Update("user_status_id", userStatus.ID).Error
	if err != nil {
		return err
	}
	// update using native gorm method
	err = m.DB.First(&model.User{}, userID).Delete(&user).Error
	return err
}

func (m *Manager) GetUserByEmail(email string) (user model.User, err error) {
	err = m.DB.Where("email = ?", email).First(&user).Error
	return
}

func (m *Manager) UpdateSessionTokenByUserID(userID uint, sessionToken string) error {
	return m.DB.First(&model.User{}, userID).Update("session_token", sessionToken).Error
}

func (m *Manager) GetUserBySessionToken(sessionToken string) (user model.User, err error) {
	err = m.DB.Joins("UserStatus").Joins("Role").Preload("Dealerships.Users").First(&user, "session_token", sessionToken).Error
	return
}

func (m *Manager) GetUserRoleByID(userID uint) (user model.User, err error) {
	err = m.DB.Joins("Role").First(&user, userID).Error
	return
}