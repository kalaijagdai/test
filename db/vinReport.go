package db

import (
	"gitlab.com/tradeincar/backend/api/model"
)

func (m *Manager) CreateVinReport(vinReport *model.VinReport) error {
	return m.DB.Create(vinReport).Error
}

func (m *Manager) GetVinReportByID(id uint) (vinReport model.VinReport, err error) {
	err = m.DB.Joins("Status").First(&vinReport, id).Error
	return
}

func (m *Manager) GetAllVinReportByAssessmentID(assesmentID uint) (vinReports []model.VinReport, err error) {
	err = m.DB.Select(`vin_report.id`, `"Status".id AS Status__id`, `"Status".name AS Status__name`, "assessment_id", "ownership_count", "has_restrict", "is_search", "dtp_count", "created_at", "updated_at").Joins("Status").Order("created_at desc").Where("assessment_id = ?", assesmentID).Find(&vinReports).Error
	return
}
