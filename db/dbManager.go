package db

import (
	"fmt"

	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Manager struct {
	DB *gorm.DB
}

func NewDB(config *model.Configuration) *gorm.DB {
	connectionString := fmt.Sprintf("host=%v port=%v user=%v dbname=%v password=%v sslmode=%v TimeZone=Asia/Almaty", config.DbHost, config.DbPort, config.DbUser, config.DbName, config.DbPass, config.DbMode)

	// db, err := gorm.Open("postgres", connectionString)
	db, err := gorm.Open(postgres.Open(connectionString), &gorm.Config{DisableForeignKeyConstraintWhenMigrating: true})
	if err != nil {
		panic(err)
	}

	// db.LogMode(config.DbLogMode)
	// db.DB().SetMaxOpenConns(config.DbMaxOpenConns)
	// db.DB().SetMaxIdleConns(config.DbMaxIdleConns)
	return db
}

func NewDBManager(db *gorm.DB) *Manager {
	return &Manager{
		DB: db,
	}
}

func (m *Manager) AutoMigrate() {
	m.DB.AutoMigrate(
		model.DealershipStatus{},
		model.Dealership{},
		model.UserStatus{},
		model.User{},
		model.Address{},
		model.Referral{},
		model.Gender{},
		model.ClientType{},
		model.Client{},
		model.Car{},
		model.Contact{},
		model.Deal{},
		model.Communication{},
		model.DealStatus{},
		model.DealGoal{},
		model.Relevance{},
		model.DealCar{},
		model.CarInfo{},
		model.CarInterest{},
		model.CarStatus{},
		model.CarType{},
		model.CarMark{},
		model.CarModel{},
		model.CarGeneration{},
		model.CarSerie{},
		model.CarModification{},
		model.CarEquipment{},
		model.CarOption{},
		model.CarOptionValue{},
		model.CarCharacteristic{},
		model.CarCharacteristicValue{},
		model.CarCustomCharacteristicValue{},
		model.Role{},
		model.Service{},
		model.Addition{},
		model.InnerInspectionViewType{},
		model.InnerInspectionView{},
		model.InnerInspection{},
		model.Inspection{},
		model.AssessmentReport{},
		model.AssessmentInnerInspectionView{},
		model.AssessmentInnerInspection{},
		model.AssessmentInspection{},
		model.AssessmentStatus{},
		model.AssessmentPricing{},
		model.AssessmentPricingHistory{},
		model.AssessmentAddition{},
		model.AssessmentService{},
		model.AssessmentStage{},
		model.AssessmentGear{},
		model.SecurityGear{},
		model.AirbagGear{},
		model.ParktronicGear{},
		model.AssistiveSystemGear{},
		model.SalonGear{},
		model.HeatedSeatGear{},
		model.SeatVentilationGear{},
		model.ExteriorGear{},
		model.ReviewGear{},
		model.MirrorGear{},
		model.ElectricHeaterGear{},
		model.ComfortGear{},
		model.SteeringWheelGear{},
		model.MultimediaGear{},
		model.RadioGear{},
		model.MatSetGear{},
		model.TheftProtectionGear{},
		model.Assessment{},
		model.AssessmentDocumentType{},
		model.AssessmentDocument{},
		model.AssessmentDocumentFile{},
		model.AssessmentPhotoType{},
		model.AssessmentPhoto{},
		model.Assessment{},
		model.AssessmentAdmission{},
		model.AssessmentType{},
		model.TradeIn{},
		model.Sale{},
		model.SalesChannel{},
		model.Reserve{},
		model.AssessmentStatusLog{},
		model.VinReportStatus{},
		model.VinReport{},
		model.Report{},
		model.Sell{},
		model.ColumnMap{},
		model.Country{},
		model.Region{},
		model.Bank{},
		model.Factory{},
		model.PaymentMethod{},
		model.AkabStatus{},
		model.AkabErrorHiddenLog{},
		model.SellsUpdateHistory{},
		model.FilterCarByUser{},
	)
}

func (m *Manager) AutoMigrateData() {

	// m.DB.Create(&model.DealershipStatus{Name: "ACTIVE", Description: "Активный"})
	// m.DB.Create(&model.DealershipStatus{Name: "DISABLED", Description: "Отключен"})
	// m.DB.Create(&model.DealershipStatus{Name: "ARCHIVED", Description: "В архиве"})

	// m.DB.Create(&model.UserStatus{Name: "ACTIVE", Description: "Активный"})
	// m.DB.Create(&model.UserStatus{Name: "DISABLED", Description: "Отключен"})
	// m.DB.Create(&model.UserStatus{Name: "ARCHIVED", Description: "В архиве"})

	// m.DB.Create(&model.Gender{Name: "FEMALE", Description: "Женский"})
	// m.DB.Create(&model.Gender{Name: "MALE", Description: "Мужской"})

	// m.DB.Create(&model.Referral{Name: "Интернет"})
	// m.DB.Create(&model.Referral{Name: "Классифайд"})
	// m.DB.Create(&model.Referral{Name: "Наружная реклама"})
	// m.DB.Create(&model.Referral{Name: "Партнеры"})
	// m.DB.Create(&model.Referral{Name: "Печатные изделия"})
	// m.DB.Create(&model.Referral{Name: "Радио"})
	// m.DB.Create(&model.Referral{Name: "Рекомендации"})
	// m.DB.Create(&model.Referral{Name: "Социальные сети"})
	// m.DB.Create(&model.Referral{Name: "Телевидение"})

	// m.DB.Create(&model.ClientType{Name: "NATURAL", Description: "Физическое лицо"})
	// m.DB.Create(&model.ClientType{Name: "LEGAL", Description: "Юридическое лицо"})

	// m.DB.Create(&model.DealGoal{Name: "TRADE_IN_NEW", Description: "TradeIn(новый)"})
	// m.DB.Create(&model.DealGoal{Name: "TRADE_IN_OLD", Description: "TradeIn(с пробегом)"})
	// m.DB.Create(&model.DealGoal{Name: "BUY", Description: "Только купить"})
	// m.DB.Create(&model.DealGoal{Name: "SELL", Description: "Только продать"})

	// m.DB.Create(&model.DealStatus{Name: "NEW_CLIENT", Description: "Новый клиент (только создан)"})
	// m.DB.Create(&model.DealStatus{Name: "INVOLVED", Description: "Квалифицированный (заинтересован, либо начались действия(звонки))"})
	// m.DB.Create(&model.DealStatus{Name: "WAS_IN_DC", Description: "Была встреча (был в ДЦ)"})
	// m.DB.Create(&model.DealStatus{Name: "CONDITIONS_NEGOTIATION", Description: "Согласование условий (оценка/авто)"})
	// m.DB.Create(&model.DealStatus{Name: "CLIENT_REJECTION", Description: "Отказ клиента"})
	// m.DB.Create(&model.DealStatus{Name: "CLOSED_WIN", Description: "Завершена с победой (либо купил/либо продал)"})
	// m.DB.Create(&model.DealStatus{Name: "CLOSED", Description: "Введение закрыто"})
	// m.DB.Create(&model.DealStatus{Name: "ON_CLOSE_APPROVAL", Description: "На утверждении закрытия"})

	// m.DB.Create(&model.Communication{Name: "ONLINE", Description: "Online"})
	// m.DB.Create(&model.Communication{Name: "SALON_MEETING", Description: "Встреча в салоне"})
	// m.DB.Create(&model.Communication{Name: "INCOMING_CALL", Description: "Входящий звонок"})
	// m.DB.Create(&model.Communication{Name: "OUTCOMING_CALL", Description: "Исходящий звонок"})

	// m.DB.Create(&model.Relevance{Name: "1_WEEK", Description: "1 неделя"})
	// m.DB.Create(&model.Relevance{Name: "2_WEEK", Description: "2 недели"})
	// m.DB.Create(&model.Relevance{Name: "1_MONTH", Description: "1 месяц"})
	// m.DB.Create(&model.Relevance{Name: "2_MONTH", Description: "2 месяца"})

	// m.DB.Create(&model.CarInterest{Name: "BUY", Description: "Покупка"})
	// m.DB.Create(&model.CarInterest{Name: "SELL", Description: "Продажа"})

	// m.DB.Create(&model.CarStatus{Name: "DEAL", Description: "заявка (нет на складе)"})
	// m.DB.Create(&model.CarStatus{Name: "ON_ASSESSMENT", Description: "На оценке"})
	// m.DB.Create(&model.CarStatus{Name: "WAREHOUSE", Description: "На складе"})
	// m.DB.Create(&model.CarStatus{Name: "TRADE_IN_NEW", Description: "TradeIn (новое авто)"})

	// m.DB.Create(&model.Role{Name: "OWNER", Description: "Владелец"})
	// m.DB.Create(&model.Role{Name: "MANAGER", Description: "Руководитель"})
	// m.DB.Create(&model.Role{Name: "ANALYST", Description: "Аналитик"})
	// m.DB.Create(&model.Role{Name: "ATZ", Description: "АТЗ"})
	// m.DB.Create(&model.Role{Name: "SPECIALIST", Description: "Специалист"})
	// m.DB.Create(&model.Role{Name: "TECHNICIAN", Description: "Техник"})

	// m.DB.Create(&model.AkabStatus{Name: "ON_WORK", Description: "Незавершенный отчет"})
	// m.DB.Create(&model.AkabStatus{Name: "ON_PROCESSING", Description: "В обработке"})
	// m.DB.Create(&model.AkabStatus{Name: "CONFIRMED", Description: "Отчет отправлен"})

	// stageOnAssessment := &model.AssessmentStage{Name: "ON_ASSESSMENT", Description: "На оценке"}
	// m.DB.Create(&stageOnAssessment)
	// m.DB.Create(&model.AssessmentStatus{Name: "ON_WORK", Description: "Оценка в работе", AssessmentStageID: stageOnAssessment.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "DC_REFUSAL", Description: "Отказ ДЦ", AssessmentStageID: stageOnAssessment.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "ON_MANAGER_APPROVAL", Description: "Оценка на утверждении", AssessmentStageID: stageOnAssessment.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "ASSESSMENT_APPROVED", Description: "Оценка утверждена", AssessmentStageID: stageOnAssessment.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "CLIENT_NOT_SATISFIED_WITH_ASSESSMENT", Description: "Оценка не устроила клиента", AssessmentStageID: stageOnAssessment.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "DIAGNOSTICS", Description: "Диагностика", AssessmentStageID: stageOnAssessment.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "DIAGNOSTICS_ON_APPROVAL", Description: "Диагностика на утверждении", AssessmentStageID: stageOnAssessment.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "DIAGNOSTICS_APPROVED", Description: "Диагностика утверждена", AssessmentStageID: stageOnAssessment.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "CLIENT_NOT_SATISFIED_WITH_DIAGNOSTICS", Description: "Диагностика не устроила клиента", AssessmentStageID: stageOnAssessment.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "SERVICE_CENTER_REMOVAL", Description: "Снятие в спецЦоне", AssessmentStageID: stageOnAssessment.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "CLIENT_REFUSAL", Description: "Отказ клиента", AssessmentStageID: stageOnAssessment.ID})

	// stageInStock := &model.AssessmentStage{Name: "IN_STOCK", Description: "На складе"}
	// m.DB.Create(&stageInStock)
	// m.DB.Create(&model.AssessmentStatus{Name: "SALE_PREPARATION", Description: "Подготовка к продаже", AssessmentStageID: stageInStock.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "ON_ROP_APPROVAL", Description: "Утверждение РОП", AssessmentStageID: stageInStock.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "ON_SALE", Description: "В продаже", AssessmentStageID: stageInStock.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "RESERVE", Description: "Резерв", AssessmentStageID: stageInStock.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "RETURN_TO_CLIENT", Description: "Возвращен клиенту", AssessmentStageID: stageInStock.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "TEMPORALLY_NOT_SALE", Description: "Продажа закрыта", AssessmentStageID: stageInStock.ID})
	// m.DB.Create(&model.AssessmentStatus{Name: "DELETED", Description: "Удален", AssessmentStageID: stageInStock.ID})

	// m.DB.Create(&model.CarCharacteristicValue{Key: "GASOLINE", Value: "Бензиновый", CarCharacteristicID: 12, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "HYBRID", Value: "Гибридный", CarCharacteristicID: 12, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "DIESEL", Value: "Дизельный", CarCharacteristicID: 12, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "ELECTRICAL", Value: "Электрический", CarCharacteristicID: 12, IsFromBasebuy: false, CarTypeID: 1})

	// m.DB.Create(&model.CarCharacteristicValue{Key: "AUTOMATIC", Value: "Автомат", CarCharacteristicID: 24, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "VARIATOR", Value: "Вариатор", CarCharacteristicID: 24, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "MECHANICAL", Value: "Механика", CarCharacteristicID: 24, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "ROBOT", Value: "Робот", CarCharacteristicID: 24, IsFromBasebuy: false, CarTypeID: 1})

	// m.DB.Create(&model.CarCharacteristicValue{Key: "RWD", Value: "Задний", CarCharacteristicID: 27, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "FWD", Value: "Передний", CarCharacteristicID: 27, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "4WD", Value: "Полный", CarCharacteristicID: 27, IsFromBasebuy: false, CarTypeID: 1})

	// m.DB.Create(&model.CarCharacteristicValue{Key: "SUV", Value: "Внедорожник", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "CABRIOLET", Value: "Кабриолет", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "COUPE", Value: "Купе", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "LIMOUSINE", Value: "Лимузин", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "LIFTBACK", Value: "Лифтбек", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "MINIVAN", Value: "Минивэн", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "PICKUP", Value: "Пикап", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "PICKUP_DOUBLE_CAB", Value: "Пикап Двойная кабина", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "PICKUP_SINGLE_CAB", Value: "Пикап Одинарная кабина", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "PICKUP_HALF_CAB", Value: "Пикап Полуторная кабина", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "ROADSTER", Value: "Родстер", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "SEDAN", Value: "Седан", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "STATION_WAGON", Value: "Универсал", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "VAN", Value: "Фургон", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "HATCHBACK", Value: "Хэтчбек", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "CUV", Value: "Кроссовер", CarCharacteristicID: 2, IsFromBasebuy: false, CarTypeID: 1})

	// m.DB.Create(&model.CarCharacteristicValue{Key: "LEFT", Value: "Левый", CarCharacteristicID: 53, IsFromBasebuy: false, CarTypeID: 1})
	// m.DB.Create(&model.CarCharacteristicValue{Key: "RIGHT", Value: "Правый", CarCharacteristicID: 53, IsFromBasebuy: false, CarTypeID: 1})

	// m.DB.Create(&model.AssessmentDocumentType{Name: "ASSESSMENT_TECH_PASSPORT", Description: "Оценка / Тех паспорт"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "RECEPTION_INSPECTION_ACT", Description: "Прием / Акт осмотра"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "RECEPTION_ACCEPTANCE_ACT", Description: "Прием / Акт приема-передачи"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "RECEPTION_DIAGNOSTIC_CARD", Description: "Прием / Диагностическая карта"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "RECEPTION_POWER_OF_ATTORNEY", Description: "Прием / Доверенность"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "RECEPTION_CONTRACT", Description: "Прием / Договор"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "RECEPTION_ORDER", Description: "Прием / Заказ-наряд"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "RECEPTION_APP_FOR_TRANSFER_DS", Description: "Прием / Заявление на перевод ДС"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "RECEPTION_PASSPORT", Description: "Прием / Паспорт"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "RECEPTION_TECH_PASSPORT", Description: "Прием / Тех паспорт"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "RECEPTION_SERVICE_BOOK", Description: "Прием / Сервисная книжка"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "SALE_ACCEPTANCE_ACT", Description: "Продажа / Акт приема-передачи"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "SALE_CONTRACT", Description: "Продажа / Договор"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "SALE_PASSPORT", Description: "Продажа / Паспорт"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "SALE_PTS", Description: "Продажа / ПТС"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "TRADE_IN_ACCEPTANCE_ACT", Description: "Trade-In / Акт приема-передачи"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "TRADE_IN_CONTRACT", Description: "Trade-In / Договор"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "TRADE_IN_APP_FOR_TRANSFER_DS", Description: "Trade-In / Заявление на перевод ДС"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "TRADE_IN_PASSPORT", Description: "Trade-In / Паспорт"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "TRADE_IN_PTS", Description: "Trade-In / ПТС"})
	// m.DB.Create(&model.AssessmentDocumentType{Name: "OTHER", Description: "Другое"})

	// m.DB.Create(&model.AssessmentPhotoType{Name: "FRONT_LEFT", Description: "Спереди слева"})
	// m.DB.Create(&model.AssessmentPhotoType{Name: "FRONT", Description: "Передняя часть"})
	// m.DB.Create(&model.AssessmentPhotoType{Name: "FRONT_RIGHT", Description: "Спереди справа"})
	// m.DB.Create(&model.AssessmentPhotoType{Name: "LEFT_SIDE", Description: "Левый борт"})
	// m.DB.Create(&model.AssessmentPhotoType{Name: "INTERIOR", Description: "Интерьер"})
	// m.DB.Create(&model.AssessmentPhotoType{Name: "RIGHT_SIDE", Description: "Правый борт"})
	// m.DB.Create(&model.AssessmentPhotoType{Name: "BACK_LEFT", Description: "Сзади слева"})
	// m.DB.Create(&model.AssessmentPhotoType{Name: "BACK_SEATS", Description: "Задние сиденья"})
	// m.DB.Create(&model.AssessmentPhotoType{Name: "BACK_RIGHT", Description: "Сзади справа"})
	// m.DB.Create(&model.AssessmentPhotoType{Name: "TRUNK", Description: "Багажное отделение"})
	// m.DB.Create(&model.AssessmentPhotoType{Name: "BACK", Description: "Задняя часть"})
	// m.DB.Create(&model.AssessmentPhotoType{Name: "WHEEL", Description: "Колеса"})
	// m.DB.Create(&model.AssessmentPhotoType{Name: "ASSESSMENT_PHOTO", Description: "Фото оценки"})
	// m.DB.Create(&model.AssessmentPhotoType{Name: "ADDITIONAL", Description: "Дополнительно"})
	// m.DB.Create(&model.AssessmentPhotoType{Name: "YOUTUBE_LINK", Description: "Видео на Youtube (url)"})

	// m.DB.Create(&model.AssessmentType{Name: "TRADE_IN_NEW", Description: "Trade-in (новый)"})
	// m.DB.Create(&model.AssessmentType{Name: "TRADE_IN_MILEAGE", Description: "Trade-in (с пробегом)"})
	// m.DB.Create(&model.AssessmentType{Name: "BUYOUT", Description: "Выкуп"})
	// m.DB.Create(&model.AssessmentType{Name: "COMMISSION", Description: "Комиссия"})

	// m.DB.Create(&model.SalesChannel{Name: "MYCAR", Description: "mycar.kz"})

	// m.DB.Create(&model.VinReportStatus{ID: 1, Name: "В очереди"})
	// m.DB.Create(&model.VinReportStatus{ID: 2, Name: "Отчет формируется"})
	// m.DB.Create(&model.VinReportStatus{ID: 3, Name: "Отчет готов"})
	// m.DB.Create(&model.VinReportStatus{ID: 4, Name: "Отчет сформирован с ошибкой"})

}
