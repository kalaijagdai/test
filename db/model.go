package db

import (
	"gitlab.com/tradeincar/backend/api/model"
)


func (m *Manager) GetModelsByMarkID(carMarkID uint) (models []model.CarModel, err error) {
	err = m.DB.Order("id").Where("car_mark_id = ?", carMarkID).Find(&models).Error
	return
}
