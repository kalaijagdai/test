package db

import (
	"fmt"
	"strings"

	"gitlab.com/tradeincar/backend/api/model"
	"gorm.io/gorm"
)

func (m *Manager) CreateDealership(dealership *model.Dealership) error {
	return m.DB.Create(&dealership).Error
}

func (m *Manager) GetAllDealerships(queries map[string]interface{}, user model.User, limit, offset uint64) (dealerships []model.Dealership, err error) {
	db := m.DB

	var arrayOfConditions []string

	if val, ok := queries["name"]; ok {
		queries["name"] = fmt.Sprintf("%%%s%%", val)
		strFmt := `LOWER(dealership.name) LIKE LOWER(@name)`
		arrayOfConditions = append(arrayOfConditions, strFmt)
	}

	var dealershipUserIDs []uint
	for _, dealership := range user.Dealerships {
		dealershipUserIDs = append(dealershipUserIDs, dealership.ID)
	}

	if user.Role.Name != "OWNER" && len(user.Dealerships) > 0 {
		queries["dealerships"] = dealershipUserIDs
		arrayOfConditions = append(arrayOfConditions, "dealership.id IN @dealerships")
	}

	if len(arrayOfConditions) > 0 {
		conditions := fmt.Sprintf("%s", strings.Join(arrayOfConditions, " AND "))
		db = db.Where(conditions, queries)
	}

	err = db.Limit(int(limit)).Offset(int(offset)).Preload("DealershipStatus").Preload("Marks").Preload("Region").Preload("Address").Preload("Users").Preload("Additions").Preload("Services").Order("created_at desc").Find(&dealerships).Error
	return
}

func (m *Manager) GetDealershipUsers(dealershipID uint) (users []model.User, err error) {
	dealership := model.Dealership{}
	err = m.DB.Preload("Users").Preload("Users.Role").Preload("Users.UserStatus").First(&dealership, dealershipID).Error
	users = dealership.Users
	return
}

func (m *Manager) GetDealershipByID(dealershipID uint) (dealership model.Dealership, err error) {
	err = m.DB.Preload("DealershipStatus").Preload("Marks").Preload("Address").Preload("Users").Preload("Additions").Preload("Services").Preload("Region").First(&dealership, dealershipID).Error
	return
}

func (m *Manager) UpdateDealership(dealership *model.Dealership) error {
	return m.DB.Transaction(func(tx *gorm.DB) error {
		if dealership.ManagerAffirmation == false || dealership.SaleTransfer == false {
			if err := tx.Model(dealership).Updates(map[string]interface{}{"ManagerAffirmation": dealership.ManagerAffirmation, "SaleTransfer": dealership.SaleTransfer}).Error; err != nil {
				return err
			}
		}

		if err := tx.Updates(dealership).Error; err != nil {
			return err
		}

		return nil
	})
}

func (m *Manager) RemoveMarkFromDealership(mark *model.CarMark, dealership *model.Dealership) error {
	return m.DB.Model(dealership).Association("Marks").Delete(mark)
	// Error #todo
}

func (m *Manager) DeleteDealership(dealershipID uint) error {
	dealership := model.Dealership{ID: dealershipID}
	return m.DB.First(&model.Dealership{}, dealershipID).Delete(&dealership).Error
	// return m.DB.Select(clause.Associations).Delete(&dealership).Error
}
