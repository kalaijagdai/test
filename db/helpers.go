package db

import (
	"gitlab.com/tradeincar/backend/api/model"
)

func (m *Manager) GetAllGenders() (statuses []model.Gender, err error) {
	err = m.DB.Find(&statuses).Error
	return
}

func (m *Manager) GetAllReferrals() (statuses []model.Referral, err error) {
	err = m.DB.Find(&statuses).Error
	return
}

func (m *Manager) GetAllClientTypes() (statuses []model.ClientType, err error) {
	err = m.DB.Find(&statuses).Error
	return
}

func (m *Manager) GetAllRoles() (statuses []model.Role, err error) {
	err = m.DB.Not("name = ?", "OWNER").Find(&statuses).Error
	return
}

func (m *Manager) GetAllDocumentTypes() (assessmentDocumentTypes []model.AssessmentDocumentType, err error) {
	err = m.DB.Order("id").Find(&assessmentDocumentTypes).Error
	return
}

func (m *Manager) GetAllPhotoTypes() (assessmentPhotoTypes []model.AssessmentPhotoType, err error) {
	err = m.DB.Order("id").Find(&assessmentPhotoTypes).Error
	return
}

func (m *Manager) GetAllAssessmentStatuses() (statuses []model.AssessmentStatus, err error) {
	err = m.DB.Find(&statuses).Error
	return
}

func (m *Manager) GetAllAssessmentTypes() (types []model.AssessmentType, err error) {
	err = m.DB.Find(&types).Error
	return
}

func (m *Manager) GetAllCommunications() (communications []model.Communication, err error) {
	err = m.DB.Find(&communications).Error
	return
}

func (m *Manager) GetAllDealGoals() (dealGoals []model.DealGoal, err error) {
	err = m.DB.Find(&dealGoals).Error
	return
}

func (m *Manager) GetAllCarInterests() (carInterests []model.CarInterest, err error) {
	err = m.DB.Find(&carInterests).Error
	return
}

func (m *Manager) GetAllRelevances() (relevances []model.Relevance, err error) {
	err = m.DB.Find(&relevances).Error
	return
}

func (m *Manager) GetAllCarStatuses() (carStatuses []model.CarStatus, err error) {
	err = m.DB.Find(&carStatuses).Error
	return
}

func (m *Manager) GetAllAssessmentStages() (assessmentStages []model.AssessmentStage, err error) {
	err = m.DB.Find(&assessmentStages).Error
	return
}

func (m *Manager) ImportAllAuto() {

}

func (m *Manager) GetCountries(pagination *model.Pagination) (countries []model.Country, err error) {
	err = m.DB.Scopes(Paginate(pagination.Offset, pagination.Limit)).
		Preload("Regions").
		Preload("Factories").
		Preload("Banks").
		Order("is_popular desc").
		Order("name").
		Find(&countries, "LOWER(name) LIKE LOWER(?)", "%"+pagination.SearchText+"%").Error
	return
}

func (m *Manager) GetRegionsByCountryID(countryID uint, pagination *model.Pagination) (regions []model.Region, err error) {
	err = m.DB.Scopes(Paginate(pagination.Offset, pagination.Limit)).
		Where("country_id = ? AND LOWER(name) LIKE LOWER(?)",
			countryID,
			"%"+pagination.SearchText+"%",
		).Find(&regions).Error
	return
}

func (m *Manager) GetFactories(countryID uint, pagination *model.Pagination) (factories []model.Factory, err error) {
	err = m.DB.Scopes(Paginate(pagination.Offset, pagination.Limit)).
		Where("country_id = ? AND LOWER(name) LIKE LOWER(?)",
			countryID,
			"%"+pagination.SearchText+"%",
		).Find(&factories).Error
	return
}

func (m *Manager) GetBanksByCountryID(countryID uint, pagination *model.Pagination) (banks []model.Bank, err error) {
	err = m.DB.Scopes(Paginate(pagination.Offset, pagination.Limit)).
		Where("country_id = ? AND LOWER(name) LIKE LOWER(?)",
			countryID,
			"%"+pagination.SearchText+"%",
		).Find(&banks).Error
	return
}

func (m *Manager) GetPaymentMethods(pagination *model.Pagination) (paymentMethods []model.PaymentMethod, err error) {
	err = m.DB.Scopes(Paginate(pagination.Offset, pagination.Limit)).Find(&paymentMethods, "LOWER(name) LIKE LOWER(?)", "%"+pagination.SearchText+"%").Error
	return
}

func (m *Manager) GetCountryIdByName(name string) (country model.Country, err error) {
	err = m.DB.
		Where("name = ?",
			name,
		).Find(&country).Error
	return
}

//'id_car_type','name'
//'1','легковые'
//'2','лёгкие коммерческие'
//'3','седельные тягачи'
//'4','прицепы'
//'5','грузовики'
//'6','автобусы'
//'7','бронеавтомобили'
//'8','автокраны'
//'9','бульдозеры'
//'10','автопогрузчики'
//'11','экскаваторы'
//'12','сельхоз-техника'
//'13','коммунальная'
//'14','самопогрузчики'
//'15','строительная техника'
//'16','ретро автомобили'
//'17','автодом'
//'18','трейлеры'
//'19','автодом жилой модуль'
//'20','мотоциклы'
//'21','багги'
//'22','мотовездеходы'
//'23','скутеры'
//'24','картинг'
//'25','вездеходы-амфибии'
//'26','снегоходы'
//'27','катера и яхты'
//'28','реактивные катера'
//'29','лодочные моторы'
//'30','гидроциклы'
//'31','лодки'
//'32','вертолёты'
//'33','самолёты'
//'34','велосипеды'
