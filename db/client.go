package db

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/utils"
	"gorm.io/gorm"
)

func (m *Manager) CreateClient(client *model.Client) error {
	return m.DB.Create(client).Error
}

func (m *Manager) GetClientByID(client_id uint) (client model.Client, err error) {
	db := m.DB
	db = db.Unscoped()
	db = db.Joins("Gender")
	db = db.Joins("ClientType")
	db = db.Joins("Referral")
	db = db.Joins("Address")
	db = db.Joins("Company")
	db = db.Preload("Company.Address")
	db = db.Preload("Deals", func(db *gorm.DB) *gorm.DB {
		db = db.Order("created_at desc")
		db = db.Preload("Cars.CarStatus")
		db = db.Preload("Cars.CarModel.CarMark")
		db = db.Preload("Cars.DealCar.CarInterest")
		db = db.Preload("Cars.DealCar.Relevance")
		db = db.Preload("Cars.CarCustomCharacteristicValues.CarCharacteristic")
		db = db.Preload("Cars.CarCustomCharacteristicValues.CarCharacteristicValue")
		return db
	})
	db = db.Joins("User")
	db = db.Preload("Deals.Communication")
	db = db.Preload("Deals.DealStatus")
	db = db.Preload("Deals.DealGoal")
	db = db.Preload("Deals.User", utils.Unscopedizer())
	db = db.Preload("Deals.User.Dealerships", utils.Unscopedizer())
	db = db.Preload("Deals.Contacts", func(db *gorm.DB) *gorm.DB {
		return db.Order("created_at desc")
	})
	db = db.Preload("Assessments.AssessmentStatus.AssessmentStage")
	db = db.Preload("Assessments.AssessmentPricingHistory.AssessmentStatus.AssessmentStage")
	db = db.Preload("Assessments.AssessmentPricing.AssessmentStatus.AssessmentStage")
	db = db.Preload("Assessments.Car.CarModification")
	db = db.Preload("Assessments.Car.CarModel.CarMark")
	db = db.Preload("DealGoal")
	err = db.First(&client, client_id).Error
	return
}

func (m *Manager) GetClients(queries map[string]interface{}, user model.User, limit, offset uint64) (clients []model.Client, err error) {
	db := m.DB

	var arrayOfConditions []string

	if val, ok := queries["name"]; ok {
		queries["name"] = fmt.Sprintf("%%%s%%", val)
		strFmt := `LOWER("client".name) LIKE LOWER(@name)`
		arrayOfConditions = append(arrayOfConditions, strFmt)
	}

	if onlyMy, ok := queries["onlyMy"]; ok {
		if isOnlyMy, _ := strconv.ParseBool(onlyMy.(string)); isOnlyMy == true {
			queries["userID"] = user.ID
			arrayOfConditions = append(arrayOfConditions, `"User".id = @userID`)
		}
	}

	if _, ok := queries["type"]; ok {
		strFmt := `"ClientType".name=@type`
		arrayOfConditions = append(arrayOfConditions, strFmt)
	}

	if _, ok := queries["referral"]; ok {
		strFmt := `"client".referral_id = @referral`
		arrayOfConditions = append(arrayOfConditions, strFmt)
	}

	if val, ok := queries["createdAt"]; ok {
		var regexSQLProtector = regexp.MustCompile(utils.SQLInjectPattern)
		arrayOfString := strings.Split(val.(string), ",")
		arrayOfString[0] = regexSQLProtector.ReplaceAllLiteralString(arrayOfString[0], "")
		arrayOfString[1] = regexSQLProtector.ReplaceAllLiteralString(arrayOfString[1], "")
		strFmt := fmt.Sprintf(`"client".created_at BETWEEN '%s' AND '%s'`, arrayOfString[0], arrayOfString[1])
		arrayOfConditions = append(arrayOfConditions, strFmt)
	}

	queries["isEmptyName"] = ""
	strFmt := `"client".name != @isEmptyName`
	arrayOfConditions = append(arrayOfConditions, strFmt)

	if len(arrayOfConditions) > 0 {
		conditions := fmt.Sprintf("%s", strings.Join(arrayOfConditions, " AND "))
		db = db.Where(conditions, queries)
	}

	err = db.Limit(int(limit)).Offset(int(offset)).Joins("Gender").Joins("User").Joins("ClientType").Joins("Referral").Joins("Address").Joins("DealGoal").Order("created_at desc").Find(&clients).Error
	return
}

func (m *Manager) GetFormattedClients() (clients []model.ClientInGetClients, err error) {
	sql := `
		SELECT
			DISTINCT ON (client.id)
			client.id "ClientID",
			client.telephone "Telephone",
			client.name "ClientName",
			client.surname "ClientSurname",
			deal.id	"DealID",
			users.first_name || ' ' || users.last_name "User",
			deal.description "Description",
			deal_car.deal_id "DealID",
			car.id "CarID",
			car.vin "Car",
			car_interest.description "CarInterest",
			contact.id "ContactID",
			contact.date "Contact"
		FROM client
		JOIN deal ON deal.client_id = client.id
		JOIN users ON deal.user_id = users.id
		JOIN deal_car ON deal_car.deal_id = deal.id
		JOIN car ON car.id = deal_car.car_id
		JOIN car_interest ON car_interest.id = deal_car.car_interest_id
		JOIN deal_contact ON deal_contact.deal_id = deal.id
		JOIN contact ON contact.id = deal_contact.contact_id
		WHERE client.deleted IS null;
	`
	err = m.DB.Raw(sql).Find(&clients).Error
	return
}

func (m *Manager) UpdateClient(client *model.Client) error {
	return m.DB.Omit("Assessment").Omit("User").Session(&gorm.Session{FullSaveAssociations: true}).Updates(client).Error
}

func (m *Manager) DeleteClient(clientID uint) error {
	client := model.Client{ID: clientID}
	return m.DB.First(&model.Client{}, clientID).Delete(&client).Error
	//return m.DB.Select(clause.Associations).Delete(&client).Error
}
