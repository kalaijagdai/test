package scheme

import (
	"time"

	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/utils"
	"gorm.io/gorm"
)

type ClientInUpdate struct {
	ID                uint              `json:"id"`
	Name              string            `json:"name"`
	Surname           string            `json:"surname"`
	Patronymic        string            `json:"patronymic"`
	Birthday          *time.Time        `json:"birthday"`
	IIN               string            `json:"iin"`
	Gender            *model.Gender     `json:"gender"`
	GenderID          uint              `json:"genderID"`
	UserID            uint              `json:"userID"`
	ClientType        *model.ClientType `json:"clientType"`
	ClientTypeID      uint              `json:"clientTypeID"`
	Referral          *model.Referral   `json:"referral"`
	ReferralID        uint              `json:"referralID"`
	Telephone         string            `json:"telephone"`
	WorkPhone         string            `json:"workPhone"`
	Email             string            `json:"email"`
	PassportID        string            `json:"passportID"`
	PassportAuthority string            `json:"passportAuthority"`
	PassportIssueDate *time.Time        `json:"passportIssueDate"`
	Address           *model.Address    `json:"address"`
	AddressID         uint              `json:"addressID"`
	Company           *model.Company    `json:"company"`
	CompanyID         uint              `json:"companyID"`
	Agreement         bool              `json:"agreement"`
	// Deals             []DealInResponse   `json:"deals"`
	// Assessments       []model.Assessment `json:"assessments"`
	DealGoalID *utils.FlexibleJsonUint `json:"dealGoalID"`
	DealGoal   *model.DealGoal         `json:"dealGoal"`
}

type ClientInCreate struct {
	ID                uint                    `json:"id"`
	Name              string                  `json:"name"`
	Surname           string                  `json:"surname"`
	Patronymic        string                  `json:"patronymic"`
	Birthday          string                  `json:"birthday"`
	IIN               string                  `json:"iin"`
	Gender            string                  `json:"gender"`
	User              *model.User             `json:"user"`
	ClientType        string                  `json:"clientType"`
	Referral          string                  `json:"referral"`
	Telephone         string                  `json:"telephone"`
	WorkPhone         string                  `json:"workPhone"`
	Email             string                  `json:"email"`
	PassportID        string                  `json:"passportID"`
	PassportAuthority string                  `json:"passportAuthority"`
	PassportIssueDate string                  `json:"passportIssueDate"`
	Agreement         bool                    `json:"agreement"`
	Address           model.Address           `json:"address"`
	Company           *model.Company          `json:"company"`
	DealGoalID        *utils.FlexibleJsonUint `json:"dealGoalID"`
}

type ClientPassportScanData struct {
	Name              string `json:"name"`
	Surname           string `json:"surname"`
	Patronymic        string `json:"patronymic"`
	Birthday          string `json:"birthday"`
	IIN               string `json:"iin"`
	PassportID        string `json:"passportID"`
	PassportAuthority string `json:"passportAuthority"`
	PassportIssueDate string `json:"passportIssueDate"`
	IsNewPassport     bool   `json:"isNew"`
}

type AssessmentClientInCreate struct {
	ID        uint   `json:"id"`
	Name      string `json:"name"`
	Telephone string `json:"telephone"`
}

type ClientInResponse struct {
	ID                uint                    `json:"id"`
	Name              string                  `json:"name"`
	Surname           string                  `json:"surname"`
	Patronymic        string                  `json:"patronymic"`
	Birthday          *time.Time              `json:"birthday"`
	IIN               string                  `json:"iin"`
	Gender            *model.Gender           `json:"gender"`
	GenderID          uint                    `json:"genderID"`
	User              *model.User             `json:"user"`
	UserID            uint                    `json:"userID"`
	ClientType        *model.ClientType       `json:"clientType"`
	ClientTypeID      uint                    `json:"clientTypeID"`
	Referral          *model.Referral         `json:"referral"`
	ReferralID        uint                    `json:"referralID"`
	Telephone         string                  `json:"telephone"`
	WorkPhone         string                  `json:"workPhone"`
	Email             string                  `json:"email"`
	PassportID        string                  `json:"passportID"`
	PassportAuthority string                  `json:"passportAuthority"`
	PassportIssueDate *time.Time              `json:"passportIssueDate"`
	Address           *model.Address          `json:"address"`
	AddressID         uint                    `json:"addressID"`
	Company           *model.Company          `json:"company"`
	CompanyID         uint                    `json:"companyID"`
	Agreement         bool                    `json:"agreement"`
	Deals             []DealInResponse        `json:"deals"`
	Assessments       []model.Assessment      `json:"assessments"`
	CreatedAt         *time.Time              `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt         *time.Time              `json:"updatedAt" sql:"DEFAULT:'now()'"`
	DealGoalID        *utils.FlexibleJsonUint `json:"dealGoalID"`
	DealGoal          *model.DealGoal         `json:"dealGoal"`
	Deleted           gorm.DeletedAt
}
