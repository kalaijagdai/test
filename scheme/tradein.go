package scheme

import (
	"time"
)

type TradeInInUpdate struct {
	ID             uint            `json:"id"`
	Price          *float32        `json:"price"`
	Discount       uint            `json:"discount"`
	Comment        string          `json:"comment"`
	AdditionalInfo string          `json:"additionalInfo"`
	AssessmentID   uint            `json:"assessmentID"`
	Client         *ClientInCreate `json:"client"`
	UserID         uint            `json:"userID"`
	CarID          uint            `json:"carID"`
	CarModelID     uint            `json:"carModelID" gorm:"REFERENCES car_models(id)"`
	SaleAt         *time.Time      `json:"saleAt"`
}
