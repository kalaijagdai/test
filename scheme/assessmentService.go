package scheme

type AssessmentServiceInCreate struct {
	Name      string   `json:"name" binding:"required"`
	Price     *float32 `json:"price"`
	ByCompany bool     `json:"byCompany"`
	Visible   bool     `json:"visible"`
}
