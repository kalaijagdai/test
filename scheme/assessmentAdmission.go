package scheme

import "gitlab.com/tradeincar/backend/api/utils"

// request scheme
type AsessmentAdmissionInUpdate struct {
	ID              uint                      `json:"id"`
	AssessmentID    uint                      `json:"assessmentID"`
	Client          *ClientInCreate           `json:"client"`
	SaleLink        string                    `json:"saleLink"`
	AssessmentPrice utils.FlexibleJsonFloat32 `json:"assessmentPrice"`
	AdmissionPrice  utils.FlexibleJsonFloat32 `json:"admissionPrice"`
	ContractNumber  string                    `json:"contractNumber"`
}
