package scheme

import (
	"encoding/json"
	"time"
)

type VinReportStatus struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}

type VinReportInAssessment struct {
	ID             uint             `json:"id"`
	Status         *VinReportStatus `json:"status"`
	OwnershipCount uint             `json:"ownershipCount"`
	HasRestrict    bool             `json:"hasRestrict"`
	IsSearch       bool             `json:"isSearch"`
	DtpCount       uint             `json:"dtpCount"`
	CreatedAt      *time.Time       `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt      *time.Time       `json:"updatedAt" sql:"DEFAULT:'now()'"`
}

type VinReportInResponse struct {
	ID             uint             `json:"id"`
	AssessmentID   uint             `json:"assessmentID"`
	StatusID       uint             `json:"statusID"`
	TaskID         uint             `json:"taskId"`
	Status         *VinReportStatus `json:"status"`
	OwnershipCount uint             `json:"ownershipCount"`
	HasRestrict    bool             `json:"hasRestrict"`
	IsSearch       bool             `json:"isSearch"`
	DtpCount       uint             `json:"dtpCount"`
	Data           json.RawMessage  `json:"data"`
	CreatedAt      *time.Time       `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt      *time.Time       `json:"updatedAt" sql:"DEFAULT:'now()'"`
}
