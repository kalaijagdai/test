package scheme

import "gitlab.com/tradeincar/backend/api/model"

type StatusChangeNotification struct {
	User *model.User `json:"user"`
	Car *model.Car `json:"car"`
	Dealership *model.Dealership `json:"dealership"`
	AssessmentStatus *model.AssessmentStatus `json:"assessmentStatus"`
}

type CreateDealershipNotification struct {
	User *model.User `json:"user"`
	Dealership *model.Dealership `json:"dealership"`
	Manager *model.User `json:"manager"`
	AssessmentStatus *model.AssessmentStatus `json:"assessmentStatus"`
}

type DealStatusChangeNotification struct {
	User *model.User `json:"user"`
	Deal *model.Deal `json:"deal"`
	Reason string `json:"reason"`
}

type NewClientNotification struct {
	User *model.User `json:"user"`
	Client *model.Client `json:"client"`
}