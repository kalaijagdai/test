package scheme

import (
	"time"

	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/utils"
)

type AssessmentInCreate struct {
	AssessmentTypeID uint                     `json:"assessmentTypeID"`
	DealershipID     uint                     `json:"dealershipID"`
	Car              CarInCreate              `json:"car"`
	CarInfo          model.CarInfo            `json:"carInfo"`
	Client           AssessmentClientInCreate `json:"client"`
}

type AssessmentInUpdate struct {
	ClientID       uint                      `json:"clientID"`
	MaxMarketValue utils.FlexibleJsonFloat32 `json:"maxMarketValue"`
	MinMarketValue utils.FlexibleJsonFloat32 `json:"minMarketValue"`
	SaleLink       string                    `json:"saleLink"`
}

type AssessmentManagerApproveRequest struct {
	PassportSeries      string     `json:"passportSeries"  binding:"required"`
	PassportNumber      string     `json:"passportNumber"  binding:"required"`
	PassportIssueDate   *time.Time `json:"passportIssueDate"  binding:"required"`
	NumberOfOwners      string     `json:"numberOfOwners"  binding:"required"`
	Mileage             uint       `json:"mileage"  binding:"required"`
	CarCondition        string     `json:"carCondition"  binding:"required"`
	CarBodyColor        string     `json:"carBodyColor"  binding:"required"`
	AssessmentPrice     *float32   `json:"assessmentPrice"  binding:"required"`
	PlannedSellingPrice *float32   `json:"plannedSellingPrice"  binding:"required"`
}

type AssessmentPricingRequest struct {
	AssessmentPrice     *float32 `json:"assessmentPrice"`
	PlannedSellingPrice *float32 `json:"plannedSellingPrice"`
	MarginPercent       *float32 `json:"marginPercent"`
	Margin              *float32 `json:"margin"`
}

type AssessmentInResponse struct {
	ID                 uint                     `json:"id"`
	DealershipID       uint                     `json:"dealershipID"`
	Dealership         *model.Dealership        `json:"dealership"`
	Car                *AssessmentCarInResponse `json:"car"`
	CarID              uint                     `json:"carID"`
	CarInfo            *model.CarInfo           `json:"carInfo"`
	CarInfoID          uint                     `json:"carInfoID"`
	Client             *model.Client            `json:"client"`
	ClientID           uint                     `json:"clientID"`
	AssessmentTypeID   uint                     `json:"assessmentTypeID"`
	AssessmentType     *model.AssessmentType    `json:"assessmentType"`
	AssessmentStatus   *model.AssessmentStatus  `json:"assessmentStatus"`
	AssessmentStatusID uint                     `json:"assessmentStatusID"`

	AssessmentGear      *model.AssessmentGear      `json:"assessmentGear"`
	AssessmentAdmission *model.AssessmentAdmission `json:"assessmentAdmission"`

	AssessmentReport []model.AssessmentReport `json:"assessmentReport"`

	AssessmentServices  []model.AssessmentService  `json:"assessmentServices"`
	AssessmentAdditions []model.AssessmentAddition `json:"assessmentAdditions"`

	AssessmentPricing        []model.AssessmentPricing        `json:"assessmentPricing"`
	AssessmentPricingHistory []model.AssessmentPricingHistory `json:"assessmentPricingHistory"`

	AssessmentPhotos    []model.AssessmentPhoto    `json:"assessmentPhotos"`
	AssessmentDocuments []model.AssessmentDocument `json:"assessmentDocuments"`

	AppropriateStatuses []string `json:"appropriateStatuses"`

	TradeIn  *TradeInAssessment `json:"tradeIn"`
	Sale     *model.Sale        `json:"sale"`
	Reserves []model.Reserve    `json:"reserves"`

	MaxMarketValue *float32 `json:"maxMarketValue"`
	MinMarketValue *float32 `json:"minMarketValue"`
	SaleLink       string   `json:"saleLink"`

	VinReport []VinReportInAssessment `json:"vinReport"`

	CreatedAt *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
}

type AssessmentStatusInPost struct {
	Reason string `json:"reason"`
}

type TradeInAssessment struct {
	ID             uint            `gorm:"primaryKey" json:"id"`
	Price          *float32        `json:"price"`
	Discount       uint            `json:"discount"`
	Comment        string          `json:"comment"`
	AdditionalInfo string          `json:"additionalInfo"`
	AssessmentID   uint            `gorm:"REFERENCES assessment(id)" json:"assessmentID"`
	ClientID       uint            `gorm:"REFERENCES client(id)" json:"clientID"`
	Client         *model.Client   `json:"client"`
	UserID         uint            `gorm:"REFERENCES users(id)" json:"userID"`
	User           *model.User     `json:"user"`
	CarID          uint            `gorm:"REFERENCES car(id)" json:"carID"`
	Car            *CarInResponse  `json:"car"`
	CarModelID     uint            `json:"carModelID" gorm:"REFERENCES car_models(id)"`
	CarModel       *model.CarModel `json:"carModel"`
	SaleAt         *time.Time      `json:"saleAt" sql:"DEFAULT:'now()'"`
	CreatedAt      *time.Time      `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt      *time.Time      `json:"updatedAt" sql:"DEFAULT:'now()'"`
}
