package scheme

import (
	"time"

	"gitlab.com/tradeincar/backend/api/model"
)

type SaleInUpdate struct {
	ID             uint               `json:"id"`
	Price          *float32           `json:"price"`
	ContractNumber string             `json:"contractNumber"`
	AssessmentID   uint               `json:"assessmentID"`
	Client         *ClientInCreate    `json:"client"`
	UserID         uint               `json:"userID"`
	IsCASCO        bool               `json:"isCASCO"`
	IsCredit       bool               `json:"isCredit"`
	SalesChannelID uint               `json:"salesChannelID"`
	SalesChannel   model.SalesChannel `json:"salesChannel"`
	SaleAt         *time.Time         `json:"saleAt"`
	IssueAt        *time.Time         `json:"issueAt"`
}
