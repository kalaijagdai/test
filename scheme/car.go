package scheme

import (
	"time"
)

type CarInCreate struct {
	ID                uint   `json:"id"`
	VIN               string `json:"vin"`
	Year              string `json:"year"`
	CarInterest       string `json:"carInterest"`
	CarStatus         string `json:"carStatus"`
	CarModelID        uint   `json:"carModelID"`
	CarModificationID uint   `json:"carModificationID"`
	EngineType        string `json:"engineType"`
	EngineVolume      string `json:"engineVolume"`
	EnginePower       string `json:"enginePower"`
	KppType           string `json:"kppType"`
	DriveWheelType    string `json:"driveWheelType"`

	CarBodyStyleType string `json:"carBodyStyleType"`
	DoorCount        string `json:"doorCount"`
	SteeringType     string `json:"steeringType"`

	Relevance string `json:"relevance"`
	Comment   string `json:"comment"`
}

type CarInResponse struct {
	CarID                       uint     `json:"carID"`
	AssessmentID                uint     `json:"assessmentID"`
	AssessmentStageDescription  string   `json:"assessmentStageDescription"`
	AssessmentStatusDescription string   `json:"assessmentStatusDescription"`
	AssessmentStatusReason      string   `json:"assessmentStatusReason"`
	AssessmentTypeDescription   string   `json:"assessmentTypeDescription"`
	CarBodyColor                string   `json:"carBodyColor"`
	CarBodyStyleTypeName        string   `json:"carBodyStyleTypeName"`
	CarMark                     string   `json:"carMark"`
	CarModelID                  uint     `json:"carModelID"`
	CarModel                    string   `json:"carModel"`
	Year                        string   `json:"year"`
	CarModification             string   `json:"carModification"`
	DoorCount                   string   `json:"doorCount"`
	DriveWheelTypeName          string   `json:"driveWheelTypeName"`
	EnginePower                 string   `json:"enginePower"`
	EngineTypeName              string   `json:"engineTypeName"`
	EngineVolume                string   `json:"engineVolume"`
	FavoritePhoto               string   `json:"favoritePhoto"`
	KppTypeName                 string   `json:"kppTypeName"`
	Mileage                     uint     `json:"mileage"`
	MileageType                 string   `json:"mileageType"`
	PlannedSellingPrice         *float32 `json:"plannedSellingPrice"`
	Price                       *float32 `json:"price"`
	SaleAt                      string   `json:"saleAt"`
	VIN                         string   `json:"vin"`
	CreatedAt                   string   `json:"createdAt"`
}

type AssessmentCarInResponse struct {
	ID            uint   `json:"id"`
	VIN           string `json:"vin"`
	CarMark       string `json:"carMark"`
	CarModel      string `json:"carModel"`
	CarGeneration string `json:"carGeneration"`

	Year               *time.Time `json:"year"`
	CarModification    string     `json:"carModification"`
	EngineTypeName     string     `json:"engineTypeName"`
	EngineVolume       string     `json:"engineVolume"`
	EnginePower        string     `json:"enginePower"`
	KppTypeName        string     `json:"kppTypeName"`
	DriveWheelTypeName string     `json:"driveWheelTypeName"`

	CarBodyStyleTypeName string `json:"carBodyStyleTypeName"`
	DoorCount            string `json:"doorCount"`
	SteeringTypeName     string `json:"steeringType"`
}

type CarModificationInResponse struct {
	ID               uint   `json:"id"`
	Name             string `json:"name"`
	IsFromBasebuy    bool   `json:"isFromBasebuy"`
	EngineType       string `json:"engineType"`
	EngineVolume     string `json:"engineVolume"`
	EnginePower      string `json:"enginePower"`
	KppType          string `json:"kppTypeName"`
	DriveWheelType   string `json:"driveWheelType"`
	CarBodyStyleType string `json:"carBodyStyleType"`
	DoorCount        string `json:"doorCount"`
	SteeringType     string `json:"steeringType"`
}
