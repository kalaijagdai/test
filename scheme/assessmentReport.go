package scheme

import "gitlab.com/tradeincar/backend/api/model"

type AssessmentReportInCreate struct {
	ID						uint							`json:"id"`
	Name					string							`json:"name"`
	AssessmentID			uint							`json:"assessmentID"`
	AssessmentStatusID 		uint             				`json:"assessmentStatusID"`
	UserID					uint							`json:"userID"`
	Notes					string							`json:"notes"`
	File					string							`json:"file"`
	AssessmentInspection	[]model.AssessmentInspection	`json:"inspections"`
}

type AssessmentReportInUpdate struct {
	ID						uint							`json:"id"`
	Name					string							`json:"name"`
	AssessmentID			uint							`json:"assessmentID"`
	AssessmentStatusID 		uint             				`json:"assessmentStatusID"`
	UserID					uint							`json:"userID"`
	Notes					string							`json:"notes"`
	File					string							`json:"file"`
	AssessmentInspection	[]model.AssessmentInspection	`json:"inspections"`
}
