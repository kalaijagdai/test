package scheme

type ReserveInUpdate struct {
	ID           uint    `json:"id"`
	AssessmentID uint    `json:"assessmentID"`
	Client     *ClientInCreate    `json:"client"`
	IsPaid       bool    `json:"isPaid"`
	IsTestDrive           bool `json:"isTestDrive"`
	IsCASCO               bool `json:"isCASCO"`
	IsLeasing             bool `json:"isLeasing"`
	IsCredit              bool `json:"isCredit"`
	IsTradeIn             bool `json:"isTradeIn"`
	IsAdditionalEquipment bool `json:"isAdditionalEquipment"`
	IsTechnicalGuarantee  bool `json:"isTechnicalGuarantee"`
	IsCarPurchaseOnly     bool `json:"isCarPurchaseOnly"`
}
