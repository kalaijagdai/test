package scheme

import (
	"time"

	"gitlab.com/tradeincar/backend/api/model"
)

// request scheme
type DealInCreate struct {
	Client   *DealClientInCreate `json:"client"`
	ClientID uint                `json:"clientID"`

	Cars []CarInCreate `json:"cars"`

	DealershipID uint `json:"dealershipID"`

	UserID uint `json:"userID"`
	User uint `json:"user"`

	Contact *model.ContactRequest `json:"contact"`

	// information
	Description   string `json:"description" binding:"required"`
	DealGoal      string `json:"dealGoal" binding:"required"`
	Communication string `json:"communication" binding:"required"`
	Relevance     string `json:"relevance"`
}

type DealClientInCreate struct {
	ID        uint   `json:"id"`
	Name      string `json:"name"`
	Telephone string `json:"telephone"`
	Referral string `json:"referral"`
}

type DealInResponse struct {
	ID uint `gorm:"primaryKey" json:"id"`

	Client   *model.Client `json:"client"`
	ClientID uint          `json:"clientID"`

	Dealership   *model.Dealership `json:"dealership"`
	DealershipID uint          `json:"dealershipID"`

	Cars []DealCarInResponse `json:"cars"`

	User   model.User `json:"user"`
	UserID uint       `json:"userID"`

	Contacts []model.Contact `json:"contacts"`

	// information
	Description string `json:"description"`

	DealGoal   model.DealGoal `json:"dealGoal"`
	DealGoalID uint           `json:"dealGoalID"`

	DealStatus   model.DealStatus `json:"dealStatus"`
	DealStatusID uint             `json:"dealStatusID"`

	CommunicationID uint                `json:"communicationID" `
	Communication   model.Communication `json:"communication"`

	CreatedAt *time.Time `json:"createdAt"`
	UpdatedAt *time.Time `json:"updatedAt"`
}

type DealCarInResponse struct {
	ID       uint   `json:"id"`
	VIN      string `json:"vin"`
	CarMark  string `json:"carMark"`
	CarModel string `json:"carModel"`

	Year               *time.Time `json:"year"`
	CarModification    string     `json:"carModification"`
	EngineTypeName     string     `json:"engineTypeName"`
	EngineVolume       string     `json:"engineVolume"`
	EnginePower        string     `json:"enginePower"`
	KppTypeName        string     `json:"kppTypeName"`
	DriveWheelTypeName string     `json:"driveWheelTypeName"`

	CarBodyStyleTypeName string `json:"carBodyStyleTypeName"`
	DoorCount            string `json:"doorCount"`
	SteeringTypeName     string `json:"steeringType"`

	CarStatus   string `json:"carStatus"`
	CarInterest string `json:"carInterest"`
	Relevance   string `json:"relevance"`
	Comment     string `json:"comment"`
}
