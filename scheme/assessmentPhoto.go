package scheme

type AssessmentPhotoInCreate struct {
	URL                 string `json:"url"`
	AssessmentPhotoType string `json:"assessmentPhotoType"`
}
