package scheme

type InspectionResponse struct {
	Name					string						`json:"name"`
	InnerInspections		[]InnerInspectionResponse	`json:"innerInspections"`
}

type InnerInspectionResponse struct {
	Name					string							`json:"name"`
	InnerInspectionViews	[]InnerInspectionViewResponse	`json:"innerInspectionViews"`
}

type InnerInspectionViewResponse struct {
	Name	string		`json:"name"`
}