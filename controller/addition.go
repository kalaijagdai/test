package controller

import (
	excptn "gitlab.com/tradeincar/backend/api/utils"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
)

func (ctr *Controller) CreateAddition(c *gin.Context) {
	// get id from param
	dealershipIDString := c.Param("id")
	u64, err := strconv.ParseUint(dealershipIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	dealershipID := uint(u64)

	// bind with model
	var addition model.Addition
	err = c.Bind(&addition)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	if dealershipID > 0 {
		addition.DealershipID = dealershipID
	}

	err = ctr.Service.Manager.CreateAddition(&addition)
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, &addition)
}

func (ctr *Controller) GetAdditions(c *gin.Context) {
	additions, err := ctr.Service.Manager.GetAdditions()

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, additions)
}

func (ctr *Controller) GetDealershipAdditions(c *gin.Context) {
	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	dealershipID := uint(u64)

	additions, err := ctr.Service.Manager.GetDealershipAdditions(dealershipID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, additions)
}


func (ctr *Controller) GetAddition(c *gin.Context) {
	additionIDString := c.Param("id")
	u64, err := strconv.ParseUint(additionIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	additionID := uint(u64)

	addition, err := ctr.Service.Manager.GetAdditionByID(additionID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, addition)
}

func (ctr *Controller) UpdateAddition(c *gin.Context) {
	id := c.Param("id")
	var additionToUpdate model.Addition
	err := c.BindJSON(&additionToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	additionToUpdate.ID = uint(u64)

	err = ctr.Service.Manager.UpdateAddition(&additionToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, &additionToUpdate)
}

func (ctr *Controller) DeleteAddition(c *gin.Context) {
	additionIDString := c.Param("id")
	u64, err := strconv.ParseUint(additionIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	additionID := uint(u64)

	err = ctr.Service.Manager.DeleteAddition(additionID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, true)
}
