package controller

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/utils"
	excptn "gitlab.com/tradeincar/backend/api/utils"
)

func (ctr *Controller) CreateVinReport(c *gin.Context) {
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		exp, errMessage := excptn.New(3, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	assessmentID := uint(u64)

	// preload vinReportStatus
	vinReportStatus, err := ctr.Service.Dict.GetVinReportStatusByName("В очереди")
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	var vinReport model.VinReport
	vinReport.AssessmentID = assessmentID
	vinReport.StatusID = vinReportStatus.ID

	err = ctr.Service.Manager.CreateVinReport(&vinReport)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	vinReport.Status = &vinReportStatus

	c.JSON(200, vinReport)
}

func (ctr *Controller) GetVinReportByID(c *gin.Context) {
	vinReportIDString := c.Param("id")
	u64, err := strconv.ParseUint(vinReportIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	vinReportID := uint(u64)

	vinReport, err := ctr.Service.Manager.GetVinReportByID(vinReportID)

	var vinReportInResponse scheme.VinReportInResponse

	err = utils.StructTransformer(vinReport, &vinReportInResponse)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	if vinReport.Data != "" {
		vinReportInResponse.Data = json.RawMessage(vinReport.Data)
	}

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, vinReportInResponse)
}

func (ctr *Controller) GetAllVinReportByAssessmentID(c *gin.Context) {
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		exp, errMessage := excptn.New(3, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	assessmentID := uint(u64)

	vinReports, err := ctr.Service.Manager.GetAllVinReportByAssessmentID(assessmentID)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, vinReports)
}
