package controller

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/useCases"
	"gitlab.com/tradeincar/backend/api/utils"
	excptn "gitlab.com/tradeincar/backend/api/utils"
)

//create assessment
func (ctr *Controller) CreateAssessment(c *gin.Context) {
	user := c.MustGet("user").(model.User)

	var assessment scheme.AssessmentInCreate
	err := c.Bind(&assessment)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	//checkForCar
	var car model.Car
	if assessment.Car.ID < 1 {
		assessment.Car.CarStatus = "ON_ASSESSMENT"
		car, err = useCases.CreateAssessmentCarEntity(ctr.Service, &assessment.Car)
		if err != nil {
			exp, errMessage := excptn.New(201, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	} else {
		_, err := ctr.Service.Manager.GetCarByID(assessment.Car.ID)
		if err != nil {
			exp, errMessage := excptn.New(200, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}

		car.ID = assessment.Car.ID
	}

	//check for car info
	if assessment.CarInfo.ID < 1 {
		err = useCases.CreateCarInfoEntity(ctr.Service, &assessment.CarInfo)
	}
	//
	var client model.Client
	if assessment.Client.ID < 1 {
		client, _ = useCases.CreateAssessmentClientEntity(ctr.Service, &assessment.Client, &user)
	} else {
		client.ID = assessment.Client.ID
	}

	assessmentCreated, err := useCases.CreateAssessmentEntity(ctr.Service, car.ID, assessment.CarInfo.ID, client.ID, assessment.AssessmentTypeID, assessment.DealershipID, user.ID)
	if err != nil {
		exp, errMessage := excptn.New(100, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, assessmentCreated)
}

//clone assessment
func (ctr *Controller) CloneAssessment(c *gin.Context) {
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		exp, errMessage := excptn.New(3, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	assessmentSrcID := uint(u64)

	assessmentSrc, err := ctr.Service.Manager.GetAssessmentByID(assessmentSrcID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	assessmentDst, err := useCases.CreateAssessmentEntity(ctr.Service, assessmentSrc.Car.ID, assessmentSrc.CarInfo.ID, assessmentSrc.Client.ID, assessmentSrc.AssessmentTypeID, assessmentSrc.DealershipID, assessmentSrc.UserID)
	if err != nil {
		exp, errMessage := excptn.New(100, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, assessmentDst)
}

//get all assessments
func (ctr *Controller) GetAssessments(c *gin.Context) {
	assessments, err := ctr.Service.Manager.GetAssessments()

	if err != nil {
		exp, errMessage := excptn.New(101, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, assessments)
}

//get assessment by id
func (ctr *Controller) GetAssessment(c *gin.Context) {
	me := c.MustGet("user").(model.User)
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		exp, errMessage := excptn.New(3, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	assessmentID := uint(u64)

	assessment, err := ctr.Service.Manager.GetAssessmentByID(assessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	var assessmentInResponse scheme.AssessmentInResponse

	customCharacteristicValMap := utils.CustomCharacteristicValsMapper(assessment.Car.CarCustomCharacteristicValues)

	if assessment.Car.CarModification == nil {
		assessment.Car.CarModification = &model.CarModification{
			Name: "(Другая модификация)",
			CarSerie: &model.CarSerie{
				CarGeneration: &model.CarGeneration{
					Name: "(Другое поколение)",
				},
			},
		}
	}

	fmtCar := scheme.AssessmentCarInResponse{
		ID:                   assessment.Car.ID,
		VIN:                  assessment.Car.VIN,
		Year:                 assessment.Car.Year,
		CarMark:              assessment.Car.CarModel.CarMark.Name,
		CarModel:             assessment.Car.CarModel.Name,
		CarGeneration:        assessment.Car.CarModification.CarSerie.CarGeneration.Name,
		CarModification:      assessment.Car.CarModification.Name,
		EngineTypeName:       customCharacteristicValMap[12],
		EngineVolume:         customCharacteristicValMap[13],
		EnginePower:          customCharacteristicValMap[14],
		KppTypeName:          customCharacteristicValMap[24],
		CarBodyStyleTypeName: customCharacteristicValMap[2],
		DriveWheelTypeName:   customCharacteristicValMap[27],
		DoorCount:            customCharacteristicValMap[3],
		SteeringTypeName:     customCharacteristicValMap[53],
	}

	assessment.Car = nil

	var assessmentTradeIn *model.TradeIn
	tradeInAssesmentTypes := []string{"TRADE_IN_NEW", "TRADE_IN_MILEAGE"}
	if _, isTradeIn := utils.FindInSlice(tradeInAssesmentTypes, assessment.AssessmentType.Name); isTradeIn && assessment.TradeIn != nil {
		assessmentTradeIn = assessment.TradeIn
		assessment.TradeIn = nil
	}

	err = utils.StructTransformer(assessment, &assessmentInResponse)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	assessmentInResponse.Car = &fmtCar

	if assessmentTradeIn != nil {
		utils.StructTransformer(assessmentTradeIn, &assessmentInResponse.TradeIn)
		if assessmentInResponse.AssessmentType.Name == "TRADE_IN_MILEAGE" {
			carModName := "(Другая модификация)"
			if assessmentTradeIn.Car.CarModification != nil {
				carModName = assessmentTradeIn.Car.CarModification.Name
			}
			assessmentInResponse.TradeIn.Car = &scheme.CarInResponse{
				CarID:           assessmentTradeIn.Car.ID,
				VIN:             assessmentTradeIn.Car.VIN,
				CarMark:         assessmentTradeIn.Car.CarModel.CarMark.Name,
				CarModel:        assessmentTradeIn.Car.CarModel.Name,
				CarModification: carModName,
				Year:            assessmentTradeIn.Car.Year.Format("2006"),
			}
		}
	}

	if _, isTradeIn := utils.FindInSlice(tradeInAssesmentTypes, assessment.AssessmentType.Name); isTradeIn && assessmentInResponse.TradeIn == nil {
		assessmentInResponse.TradeIn = &scheme.TradeInAssessment{
			Client: assessment.Client,
		}
	}

	if assessment.AssessmentAdmission != nil && assessment.AssessmentAdmission.Client == nil {
		assessmentInResponse.AssessmentAdmission.Client = assessment.Client
	}

	// send appropriate statuses
	appropriateStatuses, err := utils.GetAppropriateAssessmentStatuses(assessment.AssessmentType.Name, assessment.Dealership.ManagerAffirmation, assessment.Dealership.SaleTransfer, assessment.AssessmentStatus.Name, me.Role.Name)
	if err != nil {
		exp, errMessage := excptn.New(161, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	assessmentInResponse.AppropriateStatuses = appropriateStatuses

	// ! hotfix, if status reserve, only owner can edit assessment
	if assessment.AssessmentStatus.Name == "RESERVE" && me.Role.Name == "SPECIALIST" && assessment.UserID != me.ID {
		assessmentInResponse.AppropriateStatuses = nil
	}
	// reservesLen := len(assessment.Reserves)
	// if assessment.AssessmentType.Name == "RESERVE" && reservesLen > 0 {
	// 	reserveOwnerID := assessment.Reserves[0].UserID
	// 	if reserveOwnerID != me.ID {
	// 		assessmentInResponse.AppropriateStatuses = nil
	// 	}
	// }

	c.JSON(200, assessmentInResponse)
}

//update assessment car info
func (ctr *Controller) UpdateAssessmentCarInfo(c *gin.Context) {
	paramID := c.Param("id")
	id, err := strconv.ParseUint(paramID, 10, 32)
	if err != nil {
		exp, errMessage := excptn.New(3, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	assessmentID := uint(id)

	assessment, err := ctr.Service.Manager.GetAssessmentByID(assessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	var carInfo model.CarInfo
	err = c.Bind(&carInfo)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	carInfo.ID = assessment.CarInfoID
	err = ctr.Service.Manager.UpdateAssessmentCarInfo(&carInfo)
	if err != nil {
		exp, errMessage := excptn.New(151, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, nil)

}

//add assessment price
func (ctr *Controller) AddAssessmentPrice(c *gin.Context) {
	paramID := c.Param("id")
	id, err := strconv.ParseUint(paramID, 10, 32)
	if err != nil {
		exp, errMessage := excptn.New(3, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	assessmentID := uint(id)

	assessment, err := ctr.Service.Manager.GetAssessmentByID(assessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	var assessmentPrice scheme.AssessmentPricingRequest
	err = c.Bind(&assessmentPrice)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	assessmentPriceToReturn, err := useCases.CalculateAssessmentPrice(ctr.Service, assessment, assessmentPrice)
	if err != nil {
		exp, errMessage := excptn.New(153, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	history := model.AssessmentPricingHistory{
		AssessmentPrice:     assessmentPrice.AssessmentPrice,
		PlannedSellingPrice: assessmentPrice.PlannedSellingPrice,
		AssessmentStatusID:  assessment.AssessmentStatusID,
		AssessmentID:        assessment.ID,
	}
	err = ctr.Service.Manager.CreateAssessmentPricingHistory(&history)
	if err != nil {
		exp, errMessage := excptn.New(152, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	assessmentStatus, err := ctr.Service.Dict.GetAssessmentStatusByID(assessmentPriceToReturn.AssessmentStatusID)
	if err != nil {
		exp, errMessage := excptn.New(109, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	assessmentPriceToReturn.AssessmentStatus = &assessmentStatus

	c.JSON(200, assessmentPriceToReturn)

}

func (ctr *Controller) UpdateAssessment(c *gin.Context) {
	id := c.Param("id")
	var assessmentToUpdate scheme.AssessmentInUpdate
	err := c.BindJSON(&assessmentToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)

	data, err := json.Marshal(assessmentToUpdate)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	assessment := model.Assessment{}

	err = json.Unmarshal(data, &assessment)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	assessment.ID = assessmentID

	err = ctr.Service.Manager.UpdateAssessment(&assessment)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, &assessmentToUpdate)
}

func (ctr *Controller) DeleteAssessment(c *gin.Context) {
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)

	err = ctr.Service.Manager.DeleteAssessment(assessmentID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, true)
}

//SEND ASSESSMENT TO MANAGER APPROVE
//TODO add notification to manager
func (ctr *Controller) SendOnManagerApprove(c *gin.Context) {
	//paramID := c.Param("id")
	//id, err := strconv.ParseUint(paramID, 10, 32)
	//if err != nil {
	//	exp, errMessage := excptn.New(3, err)
	//	c.JSON(exp.HTTPCode, gin.H{
	//		"code": exp.Code,
	//		"message": exp.Message,
	//		"error": errMessage,
	//	})
	//	return
	//}
	//assessmentToUpdateID := uint(id)
	//
	//var assessmentManagerApproveRequest scheme.AssessmentManagerApproveRequest
	//err = c.Bind(&assessmentManagerApproveRequest)
	//if err != nil {
	//	exp, errMessage := excptn.New(1, err)
	//	c.JSON(exp.HTTPCode, gin.H{
	//		"code": exp.Code,
	//		"message": exp.Message,
	//		"error": errMessage,
	//	})
	//	return
	//}
	//
	//assessmentByID, err := ctr.Service.Manager.GetAssessmentByID(assessmentToUpdateID)
	//if err != nil {
	//	exp, errMessage := excptn.New(102, err)
	//	c.JSON(exp.HTTPCode, gin.H{
	//		"code": exp.Code,
	//		"message": exp.Message,
	//		"error": errMessage,
	//	})
	//	return
	//}
	//
	//assessmentOnWork, _ := ctr.Service.Dict.GetAssessmentStatusByName("ON_WORK")
	//if assessmentByID.AssessmentStatusID != assessmentOnWork.ID {
	//	exp, _ := excptn.New(103, nil)
	//	c.JSON(exp.HTTPCode, gin.H{
	//		"code": exp.Code,
	//		"message": exp.Message,
	//		"error": "",
	//	})
	//	return
	//}
	//
	//
	//onManagerApproveStatus, _ := ctr.Service.Dict.GetAssessmentStatusByName("ON_MANAGER_APPROVAL")
	//
	//assessment := model.Assessment{
	//	ID:                  assessmentByID.ID,
	//	AssessmentStatusID:  onManagerApproveStatus.ID,
	//	PassportSeries:      assessmentManagerApproveRequest.PassportSeries,
	//	PassportNumber:      assessmentManagerApproveRequest.PassportNumber,
	//	PassportIssueDate:   assessmentManagerApproveRequest.PassportIssueDate,
	//	NumberOfOwners:      assessmentManagerApproveRequest.NumberOfOwners,
	//	Mileage:             assessmentManagerApproveRequest.Mileage,
	//	CarCondition:        assessmentManagerApproveRequest.CarCondition,
	//	CarBodyColor:        assessmentManagerApproveRequest.CarBodyColor,
	//	AssessmentPrice:     assessmentManagerApproveRequest.AssessmentPrice,
	//	PlannedSellingPrice: assessmentManagerApproveRequest.PlannedSellingPrice,
	//}
	//
	//err = ctr.Service.Manager.UpdateAssessment(&assessment)
	//if err != nil {
	//	exp, errMessage := excptn.New(104, err)
	//	c.JSON(exp.HTTPCode, gin.H{
	//		"code": exp.Code,
	//		"message": exp.Message,
	//		"error": errMessage,
	//	})
	//	return
	//}

	c.JSON(200, nil)
}

//APPROVE ASSESSMENT (ASSESSMENT PRICE/PLANNED SELL PRICE)(ONLY MANAGER)
func (ctr *Controller) ApproveAssessment(c *gin.Context) {
	paramID := c.Param("id")
	id, err := strconv.ParseUint(paramID, 10, 32)
	if err != nil {
		exp, errMessage := excptn.New(3, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	assessmentToUpdateID := uint(id)

	assessmentByID, err := ctr.Service.Manager.GetAssessmentByID(assessmentToUpdateID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	onManagerApproveStatus, _ := ctr.Service.Dict.GetAssessmentStatusByName("ON_MANAGER_APPROVAL")

	//TODO check for user role
	//if user role is manager no check for status
	if false {
		if assessmentByID.AssessmentStatusID != onManagerApproveStatus.ID {
			exp, errMessage := excptn.New(102, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	}

	assessmentApproved, _ := ctr.Service.Dict.GetAssessmentStatusByName("ASSESSMENT_APPROVED")
	assessment := model.Assessment{
		ID:                 assessmentByID.ID,
		AssessmentStatusID: assessmentApproved.ID,
	}
	err = ctr.Service.Manager.UpdateAssessment(&assessment)
	if err != nil {
		exp, errMessage := excptn.New(104, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, assessment)
}

func (ctr *Controller) ChangeStatus(c *gin.Context) {

}

func (ctr *Controller) GetPricingByAssessmentID(c *gin.Context) {
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)

	documents, err := ctr.Service.Manager.GetPricingByAssessmentID(assessmentID)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, documents)
}

func (ctr *Controller) GetAssessmentPricingHistory(c *gin.Context) {
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)

	assessmentPricingHistory, err := ctr.Service.Manager.GetAssessmentPricingHistory(assessmentID)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, assessmentPricingHistory)
}
