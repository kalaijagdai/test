package controller

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	excptn "gitlab.com/tradeincar/backend/api/utils"
)

func (ctr *Controller) AddAssessmentGear(c *gin.Context) {
	// get id from param
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)

	// bind request with scheme.AssessmentGearInCreate
	var assessmentGear model.AssessmentGear
	err = c.Bind(&assessmentGear)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	err = ctr.Service.Manager.AddAssessmentGear(assessmentID, &assessmentGear)
	if err != nil {
		exp, errMessage := excptn.New(406, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, &assessmentGear)
}

func (ctr *Controller) UpdateAssessmentGear(c *gin.Context) {
	var assessmentGearToUpdate model.AssessmentGear
	err := c.BindJSON(&assessmentGearToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	err = ctr.Service.Manager.UpdateAssessmentGear(&assessmentGearToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, &assessmentGearToUpdate)
}

func (ctr *Controller) DeleteAssessmentGear(c *gin.Context) {
	var assessmentGear model.AssessmentGear
	err := c.Bind(&assessmentGear)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// remove assessmentGear from Assessment
	err = ctr.Service.Manager.DeleteAssessmentGear(&assessmentGear)
	if err != nil {
		exp, errMessage := excptn.New(107, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, true)
}
