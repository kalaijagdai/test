package controller

import (
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
)

func (ctr *Controller) CreateCarStatus(c *gin.Context) {
	carIDString := c.Param("id")
	var carStatusRequest model.CarStatusRequest
	err := c.Bind(&carStatusRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	u64, err := strconv.ParseUint(carIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	carID := uint(u64)

	// get enum
	carStatus, err := ctr.Service.Dict.GetCarStatusByName(carStatusRequest.CarStatusName)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	// add CarStatus to Car in DB
	err = ctr.Service.Manager.UpdateCar(carID, map[string]interface{}{"car_status_id": carStatus.ID, "car_status": carStatus})
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, carStatus)
}

// func (ctr *Controller) GetCarStatuses(c *gin.Context) {
// 	CarStatuses,  := ctr.Service.Manager.GetCarStatuses()

// 	if err != nil {
// 		c.JSON(400, gin.H{
// 			"message": err.Error(),
// 		})
// 		return
// 	}

// 	c.JSON(200, CarStatuses)
// }
