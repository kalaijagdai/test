package controller

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/useCases"
	excptn "gitlab.com/tradeincar/backend/api/utils"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
)

func (ctr *Controller) AddAssessmentAddition(c *gin.Context) {
	// get id from param
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)

	assessment, err := ctr.Service.Manager.GetAssessmentByID(assessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// check if status is right
	assessmentStatusString := strings.ToUpper(c.Param("status"))
	status, err := ctr.Service.Dict.GetAssessmentStatusByName(assessmentStatusString)
	if err != nil {
		exp, errMessage := excptn.New(109, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	if status.Name != "ON_WORK" && status.Name != "DIAGNOSTICS" && status.Name != "SALE_PREPARATION" {
		err = errors.New("Not satisfied with status")
		exp, errMessage := excptn.New(154, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	if assessment.AssessmentStatusID != status.ID || assessment.AssessmentStatus.AssessmentStageID != status.AssessmentStageID {
		err = errors.New("Not satisfied with status")
		exp, errMessage := excptn.New(154, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// bind request with scheme.AssessmentAdditionInCreate
	var assessmentAdditionRequest scheme.AssessmentAdditionInCreate
	err = c.Bind(&assessmentAdditionRequest)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// create assessmentAddition model from request
	assessmentAddition := model.AssessmentAddition{
		Name:      assessmentAdditionRequest.Name,
		Price:     assessmentAdditionRequest.Price,
		ByCompany: assessmentAdditionRequest.ByCompany,
		Visible:   assessmentAdditionRequest.Visible,
	}

	// set assessment status for assessmentAddition
	assessmentStatus, _ := ctr.Service.Dict.GetAssessmentStatusByName(assessmentStatusString)
	assessmentAddition.AssessmentStatusID = assessmentStatus.ID

	err = ctr.Service.Manager.AddAssessmentAddition(assessmentID, &assessmentAddition)
	if err != nil {
		exp, errMessage := excptn.New(106, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	assessmentAddition.AssessmentStatusID = status.ID
	assessmentAddition.AssessmentStatus = &status

	assessment.AssessmentAdditions = append(assessment.AssessmentAdditions, assessmentAddition)

	err = useCases.RecalculateMarginOfAssessmentPrice(ctr.Service, assessment)
	if err != nil {
		exp, errMessage := excptn.New(155, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, &assessmentAddition)
}

func (ctr *Controller) UpdateAssessmentAddition(c *gin.Context) {
	var assessmentAdditionToUpdate model.AssessmentAddition
	err := c.BindJSON(&assessmentAdditionToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	//TODO low priority - check if addition status equal to current assessment status

	err = ctr.Service.Manager.UpdateAssessmentAddition(&assessmentAdditionToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	assessment, err := ctr.Service.Manager.GetAssessmentByID(assessmentAdditionToUpdate.AssessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	err = useCases.RecalculateMarginOfAssessmentPrice(ctr.Service, assessment)
	if err != nil {
		exp, errMessage := excptn.New(155, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, &assessmentAdditionToUpdate)
}

func (ctr *Controller) DeleteAssessmentAddition(c *gin.Context) {
	var assessmentAddition model.AssessmentAddition
	err := c.Bind(&assessmentAddition)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	//TODO low priority - check if addition status equal to current assessment status

	// remove assessmentAddition from Assessment
	err = ctr.Service.Manager.DeleteAssessmentAddition(&assessmentAddition)
	if err != nil {
		exp, errMessage := excptn.New(107, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	assessment, err := ctr.Service.Manager.GetAssessmentByID(assessmentAddition.AssessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	err = useCases.RecalculateMarginOfAssessmentPrice(ctr.Service, assessment)
	if err != nil {
		exp, errMessage := excptn.New(155, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, true)
}
