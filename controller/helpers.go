package controller

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
)

func (ctr *Controller) GetAllDealershipStatuses(c *gin.Context) {
	statuses, err := ctr.Service.Manager.GetAllDealershipStatuses()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, statuses)
}

func (ctr *Controller) GetAllUserStatuses(c *gin.Context) {
	statuses, err := ctr.Service.Manager.GetAllUserStatuses()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, statuses)
}

func (ctr *Controller) GetAllGenders(c *gin.Context) {
	statuses, err := ctr.Service.Manager.GetAllGenders()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, statuses)
}

func (ctr *Controller) GetAllReferrals(c *gin.Context) {
	statuses, err := ctr.Service.Manager.GetAllReferrals()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, statuses)
}

func (ctr *Controller) GetAllClientTypes(c *gin.Context) {
	statuses, err := ctr.Service.Manager.GetAllClientTypes()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, statuses)
}

func (ctr *Controller) GetAllRoles(c *gin.Context) {
	statuses, err := ctr.Service.Manager.GetAllRoles()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, statuses)
}

func (ctr *Controller) GetAllDocumentTypes(c *gin.Context) {
	statuses, err := ctr.Service.Manager.GetAllDocumentTypes()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, statuses)
}

func (ctr *Controller) GetAllPhotoTypes(c *gin.Context) {
	statuses, err := ctr.Service.Manager.GetAllPhotoTypes()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, statuses)
}

func (ctr *Controller) GetAllAssessmentStatuses(c *gin.Context) {
	statuses, err := ctr.Service.Manager.GetAllAssessmentStatuses()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, statuses)
}

func (ctr *Controller) GetAllAssessmentTypes(c *gin.Context) {
	types, err := ctr.Service.Manager.GetAllAssessmentTypes()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, types)
}

func (ctr *Controller) GetAllCommunications(c *gin.Context) {
	communications, err := ctr.Service.Manager.GetAllCommunications()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, communications)
}

func (ctr *Controller) GetAllDealGoals(c *gin.Context) {
	dealGoals, err := ctr.Service.Manager.GetAllDealGoals()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, dealGoals)
}

func (ctr *Controller) GetAllCarInterests(c *gin.Context) {
	carInterests, err := ctr.Service.Manager.GetAllCarInterests()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, carInterests)
}

func (ctr *Controller) GetAllRelevances(c *gin.Context) {
	relevances, err := ctr.Service.Manager.GetAllRelevances()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, relevances)
}

func (ctr *Controller) GetAllCarStatuses(c *gin.Context) {
	carStatuses, err := ctr.Service.Manager.GetAllCarStatuses()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, carStatuses)
}

func (ctr *Controller) GetAllAssessmentStages(c *gin.Context) {
	assessmentStages, err := ctr.Service.Manager.GetAllAssessmentStages()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, assessmentStages)
}

// GetCountries controller
func (ctr *Controller) GetCountries(c *gin.Context) {
	pagination := new(model.Pagination)

	err := c.ShouldBindQuery(pagination)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	countries, err := ctr.Service.Manager.GetCountries(pagination)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, countries)
}

// GetRegions controller
func (ctr *Controller) GetRegions(c *gin.Context) {
	countryID, err := strconv.ParseUint(c.Param("countryID"), 10, 64)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	pagination := new(model.Pagination)
	err = c.ShouldBindQuery(pagination)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	regions, err := ctr.Service.Manager.GetRegionsByCountryID(uint(countryID), pagination)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, regions)
}

// GetFactories controller
func (ctr *Controller) GetFactories(c *gin.Context) {
	countryID, err := strconv.ParseUint(c.Param("countryID"), 10, 64)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	pagination := new(model.Pagination)
	err = c.ShouldBindQuery(pagination)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	regions, err := ctr.Service.Manager.GetFactories(uint(countryID), pagination)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, regions)
}

// GetBanks controller
func (ctr *Controller) GetBanks(c *gin.Context) {
	countryID, err := strconv.ParseUint(c.Param("countryID"), 10, 64)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	pagination := new(model.Pagination)
	err = c.ShouldBindQuery(pagination)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	banks, err := ctr.Service.Manager.GetBanksByCountryID(uint(countryID), pagination)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, banks)
}

// GetPaymentMethods controller
func (ctr *Controller) GetPaymentMethods(c *gin.Context) {
	pagination := new(model.Pagination)

	err := c.ShouldBindQuery(pagination)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	paymentMethods, err := ctr.Service.Manager.GetPaymentMethods(pagination)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, paymentMethods)
}
