package controller

import (
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/useCases"
	"gitlab.com/tradeincar/backend/api/utils"
)

func (ctr *Controller) CreateCar(c *gin.Context) {
	var carRequest scheme.CarInCreate
	err := c.Bind(&carRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	createCar, err := useCases.CreateAssessmentCarEntity(ctr.Service, &carRequest)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	// save to DB
	err = ctr.Service.Manager.CreateCar(&createCar)
	if err != nil {
		return
	}

	c.JSON(200, &createCar)
}

func (ctr *Controller) GetCarOnAssessment(c *gin.Context) {
	q := c.Request.URL.Query()
	q.Add("assessmentStatusKeys", "ON_WORK,ON_MANAGER_APPROVAL,ASSESSMENT_APPROVED,DIAGNOSTICS,DIAGNOSTICS_ON_APPROVAL,DIAGNOSTICS_APPROVED,SERVICE_CENTER_REMOVAL")
	c.Request.URL.RawQuery = q.Encode()
	ctr.GetCars(c)
}

func (ctr *Controller) GetCarsOnWarehouse(c *gin.Context) {
	q := c.Request.URL.Query()
	q.Add("assessmentStatusKeys", "SALE_PREPARATION,ON_ROP_APPROVAL,RESERVE,ON_SALE")
	c.Request.URL.RawQuery = q.Encode()
	ctr.GetCars(c)
}

func (ctr *Controller) GetCarsOnSale(c *gin.Context) {
	q := c.Request.URL.Query()
	q.Add("assessmentStatusKeys", "RESERVE,ON_SALE")
	c.Request.URL.RawQuery = q.Encode()
	ctr.GetCars(c)
}

func (ctr *Controller) GetCars(c *gin.Context) {
	user := c.MustGet("user").(model.User)

	specialFilterForRoles := []string{"ATZ", "TECHNICIAN"}

	if _, isFindRole := utils.FindInSlice(specialFilterForRoles, user.Role.Name); isFindRole {
		q := c.Request.URL.Query()
		q.Add("assessmentStage", "IN_STOCK")
		c.Request.URL.RawQuery = q.Encode()
	}

	queryParams := utils.GetQueryParams(c)
	limit, err := strconv.ParseUint(c.DefaultQuery("limit", "100"), 10, 32)
	page, err := strconv.ParseUint(c.DefaultQuery("page", "1"), 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	offset := (page - 1) * limit
	cars, err := ctr.Service.Manager.GetCars(queryParams, user, limit, offset)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, cars)
}

func (ctr *Controller) GetCarByVIN(c *gin.Context) {
	vin := c.Param("vin")
	car, err := ctr.Service.Manager.GetCarByVIN(vin)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, car)
}

func (ctr *Controller) GetAllCarEngineType(c *gin.Context) {
	c.Set("characteristicID", uint(12))
	ctr.GetCharacteristicValues(c)
}

func (ctr *Controller) GetAllCarDriveWheelType(c *gin.Context) {
	c.Set("characteristicID", uint(27))
	ctr.GetCharacteristicValues(c)
}

func (ctr *Controller) GetAllCarKppType(c *gin.Context) {
	c.Set("characteristicID", uint(24))
	ctr.GetCharacteristicValues(c)
}

func (ctr *Controller) GetAllCarBodyStyleType(c *gin.Context) {
	c.Set("characteristicID", uint(2))
	ctr.GetCharacteristicValues(c)
}

func (ctr *Controller) GetAllSteeringType(c *gin.Context) {
	c.Set("characteristicID", uint(53))
	ctr.GetCharacteristicValues(c)
}

func (ctr *Controller) GetCharacteristicValues(c *gin.Context) {
	characteristicID := c.MustGet("characteristicID").(uint)
	characteristicValues, err := ctr.Service.Manager.GetValuesByCharacteristicID(characteristicID)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, characteristicValues)
}

func (ctr *Controller) GetCarYearsByModelID(c *gin.Context) {
	carModelIDString := c.Param("id")
	u64, err := strconv.ParseUint(carModelIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	carModelID := uint(u64)

	yearBeginGeneration, err := ctr.Service.Manager.GetYearBeginGenerationByModelID(carModelID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	yearEndGeneration, err := ctr.Service.Manager.GetYearEndGenerationByModelID(carModelID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	var years []int

	yearBegin := yearBeginGeneration.YearBegin.Year()

	yearEnd := yearEndGeneration.YearEnd.Year()

	for year := yearBegin; year <= yearEnd; year++ {
		years = append(years, year)
	}

	c.JSON(200, years)
}

func (ctr *Controller) GetGenerationsByModelIDAndYear(c *gin.Context) {
	carModelIDString := c.Param("id")
	u64, err := strconv.ParseUint(carModelIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	carModelID := uint(u64)

	// currentYear := fmt.Sprintf("%d", time.Now().Year())
	year := c.DefaultQuery("year", "")

	generations, err := ctr.Service.Manager.GetGenerationsByModelIDAndYear(carModelID, year)

	defaultCarModification := model.CarModification{
		ID:   0,
		Name: "(Другая модификация)",
	}

	defaultCarSerie := model.CarSerie{}
	defaultCarSerie.CarModifications = append(defaultCarSerie.CarModifications, defaultCarModification)

	for i := range generations {
		generations[i].CarSeries = append(generations[i].CarSeries, defaultCarSerie)
	}

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, generations)
}

func (ctr *Controller) GetModificationByID(c *gin.Context) {
	modificationIDString := c.Param("id")
	u64, err := strconv.ParseUint(modificationIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	generationID := uint(u64)

	carModification, err := ctr.Service.Manager.GetModificationByID(generationID)
	carModificationInResponse := scheme.CarModificationInResponse{}
	err = utils.StructTransformer(carModification, &carModificationInResponse)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	characteristicValMap := make(map[uint]string)

	for _, val := range carModification.CarCharacteristicValues {
		index := val.CarCharacteristicID
		characteristicValMap[index] = val.Value
	}

	carModificationInResponse.EngineType = ctr.Service.Dict.GetEngineTypeByValue(characteristicValMap[12])
	carModificationInResponse.EngineVolume = characteristicValMap[13]
	carModificationInResponse.EnginePower = characteristicValMap[14]
	carModificationInResponse.KppType = ctr.Service.Dict.GetKppTypeByValue(characteristicValMap[24])
	carModificationInResponse.CarBodyStyleType = ctr.Service.Dict.GetCarBodyStyleTypeByValue(characteristicValMap[2])
	carModificationInResponse.DriveWheelType = ctr.Service.Dict.GetDriveWheelTypeByValue(characteristicValMap[27])
	carModificationInResponse.DoorCount = characteristicValMap[3]
	carModificationInResponse.SteeringType = ctr.Service.Dict.GetSteeringTypeByValue(characteristicValMap[53])

	if carModificationInResponse.Name == "" {
		carModificationInResponse.Name = "(Другая модификация)"
	}

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, carModificationInResponse)
}
