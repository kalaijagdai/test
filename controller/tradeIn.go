package controller

import (
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/useCases"
	"gitlab.com/tradeincar/backend/api/utils"
)

func (ctr *Controller) UpdateTradeIn(c *gin.Context) {
	user := c.MustGet("user").(model.User)
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)

	var tradeinRequest scheme.TradeInInUpdate
	err = c.BindJSON(&tradeinRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	tradeinRequest.AssessmentID = assessmentID
	var clientID uint

	// handle client
	var client model.Client
	if tradeinRequest.Client != nil {
		//if tradeinRequest.Client.ID >= 0 {
		//	clientID = tradeinRequest.Client.ID
		//} else {
		//	client, err = useCases.CreateClientEntity(ctr.Service, tradeinRequest.Client)
		//	if err != nil {
		//		fmt.Println("errr: ", err)
		//	}
		//	clientID = client.ID
		//}
		if tradeinRequest.Client.ID < 1 {
			client, err = useCases.CreateClientEntity(ctr.Service, tradeinRequest.Client, &user)
			if err != nil {
				fmt.Println("errr: ", err)
			}
			clientID = client.ID
		} else {
			clientID = tradeinRequest.Client.ID
			utils.StructTransformer(tradeinRequest.Client, &client)
		}
	}

	tradeinRequest.Client = nil

	tradein := model.TradeIn{}
	err = utils.StructTransformer(tradeinRequest, &tradein)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	tradein.ClientID = clientID
	tradein.Client = nil

	err = ctr.Service.Manager.UpdateTradeIn(&tradein)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, tradeinRequest)
}
