package controller

import (
	"fmt"
	"strconv"
	"strings"
	"text/template"

	"gitlab.com/tradeincar/backend/api/scheme"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/utils"
)

func (ctr *Controller) CreateDealership(c *gin.Context) {
	me := c.MustGet("user").(model.User)

	var dealershipRequest model.Dealership
	err := c.Bind(&dealershipRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	dealershipStatus, _ := ctr.Service.Dict.GetDealershipStatusByName("ACTIVE")
	//dealershipStatus, _ := ctr.Service.Manager.GetDealershipStatusByName("ACTIVE")
	dealershipRequest.DealershipStatusID = dealershipStatus.ID
	//dealership := model.Dealership{
	//	Name:               dealershipRequest.Name,
	//	StatusID:           dealershipStatus.ID,
	//	Entity:             dealershipRequest.Entity,
	//	Code:               dealershipRequest.Code,
	//	Profile:            dealershipRequest.Profile,
	//	Telephone:          dealershipRequest.Telephone,
	//	Fax:                dealershipRequest.Fax,
	//	Email:              dealershipRequest.Email,
	//	Website:            dealershipRequest.Website,
	//	ManagerAffirmation: dealershipRequest.ManagerAffirmation,
	//	SaleTransfer:       dealershipRequest.SaleTransfer,
	//	AcceptTime:        	dealershipRequest.AcceptTime,
	//	DaysToPriceUpdate:  dealershipRequest.DaysToPriceUpdate,
	//}
	err = ctr.Service.Manager.CreateDealership(&dealershipRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	// status change notification
	emailTmpl := `Здравствуйте, {{.User.FirstName}}! 

Создан новый  ДЦ
ДЦ
ID: {{.Dealership.ID}}
Name: {{.Dealership.Name}}
Руководитель: {{.Manager.FirstName}} {{.Manager.LastName}}

С уважением,
Команда MycarPro
`
	temp := template.Must(template.New("email").Parse(emailTmpl))

	usersToNotify, err := ctr.Service.Manager.GetAllUsers(map[string]interface{}{"role": "OWNER"}, model.User{}, 10000, 0)
	if err != nil {
		fmt.Println("Error in notify: ", err)
	}
	//send emails to dealership users
	for _, dealershipUser := range usersToNotify {
		notificationModel := scheme.CreateDealershipNotification{
			User:       &dealershipUser,
			Manager:    &me,
			Dealership: &dealershipRequest,
		}
		builder := &strings.Builder{}
		if err := temp.Execute(builder, notificationModel); err != nil {
			fmt.Println(err)
		}

		to := dealershipUser.Email
		msg := builder.String()

		if to != nil {
			go utils.SendMail(*to, msg)
		}

	}

	c.JSON(200, dealershipRequest)
}

func (ctr *Controller) GetAllDealerships(c *gin.Context) {
	user := c.MustGet("user").(model.User)
	queryParams := utils.GetQueryParams(c)
	limit, err := strconv.ParseUint(c.DefaultQuery("limit", "100"), 10, 32)
	page, err := strconv.ParseUint(c.DefaultQuery("page", "1"), 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	offset := (page - 1) * limit
	dealerships, err := ctr.Service.Manager.GetAllDealerships(queryParams, user, limit, offset)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, dealerships)
}

func (ctr *Controller) GetDealershipUsers(c *gin.Context) {
	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	dealershipID := uint(u64)

	users, err := ctr.Service.Manager.GetDealershipUsers(dealershipID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, users)
}

func (ctr *Controller) UpdateDealership(c *gin.Context) {
	id := c.Param("id")
	var dealershipToUpdate model.Dealership
	err := c.BindJSON(&dealershipToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	dealershipToUpdate.ID = uint(u64)

	//dealershipToUpdate.Marks = []model.CarMark{}
	dealershipToUpdate.Users = []model.User{}

	err = ctr.Service.Manager.UpdateDealership(&dealershipToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, &dealershipToUpdate)
}

func (ctr *Controller) RemoveMarkFromDealership(c *gin.Context) {
	id := c.Param("dealershipId")
	var dealershipToUpdate model.Dealership
	var mark model.CarMark
	err := c.BindJSON(&mark)
	fmt.Printf("CarMark: %v", mark)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	dealershipToUpdate.ID = uint(u64)

	err = ctr.Service.Manager.RemoveMarkFromDealership(&mark, &dealershipToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, nil)
}

func (ctr *Controller) GetDealership(c *gin.Context) {
	dealershipIDString := c.Param("id")
	u64, err := strconv.ParseUint(dealershipIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	dealershipID := uint(u64)

	dealership, err := ctr.Service.Manager.GetDealershipByID(dealershipID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, dealership)
}

func (ctr *Controller) DeleteDealership(c *gin.Context) {
	dealershipIDString := c.Param("id")
	u64, err := strconv.ParseUint(dealershipIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	dealershipID := uint(u64)

	dealership, err := ctr.Service.Manager.GetDealershipByID(dealershipID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	if len(dealership.Users) > 0 {
		c.JSON(409, gin.H{
			"message": fmt.Errorf("Невозможно удалить дилерский центр, пока в нем есть пользователи.").Error(),
		})
		return
	}

	err = ctr.Service.Manager.DeleteDealership(dealershipID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, true)
}
