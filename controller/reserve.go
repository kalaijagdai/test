package controller

import (
	"fmt"
	"strconv"

	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/useCases"
	"gitlab.com/tradeincar/backend/api/utils"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
)

func (ctr *Controller) UpdateReserve(c *gin.Context) {
	user := c.MustGet("user").(model.User)
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)

	var reserveRequest scheme.ReserveInUpdate
	err = c.BindJSON(&reserveRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	reserveRequest.AssessmentID = assessmentID
	var clientID uint

	// handle client
	var client model.Client
	if reserveRequest.Client != nil {
		if reserveRequest.Client.ID < 1 {
			client, err = useCases.CreateClientEntity(ctr.Service, reserveRequest.Client, &user)
			if err != nil {
				fmt.Println("errr: ", err)
			}
			clientID = client.ID
		} else {
			clientID = reserveRequest.Client.ID
			client, _ = ctr.Service.Manager.GetClientByID(clientID)
			if client.ID < 1 {
				utils.StructTransformer(reserveRequest.Client, &client)
			}
		}
	}

	reserveRequest.Client = nil

	reserve := model.Reserve{}
	err = utils.StructTransformer(reserveRequest, &reserve)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	reserve.ClientID = clientID
	reserve.Client = nil

	reserve.UserID = user.ID

	err = ctr.Service.Manager.UpdateReserve(&reserve)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	reserve.Client = &client

	c.JSON(200, reserve)
}
