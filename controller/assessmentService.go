package controller

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/useCases"
	excptn "gitlab.com/tradeincar/backend/api/utils"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
)

func (ctr *Controller) AddAssessmentService(c *gin.Context) {
	// get id from param
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)

	assessment, err := ctr.Service.Manager.GetAssessmentByID(assessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// check if status is right
	assessmentStatusString := strings.ToUpper(c.Param("status"))
	status, err := ctr.Service.Dict.GetAssessmentStatusByName(assessmentStatusString)
	if err != nil {
		exp, errMessage := excptn.New(109, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	if status.Name != "ON_WORK" && status.Name != "DIAGNOSTICS" && status.Name != "SALE_PREPARATION" {
		err = errors.New("Not satisfied with status")
		exp, errMessage := excptn.New(154, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	if assessment.AssessmentStatusID != status.ID || assessment.AssessmentStatus.AssessmentStageID != status.AssessmentStageID {
		err = errors.New("Not satisfied with status")
		exp, errMessage := excptn.New(154, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// bind request with scheme.AssessmentServiceInCreate
	var assessmentServiceRequest scheme.AssessmentServiceInCreate
	err = c.Bind(&assessmentServiceRequest)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// create assessmentService model from request
	assessmentService := model.AssessmentService{
		Name:      assessmentServiceRequest.Name,
		Price:     assessmentServiceRequest.Price,
		ByCompany: assessmentServiceRequest.ByCompany,
		Visible:   assessmentServiceRequest.Visible,
	}

	// set assessment status for assessmentService
	assessmentStatus, _ := ctr.Service.Dict.GetAssessmentStatusByName(assessmentStatusString)
	assessmentService.AssessmentStatusID = assessmentStatus.ID

	err = ctr.Service.Manager.AddAssessmentService(assessmentID, &assessmentService)
	if err != nil {
		exp, errMessage := excptn.New(106, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	assessmentService.AssessmentStatusID = status.ID
	assessmentService.AssessmentStatus = &status

	assessment.AssessmentServices = append(assessment.AssessmentServices, assessmentService)

	err = useCases.RecalculateMarginOfAssessmentPrice(ctr.Service, assessment)
	if err != nil {
		exp, errMessage := excptn.New(155, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, &assessmentService)
}

func (ctr *Controller) UpdateAssessmentService(c *gin.Context) {
	var assessmentServiceToUpdate model.AssessmentService
	err := c.BindJSON(&assessmentServiceToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	//TODO low priority - check if service status equal to current assessment status

	err = ctr.Service.Manager.UpdateAssessmentService(&assessmentServiceToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	assessment, err := ctr.Service.Manager.GetAssessmentByID(assessmentServiceToUpdate.AssessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	err = useCases.RecalculateMarginOfAssessmentPrice(ctr.Service, assessment)
	if err != nil {
		exp, errMessage := excptn.New(155, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, &assessmentServiceToUpdate)
}

func (ctr *Controller) DeleteAssessmentService(c *gin.Context) {
	var assessmentService model.AssessmentService
	err := c.Bind(&assessmentService)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	//TODO low priority - check if service status equal to current assessment status

	// remove assessmentService from Assessment
	err = ctr.Service.Manager.DeleteAssessmentService(&assessmentService)
	if err != nil {
		exp, errMessage := excptn.New(107, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	assessment, err := ctr.Service.Manager.GetAssessmentByID(assessmentService.AssessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	err = useCases.RecalculateMarginOfAssessmentPrice(ctr.Service, assessment)
	if err != nil {
		exp, errMessage := excptn.New(155, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, true)
}
