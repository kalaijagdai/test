package controller

import (
	"gitlab.com/tradeincar/backend/api/model"
	"github.com/gin-gonic/gin"
	"strconv"
)

func(ctr *Controller) CreateMark(c *gin.Context) {
	var mark model.CarMark
	err := c.BindJSON(&mark)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	err = ctr.Service.Manager.CreateMark(&mark)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, mark)
}

func(ctr *Controller) GetAllMarks(c *gin.Context) {
	marks, err := ctr.Service.Manager.GetAllMarks()

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, marks)
}

func(ctr *Controller) UpdateMark(c *gin.Context) {
	id := c.Param("id")
	var markToUpdate model.CarMark
	err := c.BindJSON(&markToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	markToUpdate.ID = uint(u64)

	err = ctr.Service.Manager.UpdateMark(&markToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, markToUpdate)
}