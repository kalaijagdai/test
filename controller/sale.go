package controller

import (
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/useCases"
	"gitlab.com/tradeincar/backend/api/utils"
)

func (ctr *Controller) UpdateSale(c *gin.Context) {
	me := c.MustGet("user").(model.User)
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)
	fmt.Println("id is: ", assessmentID)

	var saleRequest scheme.SaleInUpdate
	err = c.BindJSON(&saleRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	saleRequest.AssessmentID = assessmentID
	var clientID uint

	// handle client
	var client model.Client
	if saleRequest.Client != nil {
		if saleRequest.Client.ID < 1 {
			client, err = useCases.CreateClientEntity(ctr.Service, saleRequest.Client, &me)
			if err != nil {
				fmt.Println("errr: ", err)
			}
			clientID = client.ID
		} else {
			clientID = saleRequest.Client.ID
			utils.StructTransformer(saleRequest.Client, &client)
		}
	}

	saleRequest.Client = nil

	sale := model.Sale{}
	err = utils.StructTransformer(saleRequest, &sale)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	sale.ClientID = clientID
	sale.Client = nil

	err = ctr.Service.Manager.UpdateSale(&sale)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	user, _ := ctr.Service.Manager.GetUserByID(sale.UserID)

	sale.User = &user

	sale.Client = &client

	c.JSON(200, sale)
}
