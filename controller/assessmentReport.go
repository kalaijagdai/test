package controller

import (
	"errors"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/utils"
	excptn "gitlab.com/tradeincar/backend/api/utils"
)

func (ctr *Controller) CreateAssessmentReport(c *gin.Context) {
	var report scheme.AssessmentReportInCreate
	err := c.Bind(&report)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// get assessment to check if the status is appropriate
	assessment, err := ctr.Service.Manager.GetAssessmentWithStatusByID(report.AssessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// get assessment status entity from request
	assessmentStatusFromRequest, err := ctr.Service.Manager.GetAssessmentStatusByID(report.AssessmentStatusID)
	if err != nil {
		exp, errMessage := excptn.New(109, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// check if the sent status is appropriate
	if assessmentStatusFromRequest.Name != "ON_WORK" && assessmentStatusFromRequest.Name != "DIAGNOSTICS" && assessmentStatusFromRequest.AssessmentStage.Name  != "IN_STOCK" {
		err = errors.New("Not satisfied with status")
		exp, errMessage := excptn.New(154, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// check if the sent status from request is the same with the current status from assessment
	if assessment.AssessmentStatusID != assessmentStatusFromRequest.ID {
		err = errors.New("Not satisfied with status")
		exp, errMessage := excptn.New(154, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	assessmentReport := model.AssessmentReport{
		Name:                 report.Name,
		AssessmentID:         report.AssessmentID,
		AssessmentStatusID:   report.AssessmentStatusID,
		UserID:               report.UserID,
		Notes:                report.Notes,
		File:                 report.File,
		AssessmentInspection: report.AssessmentInspection,
	}

	err = ctr.Service.Manager.CreateAssessmentReport(&assessmentReport)
	if err != nil {
		exp, errMessage := excptn.New(609, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	assessmentReport.AssessmentStatus, err = ctr.Service.Dict.GetAssessmentStatusByID(assessmentReport.AssessmentStatusID)
	if err != nil {
		exp, errMessage := excptn.New(109, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	assessmentReport.User, err = ctr.Service.Manager.GetUserByID(assessmentReport.UserID)
	if err != nil {
		exp, errMessage := excptn.New(166, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, assessmentReport)
}

//get assessment by id
func (ctr *Controller) GetAssessmentReportByID(c *gin.Context) {
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		exp, errMessage := excptn.New(3, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	assessmentID := uint(u64)

	assessment, err := ctr.Service.Manager.GetAssessmentReportByID(assessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, assessment)
}

func (ctr *Controller) UpdateAssessmentReport(c *gin.Context) {
	id := c.Param("id")
	var assessmentReportInUpdate scheme.AssessmentReportInUpdate
	err := c.BindJSON(&assessmentReportInUpdate)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentReportInUpdate.ID = uint(u64)

	// get assessment to check if the status is appropriate
	assessment, err := ctr.Service.Manager.GetAssessmentWithStatusByID(assessmentReportInUpdate.AssessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// get assessment status entity from request
	assessmentStatusFromRequest, err := ctr.Service.Manager.GetAssessmentStatusByID(assessmentReportInUpdate.AssessmentStatusID)
	if err != nil {
		exp, errMessage := excptn.New(109, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}


	// check if the sent status is appropriate
	if assessmentStatusFromRequest.Name != "ON_WORK" && assessmentStatusFromRequest.Name != "DIAGNOSTICS" && assessmentStatusFromRequest.AssessmentStage.Name  != "IN_STOCK" {
		err = errors.New("Not satisfied with status")
		exp, errMessage := excptn.New(154, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// check if the sent status from request is the same with the current status from assessment
	if assessment.AssessmentStatusID != assessmentStatusFromRequest.ID {
		err = errors.New("Not satisfied with status")
		exp, errMessage := excptn.New(154, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}


	assessmentReport := model.AssessmentReport{}
	err = utils.StructTransformer(assessmentReportInUpdate, &assessmentReport)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	err = ctr.Service.Manager.UpdateAssessmentReport(&assessmentReport)
	if err != nil {
		exp, errMessage := excptn.New(165, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, &assessmentReport)
}
