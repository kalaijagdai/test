package controller

import (
	"github.com/gin-gonic/gin"
	"strconv"
)


func(ctr *Controller) GetModelsByMarkID(c *gin.Context) {
	carMarkIDString := c.Param("id")
	u64, err := strconv.ParseUint(carMarkIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	carMarkID := uint(u64)


	models, err := ctr.Service.Manager.GetModelsByMarkID(carMarkID)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, models)
}
