package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/tradeincar/backend/api/model"
)

const (
	LIMITOFRECODS = 5
)

// Метод поиск авто в сайтах объявлений
func (ctr *Controller) AdsCars(cfg *model.Configuration) gin.HandlerFunc {
	return func(c *gin.Context) {
		var filterCar model.FilterCar

		if err := c.ShouldBind(&filterCar); err != nil {
			for _, fieldErr := range err.(validator.ValidationErrors) {
				c.JSON(http.StatusBadRequest, gin.H{
					"message": fmt.Sprintf("ShouldBind. %s", fmt.Sprint(fieldErr)),
				})
				return
			}
		}
		uri, err := url.Parse(cfg.ReportURI)
		if err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
			return
		}
		reportURI := uri.String() + "/core/ad_posts/"
		req, err := http.NewRequest(http.MethodGet, reportURI, nil)
		if err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
			return
		}
		client := http.Client{
			Timeout: 30 * time.Second,
		}
		resp, err := client.Do(req)
		if err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
			return
		}
		defer resp.Body.Close()
		if resp.StatusCode != http.StatusOK {
			c.JSON(500, gin.H{
				"message": fmt.Errorf("StatusCode incorrect - %v", resp.StatusCode).Error(),
			})
			return
		}
		var resultCars []model.FilterCarResultKolesa
		if err := json.NewDecoder(resp.Body).Decode(&resultCars); err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
			return
		}
		matchCars := ctr.compareCars(filterCar, resultCars)

		c.JSON(http.StatusOK, matchCars)
	}
}

func (ctr *Controller) compareCars(filterCar model.FilterCar, resultCars []model.FilterCarResultKolesa) *[]model.FilterCarResultKolesa {
	var cars []model.FilterCarResultKolesa
	for _, car := range resultCars {
		if car.Brand == filterCar.Mark {
			if car.Model == filterCar.Model {
				cars = append(cars, car)
			}
		}
	}
	return &cars
}

func (ctr *Controller) CreateFilterCarByUser(c *gin.Context) {
	user := c.MustGet("user").(model.User)
	var filterCar model.FilterCar
	if err := c.Bind(&filterCar); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": fmt.Sprintf("Bind. %s", err.Error()),
		})
		return
	}
	filterCarByUser, err := ctr.validateOfFilterByCar(user, filterCar)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": fmt.Sprintf("validateOfFilterByCar. %s", err.Error()),
		})
		return
	}

	cntFilter, err := ctr.Service.Manager.CountFilterCarByUser(user.ID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": fmt.Sprintf("CountFilterCarByUser. %s", err.Error()),
		})
		return
	}
	if cntFilter >= LIMITOFRECODS {
		c.JSON(http.StatusRequestedRangeNotSatisfiable, gin.H{
			"message": "Количество записей достигло до 5",
		})
		return
	}
	if err := ctr.Service.Manager.CreateFilterCarByUser(filterCarByUser); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": fmt.Sprintf("CreateFilterCarByUser. %s", err.Error()),
		})
		return
	}
	c.Status(http.StatusCreated)
}
func (ctr *Controller) GetAllFilterByCar(c *gin.Context) {
	filterCarByUser, err := ctr.Service.Manager.GetAllFilterCarByUser()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": fmt.Sprintf("GetAllFilterCarByUser. %s", err.Error()),
		})
		return
	}
	c.JSON(http.StatusOK, filterCarByUser)
}

func (ctr *Controller) UpdateFilterByCar(c *gin.Context) {
	user := c.MustGet("user").(model.User)
	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	var filterCar model.FilterCar
	if err := c.Bind(&filterCar); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": fmt.Sprintf("Bind. %s", err.Error()),
		})
		return
	}
	filterCar.ID = uint(u64)
	filterCarByUser, err := ctr.validateOfFilterByCar(user, filterCar)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": fmt.Sprintf("validateOfFilterByCar. %s", err.Error()),
		})
		return
	}
	if err := ctr.Service.Manager.UpdateFilterCarByUser(filterCarByUser); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": fmt.Sprintf("UpdateFilterCarByUser. %s", err.Error()),
		})
		return
	}
	c.Status(http.StatusOK)
}

func (ctr *Controller) DeleteFilterByCar(c *gin.Context) {
	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	filterCar := model.FilterCarByUser{
		ID: uint(u64),
	}

	if err := ctr.Service.Manager.DeleteFilterCarByUser(&filterCar); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": fmt.Sprintf("DeleteFilterCarByUser. %s", err.Error()),
		})
		return
	}
	c.Status(http.StatusOK)
}

func (ctr *Controller) validateOfFilterByCar(user model.User, filterCar model.FilterCar) (*model.FilterCarByUser, error) {

	fromYearOut, toYearOut, err := ctr.field("time", "YearOut", filterCar.YearOut)
	if err != nil {
		return nil, err
	}

	fromMileage, toMileage, err := ctr.field("int64", "Mileage", filterCar.Mileage)
	if err != nil {
		return nil, err
	}

	fromCapacity, toCapacity, err := ctr.field("float64", "Capacity", filterCar.Capacity)
	if err != nil {
		return nil, err
	}

	fromPrice, toPrice, err := ctr.field("float64", "Price", filterCar.Price)
	if err != nil {
		return nil, err
	}

	fromCheaperMarket, toCheaperMarket, err := ctr.field("float64", "CheaperMarket", filterCar.CheaperMarket)
	if err != nil {
		return nil, err
	}

	fromDaysOnSale, toDaysOnSale, err := ctr.field("int", "DaysOnSale", filterCar.DaysOnSale)
	if err != nil {
		return nil, err
	}
	amountOfAds, err := strconv.Atoi(filterCar.AmountOfAds)
	if err != nil {
		return nil, err
	}

	filtercar := model.FilterCarByUser{
		UserID:            user.ID,
		Name:              "test",
		ID:                filterCar.ID,
		CarModel:          filterCar.Model,
		Mark:              filterCar.Mark,
		FromYearOut:       fromYearOut.(time.Time),
		ToYearOut:         toYearOut.(time.Time),
		TypeOfEngine:      filterCar.TypeOfEngine,
		KPP:               filterCar.KPP,
		DriveUnit:         filterCar.DriveUnit,
		TypeOfCarcase:     filterCar.TypeOfCarcase,
		Color:             filterCar.Color,
		SteeringWheel:     filterCar.SteeringWheel,
		FromMileage:       fromMileage.(int64),
		ToMileage:         toMileage.(int64),
		FromCapacity:      fromCapacity.(float64),
		ToCapacity:        toCapacity.(float64),
		FromPrice:         fromPrice.(float64),
		ToPrice:           toPrice.(float64),
		FromCheaperMarket: fromCheaperMarket.(float64),
		ToCheaperMarket:   toCheaperMarket.(float64),
		FromDaysOnSale:    fromDaysOnSale.(int),
		ToDaysOnSale:      toDaysOnSale.(int),
		Region:            filterCar.Region,
		Sites:             filterCar.Sites,
		AmountOfAds:       amountOfAds,
		WithPhoto:         filterCar.WithPhoto,
		CustomCleared:     filterCar.CustomCleared,
		Emergency:         filterCar.Emergency,
	}
	return &filtercar, nil
}

func (ctr *Controller) field(layout string, fieldName string, values []string) (interface{}, interface{}, error) {
	if len(values) != 2 {
		return nil, nil, fmt.Errorf("len %s неравен 2", fieldName)
	}
	fromPrice, err := ctr.parseField(layout, values[0])
	if err != nil {
		return nil, nil, err
	}
	toPrice, err := ctr.parseField(layout, values[1])
	if err != nil {
		return nil, nil, err
	}
	return fromPrice, toPrice, nil
}

func (ctr *Controller) parseField(layout string, value string) (field interface{}, err error) {
	switch layout {
	case "time":
		field, err = time.Parse(time.RFC3339, value)
		if err != nil {
			return nil, err
		}

		return field, nil
	case "int64":
		field, err = strconv.ParseInt(value, 10, 64)
		if err != nil {
			return nil, err
		}
		return field, nil
	case "float64":
		field, err = strconv.ParseFloat(value, 64)
		if err != nil {
			return nil, err
		}
		return field, nil
	case "int":
		field, err = strconv.Atoi(value)
		if err != nil {
			return nil, err
		}
		return field, nil
	default:
		return "", nil
	}
}
