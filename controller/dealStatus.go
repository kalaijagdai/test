package controller

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"text/template"

	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/utils"
	excptn "gitlab.com/tradeincar/backend/api/utils"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
)

func (ctr *Controller) CreateDealStatus(c *gin.Context) {
	dealStatusString := strings.ToUpper(c.Param("status"))
	dealIDString := c.Param("id")
	var dealStatusRequest model.DealStatusRequest
	err := c.Bind(&dealStatusRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	u64, err := strconv.ParseUint(dealIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	dealID := uint(u64)

	// get enum
	dealStatus, err := ctr.Service.Dict.GetDealStatusByName(dealStatusString)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	// get deal by id
	deal, err := ctr.Service.Manager.GetDealByID(dealID)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	// check if user belongs to dealership (only dealership users are allowed to change dealStatus)
	var doesUserBelongToDealership bool
	me := c.MustGet("user").(model.User)
	for _, userDealership := range me.Dealerships {
		if userDealership.ID == deal.Dealership.ID {
			doesUserBelongToDealership = true
			break
		}
	}

	// if not belongs than return an error
	if doesUserBelongToDealership != true {
		err = errors.New("User does not belongs to the dealership")
		exp, errMessage := excptn.New(159, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// if user is not the manager, the deal closing will be sent on approval
	if dealStatus.Name == "CLOSED" && me.Role.Name != "MANAGER" {
		dealStatus, err = ctr.Service.Dict.GetDealStatusByName("ON_CLOSE_APPROVAL")
		if err != nil {
			exp, errMessage := excptn.New(158, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}

	}

	// update DealStatus
	err = ctr.Service.Manager.UpdateDeal(dealID, map[string]interface{}{"deal_status_id": dealStatus.ID})
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	// notification format
	if dealStatus.Name == "ON_CLOSE_APPROVAL" {
		// status change notification
		emailTmpl := `Здравствуйте, {{.User.FirstName}}!

Пожалуйста, подтвердите закрытие ведения

Ведение
    ID: {{.Deal.ID}}
    Клиент: {{.Deal.Client.Name}} {{.Deal.Client.Surname}}
    Специалист: {{.Deal.User.FirstName}} {{.Deal.User.LastName}}
    Цель: {{.Deal.DealGoal.Description}}
    Причина закрытия: {{.Reason}}

С уважением,
Команда MycarPro`

		temp := template.Must(template.New("email").Parse(emailTmpl))

		// if there are no one to notify
		if deal.Dealership == nil {
			c.JSON(200, dealStatus)
			return
		}

		// notify only dealership managers
		managerRole, _ := ctr.Service.Dict.GetRoleByName("MANAGER")
		for _, dealershipUser := range deal.Dealership.Users {
			if dealershipUser.RoleID != managerRole.ID {
				continue
			}

			notificationModel := scheme.DealStatusChangeNotification{
				User:   &dealershipUser,
				Deal:   &deal,
				Reason: dealStatusRequest.Reason,
			}
			builder := &strings.Builder{}
			if err := temp.Execute(builder, notificationModel); err != nil {
				fmt.Println(err)
			}

			to := dealershipUser.Email
			msg := builder.String()

			if to != nil {
				fmt.Printf("\n")
				fmt.Println(msg)
				go utils.SendMail(*to, msg)
			}

		}
	}
	c.JSON(200, dealStatus)
}

// func (ctr *Controller) GetDealStatuses(c *gin.Context) {
// 	DealStatuses,  := ctr.Service.Manager.GetDealStatuses()

// 	if err != nil {
// 		c.JSON(400, gin.H{
// 			"message": err.Error(),
// 		})
// 		return
// 	}

// 	c.JSON(200, DealStatuses)
// }
