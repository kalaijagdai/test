package controller

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/useCases"
	"gitlab.com/tradeincar/backend/api/utils"

	"fmt"
	"strconv"
)

func (ctr *Controller) CreateClient(c *gin.Context) {
	user := c.MustGet("user").(model.User)
	var clientRequest scheme.ClientInCreate
	err := c.Bind(&clientRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	if clientRequest.User != nil && clientRequest.User.ID > 0 {
		user = *clientRequest.User
	}

	createClient, err := useCases.CreateClientEntity(ctr.Service, &clientRequest, &user)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, &createClient)
}

// сканирование удостоверения
func (ctr *Controller) ClientPassportScan(c *gin.Context) {
	var response scheme.ClientPassportScanData
	//принитие данных по файлу
	file, header, err := c.Request.FormFile("scan")
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	filename := header.Filename
	// с какой стороны документа принимаем, 1 - лицевая, 0 - обратная
	// isFront := c.Request.FormValue("isFront")
	// подготовка к обращению апи
	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)
	reqFile, _ := writer.CreateFormFile("scan", filename)
	_, err = io.Copy(reqFile, file)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	// _ = writer.WriteField("isFront", isFront)
	err = writer.Close()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	client := &http.Client{}
	req, err := http.NewRequest("POST", "http://localhost:8089/", payload)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())               //мультипарт/форм-дата
	req.Header.Add("Authorization", "Bearer a2d8373f620c2d8c2455f1abae88d1cd") //токен
	//обращение к апи
	res, err := client.Do(req)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	defer res.Body.Close()
	// буферизация данных которые мы получили
	buf, err := ioutil.ReadAll(res.Body)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	//десериялизация данных
	err = json.Unmarshal(buf, &response)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, &response) //возвращение ответа
}

func (ctr *Controller) GetClients(c *gin.Context) {
	user := c.MustGet("user").(model.User)
	queryParams := utils.GetQueryParams(c)
	limit, err := strconv.ParseUint(c.DefaultQuery("limit", "100"), 10, 32)
	page, err := strconv.ParseUint(c.DefaultQuery("page", "1"), 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	offset := (page - 1) * limit
	clients, err := ctr.Service.Manager.GetClients(queryParams, user, limit, offset)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, clients)
}

func (ctr *Controller) GetFormattedClients(c *gin.Context) {
	clients, err := ctr.Service.Manager.GetFormattedClients()

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, clients)
}

func (ctr *Controller) GetClient(c *gin.Context) {
	clientIDString := c.Param("id")
	u64, err := strconv.ParseUint(clientIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	clientID := uint(u64)

	client, err := ctr.Service.Manager.GetClientByID(clientID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	var clientInResponse scheme.ClientInResponse
	deals := client.Deals
	client.Deals = nil
	err = utils.StructTransformer(client, &clientInResponse)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	clientInResponse.Deals = clientDealsToResponse(deals)

	c.JSON(200, clientInResponse)
}

func (ctr *Controller) UpdateClient(c *gin.Context) {
	id := c.Param("id")
	var clientToUpdateRequest scheme.ClientInUpdate
	err := c.BindJSON(&clientToUpdateRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	clientToUpdateRequest.ID = uint(u64)

	// transform request dcheme to client entity
	var clientToUpdate model.Client
	err = utils.StructTransformer(clientToUpdateRequest, &clientToUpdate)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	clientToUpdate.User = nil

	err = ctr.Service.Manager.UpdateClient(&clientToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, &clientToUpdate)
}

func (ctr *Controller) DeleteClient(c *gin.Context) {
	clientIDString := c.Param("id")
	u64, err := strconv.ParseUint(clientIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	clientID := uint(u64)

	err = ctr.Service.Manager.DeleteClient(clientID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, true)
}

func clientDealsToResponse(deals []model.Deal) (dealsInResponse []scheme.DealInResponse) {
	for _, deal := range deals {
		var dealInResponse scheme.DealInResponse
		dealCars := deal.Cars
		deal.Cars = nil
		utils.StructTransformer(deal, &dealInResponse)
		for _, car := range dealCars {
			customCharacteristicValMap := utils.CustomCharacteristicValsMapper(car.CarCustomCharacteristicValues)

			fmtCar := scheme.DealCarInResponse{
				ID:                   car.ID,
				VIN:                  car.VIN,
				Year:                 car.Year,
				CarMark:              car.CarModel.CarMark.Name,
				CarModel:             car.CarModel.Name,
				EngineTypeName:       customCharacteristicValMap[12],
				EngineVolume:         customCharacteristicValMap[13],
				EnginePower:          customCharacteristicValMap[14],
				KppTypeName:          customCharacteristicValMap[24],
				CarBodyStyleTypeName: customCharacteristicValMap[2],
				DriveWheelTypeName:   customCharacteristicValMap[27],
				DoorCount:            customCharacteristicValMap[3],
				SteeringTypeName:     customCharacteristicValMap[53],
				CarStatus:            car.CarStatus.Description,
				CarInterest:          car.DealCar.CarInterest.Description,

				Comment: car.DealCar.Comment,
			}

			if car.DealCar.Relevance != nil {
				fmtCar.Relevance = car.DealCar.Relevance.Description
			}

			dealInResponse.Cars = append(dealInResponse.Cars, fmtCar)
		}
		dealsInResponse = append(dealsInResponse, dealInResponse)
	}
	return
}
