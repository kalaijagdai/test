package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/useCases"
	excptn "gitlab.com/tradeincar/backend/api/utils"
	"strconv"
)

//create inspection
func (ctr *Controller) CreateInspection(c *gin.Context) {
	var inspection model.Inspection
	err := c.Bind(&inspection)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	err = ctr.Service.Manager.CreateInspection(&inspection)
	if err != nil {
		exp, errMessage := excptn.New(603, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, nil)
}

//get all inspection
func (ctr *Controller) GetAllInspections(c *gin.Context) {
	ins, err := ctr.Service.Manager.GetAllInspections()
	if err != nil {
		exp, errMessage := excptn.New(604, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	insToOutput, err := useCases.ConvertInspectionToOutput(ins)
	c.JSON(200, insToOutput)
}

func (ctr *Controller) GetAllInspectionsDetailed(c *gin.Context) {
	ins, err := ctr.Service.Manager.GetAllInspections()
	if err != nil {
		exp, errMessage := excptn.New(604, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, ins)
}

//create inner inspection
func (ctr *Controller) CreateInnerInspection(c *gin.Context) {
	var innerIns model.InnerInspection
	err := c.Bind(&innerIns)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	err = ctr.Service.Manager.CreateInnerInspection(&innerIns)
	if err != nil {
		exp, errMessage := excptn.New(605, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, nil)
}

//get all inner inspections
func (ctr *Controller) GetAllInnerInspections(c *gin.Context) {
	ins, err := ctr.Service.Manager.GetAllInnerInspections()
	if err != nil {
		exp, errMessage := excptn.New(606, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	c.JSON(200, ins)
}

//create inner inspection view
func (ctr *Controller) CreateInnerInspectionView(c *gin.Context) {
	var innerInsView model.InnerInspectionView
	err := c.Bind(&innerInsView)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	err = ctr.Service.Manager.CreateInnerInspectionView(&innerInsView)
	if err != nil {
		exp, errMessage := excptn.New(606, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, nil)
}

//get all inner inspections
func (ctr *Controller) GetAllInnerInspectionViews(c *gin.Context) {
	ins, err := ctr.Service.Manager.GetAllInnerInspectionViews()
	if err != nil {
		exp, errMessage := excptn.New(607, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	c.JSON(200, ins)
}

//create inner inspection view type
func (ctr *Controller) CreateInnerInspectionViewType(c *gin.Context) {
	var viewType model.InnerInspectionViewType
	err := c.Bind(&viewType)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	err = ctr.Service.Manager.CreateInnerInspectionViewType(&viewType)
	if err != nil {
		exp, errMessage := excptn.New(601, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, nil)
}

//get all inner inspection view type
func (ctr *Controller) GetAllInnerInspectionViewTypes(c *gin.Context) {
	viewTypes, err := ctr.Service.Manager.GetAllInnerInspectionViewTypes()
	if err != nil {
		exp, errMessage := excptn.New(602, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	c.JSON(200, viewTypes)
}

func (ctr *Controller) UpdateInspection(c *gin.Context) {
	id := c.Param("id")
	var inspectionToUpdate model.Inspection
	err := c.BindJSON(&inspectionToUpdate)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	inspectionToUpdate.ID = uint(u64)
	inspectionToUpdate.InnerInspections = []model.InnerInspection{}

	err = ctr.Service.Manager.UpdateInspection(&inspectionToUpdate)
	if err != nil {
		exp, errMessage := excptn.New(163, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, &inspectionToUpdate)
}

func (ctr *Controller) UpdateInnerInspection(c *gin.Context) {
	id := c.Param("id")
	var innerInspectionToUpdate model.InnerInspection
	err := c.BindJSON(&innerInspectionToUpdate)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	innerInspectionToUpdate.ID = uint(u64)
	innerInspectionToUpdate.InnerInspectionViews = []model.InnerInspectionView{}

	err = ctr.Service.Manager.UpdateInnerInspection(&innerInspectionToUpdate)
	if err != nil {
		exp, errMessage := excptn.New(163, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, &innerInspectionToUpdate)
}

func (ctr *Controller) UpdateInnerInspectionView(c *gin.Context) {
	id := c.Param("id")
	var innerInspectionToUpdate model.InnerInspectionView
	err := c.BindJSON(&innerInspectionToUpdate)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	innerInspectionToUpdate.ID = uint(u64)

	err = ctr.Service.Manager.UpdateInnerInspectionView(&innerInspectionToUpdate)
	if err != nil {
		exp, errMessage := excptn.New(163, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, &innerInspectionToUpdate)
}

func (ctr *Controller) UpdateInnerInspectionViewType(c *gin.Context) {
	id := c.Param("id")
	var inspectionViewTypeToUpdate model.InnerInspectionViewType
	err := c.BindJSON(&inspectionViewTypeToUpdate)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	inspectionViewTypeToUpdate.ID = uint(u64)

	err = ctr.Service.Manager.UpdateInnerInspectionViewType(&inspectionViewTypeToUpdate)
	if err != nil {
		exp, errMessage := excptn.New(163, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, &inspectionViewTypeToUpdate)
}
