package controller

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	excptn "gitlab.com/tradeincar/backend/api/utils"
)

func (ctr *Controller) AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		var token string
		auth := c.Request.Header.Get("Authorization")
		token = strings.TrimPrefix(auth, "Bearer ")

		if token == "" {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"message": "Unauthorized",
			})
			return
		}

		user, err := ctr.Service.Manager.GetUserBySessionToken(token)
		if err != nil {
			fmt.Println(err.Error())
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"message": "Unauthorized",
			})
			return
		}

		c.Set("user", user)
	}
}

func (ctr *Controller) CanBeModifiedOnThisStatus(c *gin.Context) {
	// get dest assessment id from params
	paramID := c.Param("id")
	id, err := strconv.ParseUint(paramID, 10, 32)
	if err != nil {
		exp, errMessage := excptn.New(3, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	assessmentID := uint(id)

	// get assessment by id
	assessmentToUpdate, err := ctr.Service.Manager.GetAssessmentWithStatusByID(assessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// check if can we modify data in the current assessment status
	if assessmentToUpdate.AssessmentStatus.Name == "ON_MANAGER_APPROVAL" {
		err = errors.New("Can not modify assessment data while the status is 'ON_MANAGER_APPROVAL'")
		exp, errMessage := excptn.New(113, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		c.Abort()
		return
	}
	// Pass on to the next-in-chain
	c.Next()
}
