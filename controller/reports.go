package controller

import (
	"errors"
	"strconv"

	excptn "gitlab.com/tradeincar/backend/api/utils"

	"github.com/gin-gonic/gin"
	validator "github.com/go-playground/validator/v10"
	"gitlab.com/tradeincar/backend/api/model"
)

// CreateReport controller
func (ctr *Controller) CreateReport(c *gin.Context) {
	user := c.MustGet("user").(model.User)
	// bind with model
	var report model.Report
	err := c.Bind(&report)

	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	report.UserID = user.ID
	report.StatusID = 1

	err = validator.New().Struct(report)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	var dealerships []uint
	for _, d := range user.Dealerships {
		dealerships = append(dealerships, d.ID)
		if d.ID == report.DealershipID {
			report.Dealership = d.Name
		}
	}
	if _, found := FindInt(dealerships, report.DealershipID); !found {
		e := "foreign dealership or mark"
		exp, errMessage := excptn.New(704, errors.New(e))
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	_, err = ctr.Service.Manager.CreateReport(&report)

	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(201, report)
}

// GetReports controller
func (ctr *Controller) GetReports(c *gin.Context) {
	user := c.MustGet("user").(model.User)

	query := new(model.QueryGetReports)
	err := c.ShouldBindQuery(&query)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	query.UserID = user.ID
	reports, err := ctr.Service.Manager.GetReports(query, user.RoleID)

	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// reportsWithoutNull := new([]model.Report)

	// if len(*reports) == 0 {
	// 	reportsWithoutNull = reports
	// } else {
	// 	for _, e := range *reports {
	// 		if len(e.Sells) != 0 {
	// 			*reportsWithoutNull = append(*reportsWithoutNull, e)
	// 		}
	// 	}
	// }

	c.JSON(200, &reports)
}

//CreateSellsByReportID controller
func (ctr *Controller) CreateSellsByReportID(c *gin.Context) {
	reportID, err := strconv.ParseUint(c.Param("reportID"), 10, 64)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	var sell model.Sell
	err = c.Bind(&sell)

	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	err = validator.New().Struct(sell)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	if sell.PayForm == "Кредит" || sell.PayForm == "Льготный кредит" {
		if sell.Bank == "" {
			exp, errMessage := excptn.New(1, errors.New("Key: 'Sell.Bank' Error:Field validation for 'Bank' failed on the 'required' tag"))
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	}

	if sell.CountryOfManufacture == "Казахстан" {
		if sell.Factory == "" {
			exp, errMessage := excptn.New(1, errors.New("Key: 'Sell.Factory' Error:Field validation for 'Factory' failed on the 'required' tag"))
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	}

	sell.ReportID = uint(reportID)

	_, err = ctr.Service.Manager.CreateSellByReportID(&sell)

	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(201, sell)
}

//GetSellsByReportID controller
func (ctr *Controller) GetSellsByReportID(c *gin.Context) {

	user := c.MustGet("user").(model.User)

	reportID, err := strconv.Atoi(c.Param("reportID"))

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	query := new(model.QueryGetReports)
	err = c.ShouldBindQuery(&query)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	query.UserID = user.ID
	sells, totalCount, err := ctr.Service.Manager.GetSellsByReportID(query, reportID)
	report, err := ctr.Service.Manager.GetReportByID(reportID)
	report.Sells = *sells
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, gin.H{
		"reports": report,
		"total":   totalCount,
	})
}

func (ctr *Controller) ReportStatuses(c *gin.Context) {

	status, err := ctr.Service.Manager.GetStatuses()
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, status)
}

//UpdateStatusByReportID controller
func (ctr *Controller) UpdateStatusByReportID(c *gin.Context) {

	const ROLE_OWNER = "OWNER"
	const STATUS_ON_PROCESSING = "ON_PROCESSING"

	user := c.MustGet("user").(model.User)

	var status model.AkabStatus
	err := c.Bind(&status)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	statuses, err := ctr.Service.Manager.GetStatuses()
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	for _, e := range *statuses {
		if e.ID == status.ID {
			status.Name = e.Name
		}
	}

	if user.Role.Name != ROLE_OWNER && status.Name != STATUS_ON_PROCESSING {
		exp, _ := excptn.New(705, errors.New("Unable to access"))
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
		})
		return
	} else {
		reportID, err := strconv.Atoi(c.Param("reportID"))

		if err != nil {
			c.JSON(400, gin.H{
				"message": err.Error(),
			})
			return
		}

		report, err := ctr.Service.Manager.UpdateReportStatusByReport(reportID, status.ID)

		if err != nil {
			exp, errMessage := excptn.New(172, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}

		c.JSON(201, report)
	}

}

//UpdateSellbySellId controller
func (ctr *Controller) GetSellbySellId(c *gin.Context) {

	sellID, err := strconv.Atoi(c.Param("sellID"))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	sell, err := ctr.Service.Manager.GetSell(sellID)

	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, sell)
}

//UpdateSellbySellId controller
func (ctr *Controller) UpdateSellbySellId(c *gin.Context) {

	user := c.MustGet("user").(model.User)

	sellID, err := strconv.Atoi(c.Param("sellID"))

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	var sellToUpdate model.Sell
	err = c.Bind(&sellToUpdate)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	sellToUpdate.IsUpdated = true

	sell, err := ctr.Service.Manager.GetSell(sellID)

	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	_, err = ctr.Service.Manager.UpdateSell(sellID, &sellToUpdate)

	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	var sellToHistory model.SellsUpdateHistory
	sellToHistory.Sell = *sell

	sellToHistory.UserID = user.ID

	_, err = ctr.Service.Manager.CreateSellUpdateHistory(&sellToHistory)

	c.JSON(200, sellToUpdate)
}

//DeleteSellbySellId controller
func (ctr *Controller) DeleteSellbySellId(c *gin.Context) {

	sellID, err := strconv.Atoi(c.Param("sellID"))

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	err = ctr.Service.Manager.DeleteSell(sellID)

	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, true)
}

//GetReportsTopModels controller
func (ctr *Controller) GetReportsTopModels(c *gin.Context) {
	query := new(model.QueryGetReportsInfoGraph)
	err := c.ShouldBindQuery(query)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	markModels, err := ctr.Service.Manager.GetTopModels(query)

	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, markModels)
}

//GetReportsTopMarks controller
func (ctr *Controller) GetReportsTopMarks(c *gin.Context) {
	query := new(model.QueryGetReportsInfoGraph)
	err := c.ShouldBindQuery(query)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	markModels, err := ctr.Service.Manager.GetTopMarks(query)

	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, markModels)
}

// GetTotalSells controller
func (ctr *Controller) GetTotalSells(c *gin.Context) {
	query := new(model.QueryGetReportsInfoGraph)
	err := c.ShouldBindQuery(query)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	totalSells, err := ctr.Service.Manager.GetTotalSells(query)

	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, totalSells)
}

// GetTotalSells controller
func (ctr *Controller) GetTotalsForDashboard(c *gin.Context) {
	query := new(model.QueryGetReportsInfoGraph)
	err := c.ShouldBindQuery(query)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	salesAmount, err := ctr.Service.Manager.GetSalesAmountForDashboard(query)

	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	queryLast := new(model.QueryGetReportsInfoGraph)
	queryLast.Year = query.Year - 1

	lastYearTotals, err := ctr.Service.Manager.GetTotalSells(queryLast)

	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	currentYearTotals, err := ctr.Service.Manager.GetTotalSells(query)

	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	localSales, err := ctr.Service.Manager.GetLocalSalesRatioForDashboard(query)

	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, gin.H{
		"salesAmount":       salesAmount,
		"lastYearTotals":    lastYearTotals,
		"currentYearTotals": currentYearTotals,
		"localSales":        localSales,
	})
}
