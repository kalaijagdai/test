package controller

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
)

func (ctr *Controller) AddAssessmentDocument(c *gin.Context) {
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)

	var document model.AssessmentDocument
	err = c.BindJSON(&document)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	document.AssessmentID = assessmentID
	documentType, err := ctr.Service.Dict.GetAssessmentDocumentTypeByID(document.AssessmentDocumentTypeID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	document.AssessmentDocumentType = &documentType

	err = ctr.Service.Manager.AddAssessmentDocument(&document)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, document)
}

func (ctr *Controller) GetDocumentsByAssessmentID(c *gin.Context) {
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)

	documents, err := ctr.Service.Manager.GetDocumentsByAssessmentID(assessmentID)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, documents)
}

func (ctr *Controller) DeleteAssessmentDocument(c *gin.Context) {
	documentIDString := c.Param("id")
	u64, err := strconv.ParseUint(documentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	documentID := uint(u64)

	err = ctr.Service.Manager.DeleteAssessmentDocument(documentID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, true)
}
