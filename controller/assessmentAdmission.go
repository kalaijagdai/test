package controller

import (
	"errors"
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/useCases"
	excptn "gitlab.com/tradeincar/backend/api/utils"
)

func (ctr *Controller) UpdateAssessmentAdmission(c *gin.Context) {
	user := c.MustGet("user").(model.User)
	var assessmentAdmissionRequest scheme.AsessmentAdmissionInUpdate
	err := c.Bind(&assessmentAdmissionRequest)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// AdmissionClient
	var admissionClient model.Client
	if assessmentAdmissionRequest.Client != nil {
		// if Client already exist
		if assessmentAdmissionRequest.Client.ID > 0 {
			admissionClient, err = ctr.Service.Manager.GetClientByID(assessmentAdmissionRequest.Client.ID)
			if err != nil {
				exp, errMessage := excptn.New(171, err)
				c.JSON(exp.HTTPCode, gin.H{
					"code":    exp.Code,
					"message": exp.Message,
					"error":   errMessage,
				})
				return
			}
		} else {
			// if new Client sent
			admissionClient, err = useCases.CreateClientEntity(ctr.Service, assessmentAdmissionRequest.Client, &user)
			if err != nil {
				c.JSON(400, gin.H{
					"message": err.Error(),
				})
				return
			}
		}
	}

	assessmentAdmissionRequestAssessmentPrice := assessmentAdmissionRequest.AssessmentPrice.ParseFloat()
	assessmentAdmissionRequestAdmissionPrice := assessmentAdmissionRequest.AdmissionPrice.ParseFloat()

	// create assessmentAdmission model
	assessmentAdmissionToUpdate := model.AssessmentAdmission{
		ID:              assessmentAdmissionRequest.ID,
		AssessmentID:    assessmentAdmissionRequest.AssessmentID,
		SaleLink:        assessmentAdmissionRequest.SaleLink,
		AssessmentPrice: &assessmentAdmissionRequestAssessmentPrice,
		AdmissionPrice:  &assessmentAdmissionRequestAdmissionPrice,
		ContractNumber:  assessmentAdmissionRequest.ContractNumber,
		ClientID:        admissionClient.ID,
	}

	// get assessment by AssessmentID
	assessment, err := ctr.Service.Manager.GetAssessmentByID(assessmentAdmissionRequest.AssessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	fmt.Println("assessment.Status: ", assessment.AssessmentStatus)
	if assessment.AssessmentStatus.Name != "SERVICE_CENTER_REMOVAL" {
		err = errors.New("Not satisfied with status")
		exp, errMessage := excptn.New(154, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	err = ctr.Service.Manager.UpdateAssessmentAdmission(&assessmentAdmissionToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	assessmentAdmissionToUpdate.Client = &admissionClient

	c.JSON(200, &assessmentAdmissionToUpdate)
}

func (ctr *Controller) DeleteAssessmentAdmission(c *gin.Context) {
	var assessmentAdmission model.AssessmentAdmission
	err := c.Bind(&assessmentAdmission)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// remove assessmentAdmission from Assessment
	err = ctr.Service.Manager.DeleteAssessmentAdmission(&assessmentAdmission)
	if err != nil {
		exp, errMessage := excptn.New(107, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, true)
}
