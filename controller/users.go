package controller

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"text/template"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/utils"
	excptn "gitlab.com/tradeincar/backend/api/utils"
	"golang.org/x/crypto/bcrypt"
)

func (ctr *Controller) CreateUserEntity(userRequest *model.UserRequest, me *model.User) (createUser model.User, err error) {
	// get Enum values
	userStatus, err := ctr.Service.Dict.GetUserStatusByName("ACTIVE")
	if err != nil {
		return
	}

	userRole, err := ctr.Service.Dict.GetRoleByID(userRequest.RoleID)
	if err != nil {
		return
	}

	if me.Role.Name == "MANAGER" && userRole.Name == "MANAGER" {
		err = errors.New("Manager can not create managers")
		return
	}

	password := utils.PasswordGenerator(8)

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return
	}
	passwordStr := string(passwordHash)

	// create model.User entity
	createUser = model.User{
		LastName:       userRequest.LastName,
		FirstName:      userRequest.FirstName,
		MiddleName:     userRequest.MiddleName,
		Dealerships:    userRequest.Dealerships,
		CanExportExcel: userRequest.CanExportExcel,
		Telephone:      userRequest.Telephone,
		Email:          &userRequest.Email,
		PasswordHash:   &passwordStr,
		RoleID:         userRole.ID,
		UserStatusID:   userStatus.ID,
	}

	// save to DB
	err = ctr.Service.Manager.CreateUser(&createUser)
	if err != nil {
		return
	}

	createUser.Role = userRole

	//email tmp & sender
	emailTmpl := `Здравствуйте,  {{.FirstName}}!
В системе MycarPro для вас создан профиль {{.Role}}. Теперь вы можете войти в систему, используя следующие данные:

email:  {{.Email}}
логин:  {{.Email}}
пароль: {{.Password}}

Войти в систему: https://pro.mycar.kz


С уважением,
Команда MycarPro.
	`

	emailData := map[string]string{
		"FirstName": userRequest.FirstName,
		"Role":      userRole.Description,
		"Email":     userRequest.Email,
		"Password":  password,
	}

	temp := template.Must(template.New("email").Parse(emailTmpl))
	builder := &strings.Builder{}
	if err := temp.Execute(builder, emailData); err != nil {
		fmt.Println(err)
	}

	to := userRequest.Email
	msg := builder.String()

	utils.SendMail(to, msg)

	return
}

func (ctr *Controller) CreateUser(c *gin.Context) {
	var userRequest model.UserRequest
	err := c.Bind(&userRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	errorCode := 168
	me := c.MustGet("user").(model.User)
	createUser, err := ctr.CreateUserEntity(&userRequest, &me)

	if err != nil {
		// if not unique email
		if err.Error() == `ERROR: duplicate key value violates unique constraint "users_email_key" (SQLSTATE 23505)` {
			errorCode = 169
		}

		exp, errMessage := excptn.New(errorCode, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, &createUser)
}

func (ctr *Controller) GetAllSpecialist(c *gin.Context) {
	q := c.Request.URL.Query()
	q.Add("role", "SPECIALIST")
	c.Request.URL.RawQuery = q.Encode()
	ctr.GetAllUsers(c)
}

func (ctr *Controller) GetAllUsers(c *gin.Context) {
	user := c.MustGet("user").(model.User)
	queryParams := utils.GetQueryParams(c)
	limit, err := strconv.ParseUint(c.DefaultQuery("limit", "100"), 10, 32)
	page, err := strconv.ParseUint(c.DefaultQuery("page", "1"), 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	offset := (page - 1) * limit
	users, err := ctr.Service.Manager.GetAllUsers(queryParams, user, limit, offset)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, users)
}

func (ctr *Controller) UpdateUser(c *gin.Context) {
	id := c.Param("id")
	var userToUpdate model.User
	err := c.BindJSON(&userToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	userToUpdate.ID = uint(u64)

	// get user before update by id
	userBeforeUpdate, err := ctr.Service.Manager.GetUserByID(userToUpdate.ID)
	if err != nil {
		exp, errMessage := excptn.New(166, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// if email changed
	var newPassword string
	if *userBeforeUpdate.Email != *userToUpdate.Email {
		newPassword = utils.PasswordGenerator(8)
		newPasswordHash, err := bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)
		if err != nil {
			return
		}
		passwordStr := string(newPasswordHash)
		userToUpdate.PasswordHash = &passwordStr
	}

	managerRole, err := ctr.Service.Dict.GetRoleByName("MANAGER")

	// manager can not update the role
	me := c.MustGet("user").(model.User)
	if me.Role.Name == "MANAGER" && me.ID == userToUpdate.ID {
		if (userToUpdate.Role != (model.Role{}) && userToUpdate.Role.Name != "MANAGER") || (userToUpdate.RoleID != managerRole.ID) {
			err = errors.New("Manager can not change his role")
			exp, errMessage := excptn.New(120, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	}

	errorCode := 170
	err = ctr.Service.Manager.UpdateUser(&userToUpdate)
	if err != nil {
		// if not unique email
		if err.Error() == `ERROR: duplicate key value violates unique constraint "users_email_key" (SQLSTATE 23505)` {
			errorCode = 169
		}

		exp, errMessage := excptn.New(errorCode, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// if email changed
	if userBeforeUpdate.Email != userToUpdate.Email {
		//email tmp & sender
		emailTmpl := `Здравствуйте,  {{.FirstName}}!

Ваш новый пароль: ` + newPassword + `

Войти в систему: https://pro.mycar.kz


С уважением,
Команда MycarPro.
	`

		temp := template.Must(template.New("email").Parse(emailTmpl))
		builder := &strings.Builder{}
		if err := temp.Execute(builder, userToUpdate); err != nil {
			fmt.Println(err)
		}

		to := userToUpdate.Email
		msg := builder.String()

		utils.SendMail(*to, msg)
	}

	c.JSON(200, &userToUpdate)
}

func (ctr *Controller) RemoveDealershipFromUser(c *gin.Context) {
	id := c.Param("userId")
	var user model.User
	var dealership model.Dealership
	err := c.BindJSON(&dealership)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	user.ID = uint(u64)

	err = ctr.Service.Manager.RemoveDealershipFromUser(&dealership, &user)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, nil)
}

func (ctr *Controller) GetUser(c *gin.Context) {
	userIDString := c.Param("id")
	u64, err := strconv.ParseUint(userIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	userID := uint(u64)

	user, err := ctr.Service.Manager.GetUserByID(userID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, user)
}

func (ctr *Controller) DeleteUser(c *gin.Context) {
	userIDString := c.Param("id")
	u64, err := strconv.ParseUint(userIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	userID := uint(u64)

	err = ctr.Service.Manager.DeleteEmailByID(userID)
	if err != nil {
		exp, errMessage := excptn.New(119, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	err = ctr.Service.Manager.DeleteUser(userID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, true)
}

func (ctr *Controller) GetMe(c *gin.Context) {
	user := c.MustGet("user").(model.User)
	c.JSON(200, user)
}
