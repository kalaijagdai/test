package controller

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/utils"
)

func (ctr *Controller) AddAssessmentPhoto(c *gin.Context) {
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)

	var photoInCreate scheme.AssessmentPhotoInCreate
	err = c.BindJSON(&photoInCreate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	photoType, err := ctr.Service.Dict.GetAssessmentPhotoTypeByName(photoInCreate.AssessmentPhotoType)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	photo := model.AssessmentPhoto{
		AssessmentPhotoTypeID: photoType.ID,
		URL:                   photoInCreate.URL,
		AssessmentID:          assessmentID,
	}

	multiplePhotoTypes := []string{"ASSESSMENT_PHOTO", "ADDITIONAL"}
	if _, isMultipleType := utils.FindInSlice(multiplePhotoTypes, photoType.Name); !isMultipleType {
		if sametypePhoto, _ := ctr.Service.Manager.GetSametypePhotosByAssessmentID(assessmentID, photoType.ID); sametypePhoto.ID > 0 {
			photo.ID = sametypePhoto.ID
			photo.IsFavorite = sametypePhoto.IsFavorite
		}
	}

	err = ctr.Service.Manager.AddAssessmentPhoto(&photo)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, photo)
}

func (ctr *Controller) GetPhotosByAssessmentID(c *gin.Context) {
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)

	photos, err := ctr.Service.Manager.GetPhotosByAssessmentID(assessmentID)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, photos)
}

func (ctr *Controller) DeleteAssessmentPhoto(c *gin.Context) {
	photoIDString := c.Param("id")
	u64, err := strconv.ParseUint(photoIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	photoID := uint(u64)

	err = ctr.Service.Manager.DeleteAssessmentPhoto(photoID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, true)
}

func (ctr *Controller) ChoiseFavoriteAssessmentPhoto(c *gin.Context) {
	assessmentIDString := c.Param("id")
	u64, err := strconv.ParseUint(assessmentIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	assessmentID := uint(u64)

	err = ctr.Service.Manager.UnfavoritePhotosByAssesmentID(assessmentID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	photoIDString := c.Param("photoID")
	u64, err = strconv.ParseUint(photoIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	photoID := uint(u64)

	photo := model.AssessmentPhoto{
		ID:           photoID,
		AssessmentID: assessmentID,
		IsFavorite:   true,
	}

	err = ctr.Service.Manager.UpdateAssessmentPhoto(&photo)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, photo)
}
