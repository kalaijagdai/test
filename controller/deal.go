package controller

import (
	"fmt"
	"strconv"
	"strings"
	"sync"
	"text/template"
	"time"

	"gitlab.com/tradeincar/backend/api/scheme"
	"gitlab.com/tradeincar/backend/api/useCases"
	"gitlab.com/tradeincar/backend/api/utils"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	excptn "gitlab.com/tradeincar/backend/api/utils"
)

func (ctr *Controller) CreateDeal(c *gin.Context) {
	user := c.MustGet("user").(model.User)

	var dealRequest scheme.DealInCreate
	err := c.Bind(&dealRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}


	if dealRequest.UserID  > 0 {
		user, err = ctr.Service.Manager.GetUserByID(dealRequest.UserID)
		if err != nil {
			exp, errMessage := excptn.New(166, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	}

	// get Enum values
	dealGoal, err := ctr.Service.Dict.GetDealGoalByName(dealRequest.DealGoal)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	dealStatus, err := ctr.Service.Dict.GetDealStatusByName("NEW_CLIENT")
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	communication, err := ctr.Service.Dict.GetCommunicationByName(dealRequest.Communication)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	startContact := time.Now()
	// contact
	var dealContact model.Contact
	if dealRequest.Contact != nil {
		dealContact, err = ctr.CreateContactEntity(dealRequest.Contact)
		if err != nil {
			fmt.Println(err.Error())
			c.JSON(400, gin.H{
				"message": err.Error(),
			})
			return
		}
	}
	fmt.Printf("%v: %v\n", "time for Contact: ", time.Since(startContact).Nanoseconds())

	startClient := time.Now()
	// DealClient
	// if Client already exist (only ID sent)
	// todo get client not client id
	var dealClient model.Client
	if dealRequest.Client.ID > 0 {
		dealClient, err = ctr.Service.Manager.GetClientByID(dealRequest.Client.ID)
		if err != nil {
			c.JSON(404, gin.H{
				"message": "Client with that ID not found",
			})
			return
		}
	} else if dealRequest.Client != nil {
		// //TODO: dirty hack
		// clientReferral, _ := service.Dict.GetReferralByName(dealRequest.Client.Referral)

		// if new Client sent
		dealClientRequest := scheme.ClientInCreate{
			ID:        dealRequest.Client.ID,
			Name:      dealRequest.Client.Name,
			Telephone: dealRequest.Client.Telephone,
			Referral:  dealRequest.Client.Referral,
		}
		dealClient, err = useCases.CreateClientEntity(ctr.Service, &dealClientRequest, &user)
		if err != nil {
			fmt.Println(err.Error())
			c.JSON(400, gin.H{
				"message": err.Error(),
			})
			return
		}
	}
	fmt.Printf("%v: %v\n", "time for Client: ", time.Since(startClient).Nanoseconds())

	// startUser := time.Now()
	// // DealUser
	// var dealUser model.User
	// if dealRequest.UserID > 0 {
	// 	dealUser, err = ctr.Service.Manager.GetUserByID(dealRequest.UserID)
	// 	if err != nil {
	// 		fmt.Println(err.Error())
	// 		c.JSON(404, gin.H{
	// 			"message": "User with that ID not found",
	// 		})
	// 		return
	// 	}
	// }
	// fmt.Printf("%v: %v\n", "time for User: ", time.Since(startUser).Nanoseconds())

	// here we will store all car interests (to update CarDeals after Deal will be created)
	carInterests := make(map[string]map[string]string)

	var cars []model.Car
	for _, car := range dealRequest.Cars {

		existCar, _ := ctr.Service.Manager.GetCarByID(car.ID)
		if existCar.ID < 1 {
			// create new Car
			car.CarStatus = "DEAL"
			newCar, err := useCases.CreateDealCarEntity(ctr.Service, &car)
			cars = append(cars, newCar)
			if err != nil {
				fmt.Println(err.Error())
				c.JSON(400, gin.H{
					"message": err.Error(),
				})
				return
			}

			carInterests[newCar.VIN] = map[string]string{
				"CarInterest": car.CarInterest,
				"Relevance":   car.Relevance,
				"Comment":     car.Comment,
			}
		} else {
			cars = append(cars, existCar)
			carInterests[existCar.VIN] = map[string]string{
				"CarInterest": car.CarInterest,
				"Relevance":   car.Relevance,
				"Comment":     car.Comment,
			}
		}
	}

	finalCars := cars

	// create model.Deal entity
	createDeal := model.Deal{
		ClientID: dealClient.ID,

		UserID: user.ID,

		DealershipID: dealRequest.DealershipID,

		Cars: finalCars,

		Contacts: []model.Contact{dealContact},

		CommunicationID: communication.ID,

		DealGoalID: dealGoal.ID,

		DealStatusID: dealStatus.ID,

		// RelevanceID: relevance.ID,
	}

	startCreateDeal := time.Now()

	// save to Deal to DB
	err = ctr.Service.Manager.CreateDeal(&createDeal)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	fmt.Printf("%v: %v\n", "time for CreateDeal: ", time.Since(startCreateDeal).Nanoseconds())

	startCarInterestUpdate := time.Now()
	var wg sync.WaitGroup
	wg.Add(len(finalCars))
	for i := 0; i < len(finalCars); i++ {
		go func(i int) {
			defer wg.Done()

			// get Interest entity by interest string
			carInterestName := carInterests[finalCars[i].VIN]
			carInterestEntity, err := ctr.Service.Dict.GetCarInterestByName(carInterestName["CarInterest"])
			if err != nil {
				fmt.Println(err.Error())
				c.JSON(400, gin.H{
					"message": err.Error(),
				})
				return
			}

			relevance, err := ctr.Service.Dict.GetRelevanceByName(carInterestName["Relevance"])
			if err != nil {
				return
			}

			// update Interest field in Dealcar
			updatedData := map[string]interface{}{
				"CarInterestID": carInterestEntity.ID,
				"RelevanceID":   relevance.ID,
				"Comment":       carInterestName["Comment"],
			}
			err = ctr.Service.Manager.UpdateDealCar(createDeal.ID, finalCars[i].ID, updatedData)
			if err != nil {
				c.JSON(400, gin.H{
					"message": err.Error(),
				})
				return
			}

		}(i)
	}
	wg.Wait()

	fmt.Printf("%v: %v\n", "time for CarInterestUpdate: ", time.Since(startCarInterestUpdate).Nanoseconds())

	go fmt.Println("PARALLEL")

	if createDeal.UserID < 1 {
		return
	}

	// notify user about new client
	emailTmpl := `Здравствуйте, {{.User.FirstName}}! 

К Вам привязан новый клиент
ID: {{.Client.ID}}
Name: {{.Client.Name}} {{.Client.Surname}}
Telephone: {{.Client.Telephone}}

С уважением,
Команда MycarPro
`
	temp := template.Must(template.New("email").Parse(emailTmpl))


	//send emails to dealership users
	notificationModel := scheme.NewClientNotification{
		User:       &user,
		Client: 	&dealClient,
	}
	builder := &strings.Builder{}
	if err := temp.Execute(builder, notificationModel); err != nil {
		fmt.Println(err)
	}

	to := user.Email
	msg := builder.String()

	if to != nil {
		go utils.SendMail(*to, msg)
	}

	c.JSON(200, &createDeal)
}

func (ctr *Controller) GetDeals(c *gin.Context) {
	user := c.MustGet("user").(model.User)
	queryParams := utils.GetQueryParams(c)
	limit, err := strconv.ParseUint(c.DefaultQuery("limit", "100"), 10, 32)
	page, err := strconv.ParseUint(c.DefaultQuery("page", "1"), 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	offset := (page - 1) * limit
	deals, err := ctr.Service.Manager.GetDeals(queryParams, user, limit, offset)

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, deals)
}

func (ctr *Controller) GetDeal(c *gin.Context) {
	dealIDString := c.Param("id")
	u64, err := strconv.ParseUint(dealIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	dealID := uint(u64)

	deal, err := ctr.Service.Manager.GetDealByID(dealID)
	if err != nil {
		exp, errMessage := excptn.New(162, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	var dealInResponse scheme.DealInResponse

	var dealCars []scheme.DealCarInResponse
	for _, car := range deal.Cars {
		customCharacteristicValMap := utils.CustomCharacteristicValsMapper(car.CarCustomCharacteristicValues)

		if car.CarModification == nil {
			car.CarModification = &model.CarModification{
				Name: "(Другая модификация)",
			}
		}

		fmtCar := scheme.DealCarInResponse{
			ID:   car.ID,
			VIN:  car.VIN,
			Year: car.Year,
			// CarMark:              car.CarModel.CarMark.Name,
			CarModel:             car.CarModel.Name,
			CarModification:      car.CarModification.Name,
			EngineTypeName:       customCharacteristicValMap[12],
			EngineVolume:         customCharacteristicValMap[13],
			EnginePower:          customCharacteristicValMap[14],
			KppTypeName:          customCharacteristicValMap[24],
			CarBodyStyleTypeName: customCharacteristicValMap[2],
			DriveWheelTypeName:   customCharacteristicValMap[27],
			DoorCount:            customCharacteristicValMap[3],
			SteeringTypeName:     customCharacteristicValMap[53],
		}

		dealCars = append(dealCars, fmtCar)
	}

	deal.Cars = nil

	err = utils.StructTransformer(deal, &dealInResponse)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	dealInResponse.Cars = dealCars

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, dealInResponse)
}
