package controller

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	validator "github.com/go-playground/validator/v10"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/utils"
	excptn "gitlab.com/tradeincar/backend/api/utils"
)

func (ctr *Controller) LoadExcel(c *gin.Context) {
	user := c.MustGet("user").(model.User)
	file, header, err := c.Request.FormFile("report")
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	filename := header.Filename
	//dealership := c.Request.FormValue("dealershipId")
	// подготовка к обращению апи
	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)
	reqFile, _ := writer.CreateFormFile("file", filename)
	_, err = io.Copy(reqFile, file)
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	//_ = writer.WriteField("dealershipId", dealership)
	err = writer.Close()
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	client := &http.Client{}
	req, err := http.NewRequest("POST", ctr.Service.Configuration.ApiHost+"headers", payload)
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	req.Header.Set("Content-Type", writer.FormDataContentType()) //мультипарт/форм-дата
	//req.Header.Add("Authorization", "Bearer a2d8373f620c2d8c2455f1abae88d1cd") //токен
	//обращение к апи
	res, err := client.Do(req)
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	defer res.Body.Close()
	//буферизация данных которые мы получили
	buf, err := ioutil.ReadAll(res.Body)
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	var response []string
	//десериялизация данных
	err = json.Unmarshal(buf, &response)
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	saveHeaders, err := ctr.Service.Manager.GetColumnMaps(int(user.ID))
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	if saveHeaders.UserID != 0 {
		c.JSON(200, gin.H{
			"saveHeaders": saveHeaders,
			"headers":     response,
		})
		return
	}
	c.JSON(200, gin.H{
		"headers": response,
	})
}

func (ctr *Controller) SaveReportLabel(c *gin.Context) {

	user := c.MustGet("user").(model.User)
	var response *model.AkabExcelResponse
	var headers *model.ColumnMap

	file, header, err := c.Request.FormFile("report")
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	filename := header.Filename
	rep := c.Request.FormValue("headers")
	err = json.Unmarshal([]byte(rep), &headers)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	headers.UserID = user.ID

	err = validator.New().Struct(headers)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	saveHeaders, err := ctr.Service.Manager.GetColumnMaps(int(user.ID))
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	if saveHeaders.UserID == user.ID {
		_, err = ctr.Service.Manager.UpdateReportLabelByUser(int(user.ID), headers)
		if err != nil {
			exp, errMessage := excptn.New(172, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	} else {
		_, err = ctr.Service.Manager.AddReportLabelByUser(headers)
		if err != nil {
			exp, errMessage := excptn.New(172, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}

	}

	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)
	reqFile, _ := writer.CreateFormFile("file", filename)
	_, err = io.Copy(reqFile, file)
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	_ = writer.WriteField("column", rep)
	err = writer.Close()
	if err != nil {
		exp, errMessage := excptn.New(172, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	client := &http.Client{}
	req, err := http.NewRequest("POST", ctr.Service.Configuration.ApiHost+"excel", payload)

	req.Header.Add("Content-Type", writer.FormDataContentType())
	//req.Header.Add("Authorization", "Bearer a2d8373f620c2d8c2455f1abae88d1cd")

	res, err := client.Do(req)
	if err != nil {
		exp, errMessage := excptn.New(701, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	//буферизация данных которые мы получили
	buf, err := ioutil.ReadAll(res.Body)
	if err != nil {
		exp, errMessage := excptn.New(701, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	//десериялизация данных
	err = json.Unmarshal(buf, &response)
	if err != nil {
		exp, errMessage := excptn.New(701, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	if response == nil || len(*response.Result) == 0 {
		e := "Error while reading file"

		exp, _ := excptn.New(701, errors.New(e))

		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
		})
		return
	}

	limit, err := strconv.ParseUint(c.DefaultQuery("limit", "100"), 10, 32)
	queryParams := utils.GetQueryParams(c)
	page, err := strconv.ParseUint(c.DefaultQuery("page", "1"), 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	offset := (page - 1) * limit
	// вытаскиваем дилеров
	dealers, err := ctr.Service.Manager.GetAllDealerships(queryParams, user, limit, offset)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	var marks []string
	for _, e := range dealers {
		for _, m := range e.Marks {
			marks = append(marks, m.Name)
		}
	}

	var dealerships []string
	for _, d := range user.Dealerships {
		dealerships = append(dealerships, d.Name)
	}

	var result []*model.Report
	var foreignDealerships []string
	var foreignMarks []string
	var problemWithNone string
	for _, e := range *response.Result {
		if e.Dealership != "None" {
			if _, found := Find(dealerships, e.Dealership); !found {
				if _, found := Find(foreignDealerships, e.Dealership); !found {
					foreignDealerships = append(foreignDealerships, e.Dealership)
				}
			}
			for _, s := range e.Sells {
				if _, found := Find(marks, s.CarMark); !found {
					if _, found := Find(foreignMarks, s.CarMark); !found {
						foreignMarks = append(foreignMarks, s.CarMark)
					}
				}
			}
		} else {
			problemWithNone = e.Error
		}

	}

	var e string
	if foreignDealerships != nil || foreignMarks != nil {
		e = "foreign dealership or mark"

		exp, errMessage := excptn.New(702, errors.New(e))
		if foreignDealerships != nil {
			exp.Fields = append(exp.Fields, utils.Field{
				Name:    "dealership",
				Message: foreignDealerships,
			})
		}
		if foreignMarks != nil {
			exp.Fields = append(exp.Fields, utils.Field{
				Name:    "mark",
				Message: foreignMarks,
			})
		}

		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
			"fields":  exp.Fields,
		})
		return
	}

	for indexResponse, e := range *response.Result {
		if e.Dealership != "None" {
			result = append(result, &model.Report{
				UserID:     user.ID,
				Sells:      []model.Sell{},
				Dealership: e.Dealership,
				Status:     &model.AkabStatus{ID: 2, Name: "ON_PROCESSING", Description: "В обработке"},
				StatusID:   2,
			})
			for _, elemDealer := range user.Dealerships {
				if elemDealer.Name == e.Dealership {
					result[indexResponse].DealershipID = elemDealer.ID
				}
			}
			//result[indexResponse].UserID = user.ID
			for sellIndex, sls := range e.Sells {
				e.Sells[sellIndex].Price = strings.Join(strings.Fields(sls.Price), "")
				price, _ := strconv.Atoi(e.Sells[sellIndex].Price)

				e.Sells[sellIndex].Price = strings.Join(strings.Fields(sls.PriceUSD), "")
				priceUSD, _ := strconv.Atoi(e.Sells[sellIndex].PriceUSD)
				country, err := ctr.Service.Manager.GetCountryIdByName(sls.CountryName)
				if err != nil {
					c.JSON(400, gin.H{
						"message": err.Error(),
					})
					return
				}
				var year int
				if sls.YearOfIssue == "" || sls.YearOfIssue == "None" {
					year = 0
				} else {
					yearF, err := strconv.ParseFloat(sls.YearOfIssue, 32)
					year = int(yearF)
					if err != nil {
						var yearOfIssue []string
						yearOfIssue = append(yearOfIssue, sls.YearOfIssue)
						exp, errMessage := excptn.New(702, errors.New("Год выпуска для машины с вин кодом "+sls.Vin+", в ДЦ "+sls.Dealership+" было введено ошибочно \""+sls.YearOfIssue+"\""))
						exp.Fields = append(exp.Fields, utils.Field{
							Name:    "yearOfIssue",
							Message: yearOfIssue,
						})
						c.JSON(exp.HTTPCode, gin.H{
							"code":    exp.Code,
							"message": exp.Message,
							"error":   errMessage,
							"fields":  exp.Fields,
						})
						return
					}
				}
				var dealerID uint
				for _, d := range user.Dealerships {
					if d.Name == sls.Dealership {
						dealerID = d.ID
					}
				}
				result[indexResponse].Sells = append(result[indexResponse].Sells, model.Sell{
					Bank:                 sls.Bank,
					CarGeneration:        sls.CarGeneration,
					CarMark:              sls.CarMark,
					CarModel:             sls.CarModel,
					CarModification:      sls.CarModification,
					Color:                sls.Color,
					CountryOfManufacture: sls.CountryOfManufacture,
					DateOfSale:           sls.DateOfSale,
					Dealership:           sls.Dealership,
					DealershipID:         dealerID,
					EngineVolume:         sls.EngineVolume,
					Factory:              sls.Factory,
					PayForm:              sls.PayForm,
					Price:                uint(price),
					PriceUSD:             uint(priceUSD),
					RegionOfSale:         sls.RegionOfSale,
					// ReportID = sls.ReportID
					CountryName:  sls.CountryName,
					CountryID:    country.ID,
					Transmission: sls.Transmission,
					TypeOfClient: sls.TypeOfClient,
					TypeOfFuel:   sls.TypeOfFuel,
					TypeOfDrive:  sls.TypeOfDrive,
					YearOfIssue:  uint(year),
					Vin:          sls.Vin})

			}
			_, err := ctr.Service.Manager.CreateReport(result[indexResponse])
			if err != nil {
				exp, errMessage := excptn.New(703, err)
				c.JSON(exp.HTTPCode, gin.H{
					"code":    exp.Code,
					"message": exp.Message,
					"error":   errMessage,
				})
				return
			}

		}
	}

	for _, e := range *response.ErrorHidden {

		_, err := ctr.Service.Manager.ErrorHiddenLog(&e)
		if err != nil {
			exp, errMessage := excptn.New(703, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	}

	c.JSON(201, gin.H{
		"result":  result,
		"problem": problemWithNone,
	})
}

func Find(slice []string, val string) (int, bool) {
	for i, item := range slice {
		if item == val {
			return i, true
		}
	}
	return -1, false
}

func FindInt(slice []uint, val uint) (int, bool) {
	for i, item := range slice {
		if item == val {
			return i, true
		}
	}
	return -1, false
}
