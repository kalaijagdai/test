package controller

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"net/http"
	"strings"
	"text/template"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
	"gitlab.com/tradeincar/backend/api/utils"
	excptn "gitlab.com/tradeincar/backend/api/utils"
	"golang.org/x/crypto/bcrypt"
)

func (ctr *Controller) Login(c *gin.Context) {
	var userLoginRequest model.UserLoginRequest
	err := c.Bind(&userLoginRequest)
	user, err := ctr.Service.Manager.GetUserByEmail(userLoginRequest.Email)
	if err != nil {
		exp, errMessage := excptn.New(156, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(*user.PasswordHash), []byte(userLoginRequest.Password))
	if err != nil {
		exp, errMessage := excptn.New(157, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(userLoginRequest.Email), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	hasher := md5.New()
	hasher.Write(hash)
	sessionToken := hex.EncodeToString(hasher.Sum(nil))

	err = ctr.Service.Manager.UpdateSessionTokenByUserID(user.ID, sessionToken)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"sessionToken": sessionToken,
	})
}

func (ctr *Controller) Logout(c *gin.Context) {
	user := c.MustGet("user").(model.User)
	err := ctr.Service.Manager.UpdateSessionTokenByUserID(user.ID, "")
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{})
}

func (ctr *Controller) ResetPassword(c *gin.Context) {
	var userLoginRequest model.UserLoginRequest
	err := c.Bind(&userLoginRequest)

	userToUpdate, err := ctr.Service.Manager.GetUserByEmail(userLoginRequest.Email)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	password := utils.PasswordGenerator(8)
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return
	}
	passwordStr := string(passwordHash)
	userToUpdate.PasswordHash = &passwordStr

	err = ctr.Service.Manager.UpdateUser(&userToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	//email tmp & sender
	emailTmpl := `Здравствуйте,  {{.FirstName}}!

Ваш новый пароль: ` + password + `

Войти в систему: https://pro.mycar.kz


С уважением,
Команда MycarPro.
	`

	temp := template.Must(template.New("email").Parse(emailTmpl))
	builder := &strings.Builder{}
	if err := temp.Execute(builder, userToUpdate); err != nil {
		fmt.Println(err)
	}

	to := userToUpdate.Email
	msg := builder.String()

	utils.SendMail(*to, msg)

	c.JSON(http.StatusOK, gin.H{})
}
