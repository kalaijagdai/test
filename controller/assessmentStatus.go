package controller

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"text/template"

	"gitlab.com/tradeincar/backend/api/scheme"

	"gitlab.com/tradeincar/backend/api/model"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/useCases"
	"gitlab.com/tradeincar/backend/api/utils"
	excptn "gitlab.com/tradeincar/backend/api/utils"
)

func (ctr *Controller) ChangeAssessmentStatus(c *gin.Context) {
	// get dest status from params
	assessmentStatusString := strings.ToUpper(c.Param("status"))
	dstStatus, err := ctr.Service.Dict.GetAssessmentStatusByName(assessmentStatusString)
	if err != nil {
		exp, errMessage := excptn.New(109, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// get status reason from body
	var assessmentStatusInPost scheme.AssessmentStatusInPost
	err = c.Bind(&assessmentStatusInPost)
	if err != nil {
		exp, errMessage := excptn.New(1, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// get dest assessment id from params
	paramID := c.Param("id")
	id, err := strconv.ParseUint(paramID, 10, 32)
	if err != nil {
		exp, errMessage := excptn.New(3, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	assessmentID := uint(id)

	// get assessment by id
	assessment, err := ctr.Service.Manager.GetAssessmentByID(assessmentID)
	if err != nil {
		exp, errMessage := excptn.New(102, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// if dest status is ON_MANAGER_APPROVAL then check if all required info is filled
	onManagerApprovalStatus, _ := ctr.Service.Dict.GetAssessmentStatusByName("ON_MANAGER_APPROVAL")
	if dstStatus.ID == onManagerApprovalStatus.ID {
		_, err = useCases.IsAssessmentReadyForOnManangerApproval(ctr.Service, &assessment)
		if err != nil {
			exp, errMessage := excptn.New(118, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	}

	// if dest status is DIAGNOSTICS then check if all required info is filled
	diagnosticsStatus, _ := ctr.Service.Dict.GetAssessmentStatusByName("DIAGNOSTICS")
	if dstStatus.ID == diagnosticsStatus.ID {
		_, err = useCases.IsAssessmentReadyForDiagnostics(ctr.Service, &assessment)
		if err != nil {
			exp, errMessage := excptn.New(118, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	}

	// if dest status is DIAGNOSTICS_ON_APPROVAL then check if all required info is filled
	diagnosticsOnApprovalStatus, _ := ctr.Service.Dict.GetAssessmentStatusByName("DIAGNOSTICS_ON_APPROVAL")
	if dstStatus.ID == diagnosticsOnApprovalStatus.ID {
		_, err = useCases.IsAssessmentReadyForDiagnosticsOnApproval(ctr.Service, &assessment)
		if err != nil {
			exp, errMessage := excptn.New(118, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	}

	// if dest status is SALE_PREPARATION then check if all required info is filled
	salePreaparationStatus, _ := ctr.Service.Dict.GetAssessmentStatusByName("SALE_PREPARATION")
	if dstStatus.ID == salePreaparationStatus.ID {
		_, err = useCases.IsAssessmentReadyForSalePreaparation(ctr.Service, &assessment)
		if err != nil {
			exp, errMessage := excptn.New(118, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	}

	// if dest status is RESERVE then check if all required info is filled
	reserveStatus, _ := ctr.Service.Dict.GetAssessmentStatusByName("RESERVE")
	if dstStatus.ID == reserveStatus.ID {
		_, err = useCases.IsAssessmentReadyForReserve(ctr.Service, &assessment)
		if err != nil {
			exp, errMessage := excptn.New(118, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	}

	// if dest status is TEMPORALLY_NOT_SALE then check if all required info is filled
	temporallyNotSaleStatus, _ := ctr.Service.Dict.GetAssessmentStatusByName("TEMPORALLY_NOT_SALE")
	if dstStatus.ID == temporallyNotSaleStatus.ID {
		_, err = useCases.IsAssessmentReadyForCloseSale(ctr.Service, &assessment)
		if err != nil {
			exp, errMessage := excptn.New(118, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	}

	// check if status transition is valid
	if !(utils.IsAssessmentStatusTransitionValid(assessment.AssessmentStatus.Name, dstStatus.Name)) {
		err := errors.New("The status transition from this status to desired is not allowed.")
		exp, errMessage := excptn.New(112, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	// update assessment status
	assessmentToUpdate := model.Assessment{ID: assessment.ID, AssessmentStatusID: dstStatus.ID}
	err = ctr.Service.Manager.UpdateAssessmentStatus(&assessmentToUpdate, assessmentStatusInPost.Reason, assessment.AssessmentStatusID)
	if err != nil {
		exp, errMessage := excptn.New(104, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}
	//fmt.Println("AssessmentToUpdate: ", assessmentToUpdate)

	// get all dealership users
	//dealershipUsers, err := ctr.Service.Manager.GetDealershipUsers(assessmentToUpdate.DealershipID)
	//if err != nil {
	//	c.JSON(400, gin.H{
	//		"message": err.Error(),
	//	})
	//	return
	//}

	// status change notification
	emailTmpl := `Здравствуйте, {{.User.FirstName}}!

Автомобиль переведен в другой статус.

Автомобиль
    ID: {{.Car.ID}}
    Авто: {{.Car.CarModel.CarMark.Name}} {{.Car.CarModel.Name}}
    VIN: {{.Car.VIN}}
ДЦ: {{.Dealership.Name}}
Статус: {{.AssessmentStatus.Description}}

С уважением,
Команда MycarPro`

	fmt.Println("dstStatus: ", dstStatus.AssessmentStage.Name)
	fmt.Println("assStat: ", assessment.AssessmentStatus.AssessmentStage.Name)
	// if car came to warehouse
	if dstStatus.AssessmentStage.Name == "IN_STOCK" && assessment.AssessmentStatus.AssessmentStage.Name == "ON_ASSESSMENT" {
		emailTmpl = `Здравствуйте, {{.User.FirstName}}!

На склад поступил новый автомобиль.

Автомобиль
    ID: {{.Car.ID}}
    Авто: {{.Car.CarModel.CarMark.Name}} {{.Car.CarModel.Name}}
    VIN: {{.Car.VIN}}
ДЦ {{.Dealership.Name}}


С уважением,
Команда MycarPro`
	}

	temp := template.Must(template.New("email").Parse(emailTmpl))

	//send emails to dealership users
	for _, dealershipUser := range assessment.Dealership.Users {
		notificationModel := scheme.StatusChangeNotification{
			User:             &dealershipUser,
			Car:              assessment.Car,
			Dealership:       assessment.Dealership,
			AssessmentStatus: &dstStatus,
		}
		builder := &strings.Builder{}
		if err := temp.Execute(builder, notificationModel); err != nil {
			fmt.Println(err)
		}

		to := dealershipUser.Email
		msg := builder.String()

		if to != nil {
			go utils.SendMail(*to, msg)
		}

	}

	// if ON_WORK > DIAGNOSTICS or ASSESSMENT_APPROVED > DIAGNOSTICS then need to copy last pricing
	if assessmentToUpdate.AssessmentStatusID == diagnosticsStatus.ID && (assessment.AssessmentStatus.Name == "ON_WORK" || assessment.AssessmentStatus.Name == "ASSESSMENT_APPROVED") {
		onWorkStatus, _ := ctr.Service.Dict.GetAssessmentStatusByName("ON_WORK")
		err = useCases.CopyPriceList(ctr.Service, assessmentToUpdate.ID, onWorkStatus.ID, diagnosticsStatus.ID)
		if err != nil {
			exp, errMessage := excptn.New(111, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}

	}

	// if SERVICE_CENTER_REMOVAL > SALE_PREPARATION then need to copy last pricing
	if assessmentToUpdate.AssessmentStatusID == salePreaparationStatus.ID && assessment.AssessmentStatus.Name == "SERVICE_CENTER_REMOVAL" {
		err = useCases.CopyPriceList(ctr.Service, assessmentToUpdate.ID, diagnosticsStatus.ID, salePreaparationStatus.ID)
		if err != nil {
			exp, errMessage := excptn.New(111, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	}

	// if status became SERVICE_CENTER_REMOVAL then need to create "Admission menu with preloaded price"
	serviceCenterRemovalStatus, _ := ctr.Service.Dict.GetAssessmentStatusByName("SERVICE_CENTER_REMOVAL")
	if assessmentToUpdate.AssessmentStatusID == serviceCenterRemovalStatus.ID {
		_, err = useCases.CreateAssessmentAdmissionEntity(ctr.Service, assessmentToUpdate.ID)
		if err != nil {
			exp, errMessage := excptn.New(111, err)
			c.JSON(exp.HTTPCode, gin.H{
				"code":    exp.Code,
				"message": exp.Message,
				"error":   errMessage,
			})
			return
		}
	}
}

//func (ctr *Controller) SendServiceCenterRemoval(c *gin.Context) {
//	paramID := c.Param("id")
//	id, err := strconv.ParseUint(paramID, 10, 32)
//	if err != nil {
//		exp, errMessage := excptn.New(3, err)
//		c.JSON(exp.HTTPCode, gin.H{
//			"code":    exp.Code,
//			"message": exp.Message,
//			"error":   errMessage,
//		})
//		return
//	}
//	assessmentToUpdateID := uint(id)
//
//	assessment, err := ctr.Service.Manager.GetAssessmentByID(assessmentToUpdateID)
//	if err != nil { //102
//		exp, errMessage := excptn.New(102, err)
//		c.JSON(exp.HTTPCode, gin.H{
//			"code":    exp.Code,
//			"message": exp.Message,
//			"error":   errMessage,
//		})
//		return
//	}
//
//	assessment, err = useCases.SendServiceCenterRemoval(ctr.Service, &assessment)
//	if err != nil {
//		exp, errMessage := excptn.New(110, err)
//		c.JSON(exp.HTTPCode, gin.H{
//			"code":    exp.Code,
//			"message": exp.Message,
//			"error":   errMessage,
//		})
//		return
//	}
//
//	_, err = useCases.CreateAssessmentAdmissionEntity(ctr.Service, assessment.ID)
//	if err != nil {
//		exp, errMessage := excptn.New(111, err)
//		c.JSON(exp.HTTPCode, gin.H{
//			"code":    exp.Code,
//			"message": exp.Message,
//			"error":   errMessage,
//		})
//		return
//	}
//}
