package controller

import (
	"github.com/gin-gonic/gin"
)


func (ctr *Controller) TestApi(c *gin.Context) {
	c.JSON(200, gin.H{
		"data": true,
		"message": "success",
	})
}