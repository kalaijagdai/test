package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/useCases"
	excptn "gitlab.com/tradeincar/backend/api/utils"
)


func (ctr *Controller) ManangeExpiredAssessmentsOnReserve(c *gin.Context) {
	err := useCases.ManangeExpiredAssessmentsOnReserve(ctr.Service)
	if err != nil {
		exp, errMessage := excptn.New(113, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, true)
}

func (ctr *Controller) ManangeNotChangedAssessmentsOnSale(c *gin.Context) {
	err := useCases.ManangeNotChangedAssessmentsOnSale(ctr.Service)
	if err != nil {
		exp, errMessage := excptn.New(114, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, true)
}