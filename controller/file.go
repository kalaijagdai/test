package controller

import (
	"io"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func (ctr *Controller) UploadFile(c *gin.Context) {
	file, header, err := c.Request.FormFile("file")
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	// unique file name
	newFileName := uuid.New().String() + header.Filename
	dst, err := os.Create("static/" + newFileName)
	defer dst.Close()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	_, err = io.Copy(dst, file)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	filepath := "/static/" + newFileName
	c.JSON(http.StatusOK, gin.H{"filepath": filepath})
}
