package controller

import (
	excptn "gitlab.com/tradeincar/backend/api/utils"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/tradeincar/backend/api/model"
)

func (ctr *Controller) CreateService(c *gin.Context) {
	// get id from param
	dealershipIDString := c.Param("id")
	u64, err := strconv.ParseUint(dealershipIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	dealershipID := uint(u64)

	// bind with model
	var service model.Service
	err = c.Bind(&service)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	if dealershipID > 0 {
		service.DealershipID = dealershipID
	}

	err = ctr.Service.Manager.CreateService(&service)
	if err != nil {
		exp, errMessage := excptn.New(173, err)
		c.JSON(exp.HTTPCode, gin.H{
			"code":    exp.Code,
			"message": exp.Message,
			"error":   errMessage,
		})
		return
	}

	c.JSON(200, &service)
}

func (ctr *Controller) GetServices(c *gin.Context) {
	services, err := ctr.Service.Manager.GetServices()

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, services)
}

func (ctr *Controller) GetDealershipServices(c *gin.Context) {
	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	dealershipID := uint(u64)

	services, err := ctr.Service.Manager.GetDealershipServices(dealershipID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, services)
}

func (ctr *Controller) GetService(c *gin.Context) {
	serviceIDString := c.Param("id")
	u64, err := strconv.ParseUint(serviceIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	serviceID := uint(u64)

	service, err := ctr.Service.Manager.GetServiceByID(serviceID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, service)
}

func (ctr *Controller) UpdateService(c *gin.Context) {
	id := c.Param("id")
	var serviceToUpdate model.Service
	err := c.BindJSON(&serviceToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	serviceToUpdate.ID = uint(u64)

	err = ctr.Service.Manager.UpdateService(&serviceToUpdate)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, &serviceToUpdate)
}

func (ctr *Controller) DeleteService(c *gin.Context) {
	serviceIDString := c.Param("id")
	u64, err := strconv.ParseUint(serviceIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	serviceID := uint(u64)

	err = ctr.Service.Manager.DeleteService(serviceID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, true)
}
