package controller

import (
	"fmt"
	"strconv"

	"gitlab.com/tradeincar/backend/api/model"
	"github.com/gin-gonic/gin"
)

func (ctr *Controller) CreateContactEntity(contactRequest *model.ContactRequest) (createContact model.Contact, err error) {
	fmt.Println(contactRequest)
	fmt.Println(contactRequest.Communication)
	// get Enum values
	contactCommunication, err := ctr.Service.Dict.GetCommunicationByName(contactRequest.Communication)
	if err != nil {
		return
	}

	// create model.Contact entity
	createContact = model.Contact{
		Communication:   contactCommunication,
		CommunicationID: contactCommunication.ID,
		Date:            contactRequest.Date,
		Description:     contactRequest.Description,
	}

	return
}

func (ctr *Controller) CreateContact(c *gin.Context) {
	dealIDString := c.Param("id")
	var ContactRequest model.ContactRequest
	err := c.Bind(&ContactRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	u64, err := strconv.ParseUint(dealIDString, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	dealID := uint(u64)

	createContact, err := ctr.CreateContactEntity(&ContactRequest)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	// add Contact to Deal in DB
	err = ctr.Service.Manager.AddContactToDeal(dealID, &createContact)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, &createContact)
}

func (ctr *Controller) GetContacts(c *gin.Context) {
	Contacts, err := ctr.Service.Manager.GetContacts()

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, Contacts)
}
