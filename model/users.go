package model

import (
	"time"

	"gorm.io/gorm"
)

func (User) TableName() string {
	return "users"
}

type UserRequest struct {
	LastName       string       `json:"lastName" binding:"required"`
	FirstName      string       `json:"firstName" binding:"required"`
	MiddleName     string       `json:"middleName" binding:"required"`
	Dealerships    []Dealership `json:"dealerships"`
	CanExportExcel bool         `json:"canExportExcel"`
	Telephone      string       `json:"telephone" binding:"required"`
	WorkPhone      string       `json:"workPhone"`
	Email          string       `json:"email" binding:"required"`
	RoleID         uint         `json:"roleID" binding:"required"`
	UserStatus     string       `json:"userStatus"`
}

type User struct {
	ID          uint         `json:"id" gorm:"primary_key"`
	LastName    string       `json:"lastName" binding:"required"`
	FirstName   string       `json:"firstName" binding:"required"`
	MiddleName  string       `json:"middleName" binding:"required"`
	Dealerships []Dealership `json:"dealerships" gorm:"many2many:dealership_users"`
	// transaction? #todo
	CanExportExcel bool       `json:"canExportExcel"`
	Telephone      string     `json:"telephone" binding:"required"`
	WorkPhone      string     `json:"workPhone"`
	Email          *string    `json:"email" gorm:"unique;" binding:"required"`
	PasswordHash   *string    `json:"-"`
	SessionToken   *string    `json:"-"`
	Role           Role       `json:"role"`
	RoleID         uint       `json:"roleID" gorm:"REFERENCES role(id)" binding:"required"`
	UserStatus     UserStatus `json:"userStatus"`
	UserStatusID   uint       `json:"statusID" gorm:"REFERENCES user_status(id)"`
	CreatedAt      *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt      *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
	Deleted        gorm.DeletedAt
}

func (UserStatus) TableName() string {
	return "user_status"
}

type UserStatus struct {
	ID          uint   `gorm:"primary_key" json:"id"`
	Name        string `gorm:"unique;not null" json:"name"`
	Description string `json:"description"`
}

type UserLoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
