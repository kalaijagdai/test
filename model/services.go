package model

import "time"

func (Service) TableName() string {
	return "service"
}

type Service struct {
	ID           uint        `gorm:"primary_key" json:"id"`
	Name         string      `json:"name" binding:"required"`
	Price        *float32    `json:"price"`
	Disabled     bool        `gorm:"default:false" json:"disabled"`
	DealershipID uint        `json:"dealershipID"`
	Dealership   *Dealership `json:"dealership"`
	CreatedAt    *time.Time  `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt    *time.Time  `json:"updatedAt" sql:"DEFAULT:'now()'"`
}
