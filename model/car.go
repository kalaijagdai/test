package model

import "time"

func (Car) TableName() string {
	return "car"
}

type CarIDsRequest struct {
	ID          uint   `json:"carID"`
	CarInterest string `json:"carInterest"`
}

// CarStatus enum
func (CarStatus) TableName() string {
	return "car_status"
}

type CarStatus struct {
	ID          uint   `gorm:"primaryKey" json:"id"`
	Name        string `gorm:"unique;not null" json:"name"`
	Description string `json:"description"`
}

type Car struct {
	ID                uint             `gorm:"primaryKey" json:"id"`
	VIN               string           `json:"vin"`
	Year              *time.Time       `json:"year"`
	CarStatus         *CarStatus       `json:"carStatus"`
	CarStatusID       uint             `json:"carStatusID" gorm:"REFERENCES car_status(id)"`
	CarModelID        uint             `json:"carModelID" gorm:"REFERENCES car_models(id)"`
	CarModel          *CarModel        `json:"carModel"`
	CarModificationID uint             `json:"сarModificationID" gorm:"REFERENCES сarModifications(id)"`
	CarModification   *CarModification `json:"сarModification"`
	// CarEquipmentID                uint                           `json:"carEquipmentID" gorm:"REFERENCES car_equipments(id)"`
	// CarEquipment                  CarEquipment                   `json:"carEquipment"`
	CarCustomCharacteristicValues []CarCustomCharacteristicValue `json:"carCustomCharacteristicValues"`

	DealCar *DealCar `json:"dealCar"`

	CreatedAt *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
}

type CarStatusRequest struct {
	CarStatusName string `json:"carStatusName"`
}

type CarType struct {
	ID            uint   `gorm:"primary_key" json:"id"`
	Name          string `json:"name"`
	IsFromBasebuy bool   `sql:"DEFAULT: false" json:"isFromBasebuy"`
}

type CarMark struct {
	ID            uint   `gorm:"primary_key" json:"id"`
	Name          string `json:"name"`
	NameRus       string `json:"nameRus"`
	CarTypeID     uint   `gorm:"REFERENCES car_type(id)" json:"carTypeID"`
	IsFromBasebuy bool   `sql:"DEFAULT: false" json:"isFromBasebuy"`

	Dealerships []Dealership `json:"dealerships" gorm:"many2many:dealership_marks"`
	CreatedAt   *time.Time   `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt   *time.Time   `json:"updatedAt" sql:"DEFAULT:'now()'"`
}

type CarModel struct {
	ID            uint     `gorm:"primary_key" json:"id"`
	Name          string   `json:"name"`
	NameRus       string   `json:"nameRus"`
	CarMarkID     uint     `gorm:"REFERENCES car_mark(id)" json:"carMarkID"`
	CarMark       *CarMark `json:"carMark"`
	IsFromBasebuy bool     `sql:"DEFAULT: false" json:"isFromBasebuy"`

	CarTypeID uint `gorm:"REFERENCES car_type(id)" json:"carTypeID"`
}

type CarGeneration struct {
	ID            uint       `gorm:"primary_key" json:"id"`
	Name          string     `json:"name"`
	CarModelID    uint       `gorm:"REFERENCES car_model(id)" json:"carModelID"`
	YearBegin     *time.Time `json:"yearBegin" sql:"DEFAULT:'now()'"`
	YearEnd       *time.Time `json:"yearEnd" sql:"DEFAULT:'now()'"`
	IsFromBasebuy bool       `sql:"DEFAULT: false" json:"isFromBasebuy"`

	CarSeries []CarSerie `json:"carSeries"`
	CarTypeID uint       `gorm:"REFERENCES car_type(id)" json:"carTypeID"`
}

type CarSerie struct {
	ID              uint           `gorm:"primary_key" json:"id"`
	Name            string         `json:"name"`
	CarGenerationID uint           `gorm:"REFERENCES car_generation(id)" json:"carGenerationID"`
	CarGeneration   *CarGeneration `json:"carGeneration"`
	IsFromBasebuy   bool           `sql:"DEFAULT: false" json:"isFromBasebuy"`

	CarModifications []CarModification `json:"carModifications"`
	CarModelID       uint              `gorm:"REFERENCES car_model(id)" json:"carModelID"`
	CarTypeID        uint              `gorm:"REFERENCES car_type(id)" json:"carTypeID"`
}

type CarModification struct {
	ID            uint      `gorm:"primary_key" json:"id"`
	Name          string    `json:"name"`
	CarSerieID    uint      `gorm:"REFERENCES car_serie(id)" json:"carSerieID"`
	CarSerie      *CarSerie `json:"carSerie"`
	IsFromBasebuy bool      `sql:"DEFAULT: false" json:"isFromBasebuy"`

	CarCharacteristicValues []CarCharacteristicValue `json:"carCharacteristicValues"`
	CarModelID              uint                     `gorm:"REFERENCES car_model(id)" json:"carModelID"`
	CarTypeID               uint                     `gorm:"REFERENCES car_type(id)" json:"carTypeID"`
}

type CarEquipment struct {
	ID                uint             `gorm:"primary_key" json:"id"`
	Name              string           `json:"name"`
	CarModificationID uint             `gorm:"REFERENCES car_modification(id)" json:"carModificationID"`
	CarModification   *CarModification `json:"carModification"`
	PriceMin          uint             `json:"priceMin"`
	Year              *time.Time       `json:"year" sql:"DEFAULT:'now()'"`
	IsFromBasebuy     bool             `sql:"DEFAULT: false" json:"isFromBasebuy"`

	CarTypeID uint `gorm:"REFERENCES car_type(id)" json:"carTypeID"`
}

type CarOption struct {
	ID            uint   `gorm:"primary_key" json:"id"`
	Name          string `json:"name"`
	ParentID      uint   `gorm:"REFERENCES car_option(id)" json:"parentnID"`
	IsFromBasebuy bool   `sql:"DEFAULT: false" json:"isFromBasebuy"`

	CarTypeID uint `gorm:"REFERENCES car_type(id)" json:"carTypeID"`
}

type CarOptionValue struct {
	ID             uint `gorm:"primary_key" json:"id"`
	IsBase         bool `json:"isBase"`
	CarOptionID    uint `gorm:"REFERENCES car_option(id)" json:"carOptionID"`
	CarEquipmentID uint `gorm:"REFERENCES car_equipment(id)" json:"carEquipmentID"`
	IsFromBasebuy  bool `sql:"DEFAULT: false" json:"isFromBasebuy"`

	CarTypeID uint `gorm:"REFERENCES car_type(id)" json:"carTypeID"`
}

type CarCharacteristic struct {
	ID            uint   `gorm:"primary_key" json:"id"`
	Name          string ` json:"name"`
	ParentID      uint   `gorm:"REFERENCES car_characteristic(id)" json:"parentnID"`
	IsFromBasebuy bool   `sql:"DEFAULT: false" json:"isFromBasebuy"`

	CarTypeID uint `gorm:"REFERENCES car_type(id)" json:"carTypeID"`
}

type CarCharacteristicValue struct {
	ID                  uint               `gorm:"primary_key" json:"id"`
	Key                 string             `gorm:"unique" json:"key"`
	Value               string             `json:"value"`
	Unit                string             `json:"unit"`
	CarCharacteristicID uint               `gorm:"REFERENCES car_characteristic(id)" json:"carCharacteristicID"`
	CarCharacteristic   *CarCharacteristic `json:"carCharacteristic"`
	CarModificationID   uint               `gorm:"REFERENCES car_modification(id)" json:"carModificationID"`
	IsFromBasebuy       bool               `sql:"DEFAULT: false" json:"isFromBasebuy"`

	CarTypeID uint `gorm:"REFERENCES car_type(id)" json:"carTypeID"`
}

type CarCustomCharacteristicValue struct {
	ID                       uint                    `gorm:"primary_key" json:"id"`
	Value                    string                  ` json:"value"`
	Unit                     string                  ` json:"unit"`
	CarID                    uint                    `gorm:"REFERENCES car(id)" json:"carID"`
	Car                      *Car                    `json:"car"`
	CarCharacteristicID      uint                    `gorm:"REFERENCES car_characteristic(id)" json:"carCharacteristicID"`
	CarCharacteristic        *CarCharacteristic      `json:"carCharacteristic"`
	CarCharacteristicValueID uint                    `gorm:"REFERENCES car_characteristic_value(id)" json:"carCharacteristicValueID"`
	CarCharacteristicValue   *CarCharacteristicValue `json:"carCharacteristicValue"`
}

func (CarInfo) TableName() string {
	return "car_info"
}

type CarInfo struct {
	ID                uint       `json:"ID" gorm:"primary_key"`
	PassportSeries    string     `json:"passportSeries"`
	PassportNumber    string     `json:"passportNumber"`
	PassportIssueDate *time.Time `json:"passportIssueDate"`
	NumberOfOwners    string     `json:"numberOfOwners"`
	Mileage           uint       `json:"mileage"`
	MileageType       string     `json:"mileageType"`
	CarCondition      string     `json:"carCondition"`
	CarBodyColor      string     `json:"carBodyColor"`
	Metalic           bool       `json:"metalic"`
	WarantyIssueDate  *time.Time `json:"warantyIssueDate"`
	FactoryGuarantee  bool       `json:"factoryGuarantee"`
	ChassisNumber     string     `json:"chassisNumber"`
}
