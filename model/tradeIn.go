package model

import "time"

func (TradeIn) TableName() string {
	return "trade_in"
}

type TradeIn struct {
	ID             uint       `gorm:"primaryKey" json:"id"`
	Price          *float32   `json:"price"`
	Discount       uint       `json:"discount"`
	Comment        string     `json:"comment"`
	AdditionalInfo string     `json:"additionalInfo"`
	AssessmentID   uint       `gorm:"REFERENCES assessment(id)" json:"assessmentID"`
	ClientID       uint       `gorm:"REFERENCES client(id)" json:"clientID"`
	Client         *Client    `json:"client"`
	UserID         uint       `gorm:"REFERENCES users(id)" json:"userID"`
	User           *User      `json:"user"`
	CarID          uint       `gorm:"REFERENCES car(id)" json:"carID"`
	Car            Car        `json:"car"`
	CarModelID     uint       `json:"carModelID" gorm:"REFERENCES car_models(id)"`
	CarModel       *CarModel  `json:"carModel"`
	SaleAt         *time.Time `json:"saleAt" sql:"DEFAULT:'now()'"`
	CreatedAt      *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt      *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
}
