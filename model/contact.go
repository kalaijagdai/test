package model

import "time"

func (Contact) TableName() string {
	return "contact"
}

type ContactRequest struct {
	Communication string     `json:"communication"`
	Date          *time.Time `json:"date" binding:"required"`
	Description   string     `json:"description"`
}

type Contact struct {
	ID              uint          `gorm:"primaryKey" json:"id"`
	Communication   Communication `json:"communication"`
	CommunicationID uint          `json:"communicationID" gorm:"REFERENCES communication(id)"`
	Date            *time.Time    `json:"date" binding:"required"`
	Description     string        `json:"description"`
	Deals           []Deal        `json:"deals" gorm:"many2many:deal_contact"`
	CreatedAt       *time.Time    `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt       *time.Time    `json:"updatedAt" sql:"DEFAULT:'now()'"`
}

// Communication enum
func (Communication) TableName() string {
	return "communication"
}

type Communication struct {
	ID          uint   `gorm:"primaryKey" json:"id"`
	Name        string `gorm:"unique;not null" json:"name"`
	Description string `json:"description"`
}
