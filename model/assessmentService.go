package model

import "time"

func (AssessmentService) TableName() string {
	return "assessment_service"
}

type AssessmentService struct {
	ID                 uint              `gorm:"primary_key" json:"id"`
	Name               string            `json:"name" binding:"required"`
	Price              *float32          `json:"price"`
	ByCompany          bool              `json:"byCompany"`
	Visible            bool              `json:"visible"`
	AssessmentID       uint              `json:"assessmentID"`
	AssessmentStatusID uint              `json:"assessmentStatusID" gorm:"REFERENCES assessment_status(id)"`
	AssessmentStatus   *AssessmentStatus `json:"assessmentStatus"`
	CreatedAt          *time.Time        `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt          *time.Time        `json:"updatedAt" sql:"DEFAULT:'now()'"`
}
