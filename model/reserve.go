package model

import "time"

func (Reserve) TableName() string {
	return "reserve"
}

type Reserve struct {
	ID           uint    `gorm:"primaryKey" json:"id"`
	AssessmentID uint    `gorm:"REFERENCES assessment(id)" json:"assessmentID"`
	ClientID     uint    `gorm:"REFERENCES client(id)" json:"clientID"`
	Client       *Client `json:"client"`
	UserID       uint    `gorm:"REFERENCES users(id)" json:"userID"`
	IsPaid       bool    `gorm:"default:false" json:"isPaid"`

	IsTestDrive           bool `gorm:"default:false" json:"isTestDrive"`
	IsCASCO               bool `gorm:"default:false" json:"isCASCO"`
	IsLeasing             bool `gorm:"default:false" json:"isLeasing"`
	IsCredit              bool `gorm:"default:false" json:"isCredit"`
	IsTradeIn             bool `gorm:"default:false" json:"isTradeIn"`
	IsAdditionalEquipment bool `gorm:"default:false" json:"isAdditionalEquipment"`
	IsTechnicalGuarantee  bool `gorm:"default:false" json:"isTechnicalGuarantee"`
	IsCarPurchaseOnly     bool `gorm:"default:false" json:"isCarPurchaseOnly"`

	CreatedAt *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
}
