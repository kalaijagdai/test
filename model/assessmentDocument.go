package model

import "time"

type AssessmentDocumentType struct {
	ID          uint   `gorm:"primaryKey" json:"id"`
	Name        string `gorm:"unique;not null" json:"name"`
	Description string `json:"description"`
}

type AssessmentDocument struct {
	ID                       uint                     `gorm:"primaryKey" json:"id"`
	Name                     string                   `json:"name"`
	AssessmentID             uint                     `gorm:"REFERENCES assessment(id)" json:"assessmentID"`
	AssessmentDocumentTypeID uint                     `gorm:"REFERENCES assessment_document_types(id)" json:"assessmentDocumentTypeID"`
	AssessmentDocumentType   *AssessmentDocumentType  `json:"assessmentDocumentType"`
	AssessmentDocumentFiles  []AssessmentDocumentFile `json:"assessmentDocumentFiles"`
	CreatedAt                *time.Time               `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt                *time.Time               `json:"updatedAt" sql:"DEFAULT:'now()'"`
}

type AssessmentDocumentFile struct {
	ID                   uint                `gorm:"primaryKey" json:"id"`
	URL                  string              `json:"url"`
	AssessmentDocumentID uint                `gorm:"REFERENCES assessment_documents(id)" json:"assessmentDocumentID"`
	AssessmentDocument   *AssessmentDocument `json:"assessmentDocument"`
	CreatedAt            *time.Time          `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt            *time.Time          `json:"updatedAt" sql:"DEFAULT:'now()'"`
}
