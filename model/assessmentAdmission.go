package model

import "time"

// AssessmentAdmission
func (AssessmentAdmission) TableName() string {
	return "assessment_admission"
}

type AssessmentAdmission struct {
	ID           uint `gorm:"primary_key" json:"id"`
	AssessmentID uint `json:"assessmentID" gorm:"REFERENCES assessment(id)"`

	Client   *Client `json:"client"` //dd
	ClientID uint    `json:"clientID" gorm:"REFERENCES client(id)"`

	SaleLink        string   `json:"saleLink"`
	AssessmentPrice *float32 `json:"assessmentPrice"`
	AdmissionPrice  *float32 `json:"admissionPrice"` //dd
	ContractNumber  string   `json:"contractNumber"` //dd

	CreatedAt *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
}
