package model

import (
	"time"

	"gorm.io/gorm"
)

// referral enum
func (Referral) TableName() string {
	return "referral"
}

type Referral struct {
	ID   uint   `gorm:"primaryKey" json:"id"`
	Name string `gorm:"unique;not null" json:"name"`
}

// gender enum
func (Gender) TableName() string {
	return "gender"
}

type Gender struct {
	ID          uint   `gorm:"primaryKey" json:"id"`
	Name        string `gorm:"unique;not null" json:"name"`
	Description string `json:"description"`
}

// clientType enum
func (ClientType) TableName() string {
	return "client_type"
}

type ClientType struct {
	ID          uint   `gorm:"primaryKey" json:"id"`
	Name        string `gorm:"unique;not null" json:"name"`
	Description string `json:"description"`
}

// client entity
func (Client) TableName() string {
	return "client"
}

type Client struct {
	ID                uint         `gorm:"primaryKey" json:"id"`
	Name              string       `json:"name"`
	Surname           string       `json:"surname"`
	Patronymic        string       `json:"patronymic"`
	Birthday          *time.Time   `json:"birthday"`
	IIN               string       `json:"iin"`
	Gender            Gender       `json:"gender"`
	GenderID          uint         `json:"genderID" gorm:"REFERENCES gender(id)"`
	User              *User        `json:"user"`
	UserID            uint         `json:"userID" gorm:"REFERENCES users(id)"`
	ClientType        ClientType   `json:"clientType" gorm:"default:1"` // add default type
	ClientTypeID      uint         `json:"clientTypeID" gorm:"REFERENCES client_type(id)"`
	Referral          Referral     `json:"referral"`
	ReferralID        uint         `json:"referralID" gorm:"REFERENCES referral(id)"`
	Telephone         string       `json:"telephone"`
	WorkPhone         string       `json:"workPhone"`
	Email             string       `json:"email"`
	PassportID        string       `json:"passportID"`
	PassportAuthority string       `json:"passportAuthority"`
	PassportIssueDate *time.Time   `json:"passportIssueDate"`
	Address           Address      `json:"address"`
	AddressID         uint         `json:"addressID" gorm:"REFERENCES address(id)"`
	Company           *Company     `json:"company"`
	CompanyID         uint         `json:"companyID" gorm:"REFERENCES company(id)"`
	Agreement         bool         `json:"agreement"`
	Deals             []Deal       `json:"deals"`
	Assessments       []Assessment `json:"assessments"`

	DealGoalID *uint     `json:"dealGoalID" gorm:"REFERENCES deal_goal(id)"`
	DealGoal   *DealGoal `json:"dealGoal"`

	CreatedAt *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
	Deleted   gorm.DeletedAt
}

type ClientInGetClients struct {
	ClientID               uint   `json:"id"`
	ClientName             string `json:"name"`
	ClientSurname          string `json:"surname"`
	Telephone              string `json:"telephone"`
	DealID                 string `json:"dealID"`
	User                   string `json:"user"`
	Car                    string `json:"car"`
	CarInterest            string `json:"carInterest"`
	CarInterestDescription string `json:"carInterestDescription"`
	Description            string `json:"description"`
	ContactID              string `json:"contactID"`
	Contact                string `json:"contact"`
}

//type ClientCreate struct {
//	ID				  uint     `json:"id"`
//	Name              string   `json:"name" binding:"required"`
//	Surname           string   `json:"surname"`
//	Patronymic        string   `json:"patronymic"`
//	Birthday          string   `json:"birthday"`
//	IIN               string   `json:"iin"`
//	Gender            string   `json:"gender"`
//	ClientType        string   `json:"clientType" binding:"required"`
//	Referral          string   `json:"referral" binding:"required"`
//	Telephone         string   `json:"telephone" binding:"required"`
//	WorkPhone         string   `json:"workPhone"`
//	Email             string   `json:"email"`
//	PassportID        string   `json:"passportID"`
//	PassportAuthority string   `json:"passportAuthority"`
//	PassportIssueDate string   `json:"passportIssueDate"`
//	Agreement         bool     `json:"agreement"`
//	Address           Address  `json:"address"`
//	Company           *Company `json:"company"`
//}
