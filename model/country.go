package model

import (
	"gorm.io/gorm"
)

// Country model
type Country struct {
	gorm.Model
	Name      string     `json:"name"`
	IsPopular bool       `json:"is_popular"`
	Regions   *[]Region  `json:"regions,omitempty"`
	Factories *[]Factory `json:"factories,omitempty"`
	Banks     *[]Bank    `json:"banks,omitempty"`
}
