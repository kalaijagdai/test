package model

func (Inspection) TableName() string {
	return "inspection"
}

func (InnerInspection) TableName() string {
	return "inner_inspection"
}

func (InnerInspectionView) TableName() string {
	return "inner_inspection_view"
}

func (InnerInspectionViewType) TableName() string {
	return "inner_inspection_view_type"
}

type Inspection struct {
	ID						uint					`gorm:"primaryKey" json:"id"`
	Name					string					`json:"name"`
	InnerInspections		[]InnerInspection		`json:"innerInspections"`
}

type InnerInspection struct {
	ID						uint					`gorm:"primaryKey" json:"id"`
	Name					string					`json:"name"`
	InspectionID			uint					`json:"inspectionID"`
	InnerInspectionViews	[]InnerInspectionView	`json:"innerInspectionViews"`
}

type InnerInspectionView struct {
	ID							uint					`gorm:"primaryKey" json:"id"`
	Name						string					`json:"name"`
	InnerInspectionID			uint					`json:"innerInspectionID"`
	Visible					    *bool					`json:"visible"`
}

type InnerInspectionViewType struct {
	ID						uint					`gorm:"primaryKey" json:"id"`
	Name					string					`json:"name" binding:"required"`
}

