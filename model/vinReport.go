package model

import (
	"time"
)

func (VinReportStatus) TableName() string {
	return "vin_report_status"
}

type VinReportStatus struct {
	ID   uint   `gorm:"primaryKey" json:"id"`
	Name string `gorm:"unique;not null" json:"name"`
}

func (VinReport) TableName() string {
	return "vin_report"
}

type VinReport struct {
	ID             uint             `gorm:"primaryKey" json:"id"`
	AssessmentID   uint             `gorm:"REFERENCES assessment(id)" json:"assessmentID"`
	TaskID         uint             `json:"taskId"`
	StatusID       uint             `gorm:"REFERENCES vin_report_status(id)" json:"statusID"`
	Status         *VinReportStatus `json:"status"`
	OwnershipCount uint             `gorm:"default:0" json:"ownershipCount"`
	HasRestrict    bool             `gorm:"default:false" json:"hasRestrict"`
	IsSearch       bool             `gorm:"default:false" json:"isSearch"`
	DtpCount       uint             `gorm:"default:0" json:"dtpCount"`
	Data           string           `json:"data"`
	// Data           json.RawMessage `json:"data"`
	CreatedAt *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
}

// type VinReportTask struct {
// 	ID      uint   `gorm:"primaryKey" json:"id"`
// 	TaskID  int    `json:"taskId"`
// 	Credate string `json:"credate"`
// 	Status  int    `json:"status"`
// }
