package model

import "time"

type Test struct {
	Time       *time.Time `json:"time,omitempty"`
	TestString string     `json:"jsonString"`
}
