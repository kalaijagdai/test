package model

import "time"

func (AssessmentStatusLog) TableName() string {
	return "assessment_status_log"
}

type AssessmentStatusLog struct {
	ID                    uint       `gorm:"primary_key" json:"id"`
	AssessmentID          uint       `json:"assessmentID"`
	SrcAssessmentStatusID uint       `json:"assessmentStatusID"`
	DstAssessmentStatusID uint       `json:"assessmentStatusID"`
	Reason                string     `json:"reason"`
	CreatedAt             *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt             *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
}
