package model

import "gorm.io/gorm"

// PaymentMethod struct
type PaymentMethod struct {
	gorm.Model
	Code string `json:"code" gorm:"uniqueIndex"`
	Name string `json:"name"`
}
