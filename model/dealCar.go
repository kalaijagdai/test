package model

import "time"

func (DealCar) TableName() string {
	return "deal_car"
}

type DealCar struct {
	DealID        int
	CarID         int
	CarInterest   CarInterest `json:"carInterest"`
	CarInterestID uint        `json:"carInterestID" gorm:"REFERENCES car_interest(id)"`

	Relevance   *Relevance `json:"relevance"`
	RelevanceID uint       `json:"relevanceID" gorm:"REFERENCES relevance(id)"`
	Comment     string     `json:"comment"`

	CreatedAt *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
}

// CarInterest enum
func (CarInterest) TableName() string {
	return "car_interest"
}

type CarInterest struct {
	ID          uint   `gorm:"primaryKey" json:"id"`
	Name        string `gorm:"unique;not null" json:"name"`
	Description string `json:"description"`
}
