package model

import (
	"encoding/json"
	"os"
)

type Configuration struct {
	DbUser         string
	DbPass         string
	DbName         string
	DbHost         string
	DbPort         int
	DbMode         string
	DbMaxOpenConns int
	DbMaxIdleConns int
	DbLogMode      bool
	GinReleaseMode bool
	HTTPPort       int
	DbMigrate      bool
	DbDataMigrate  bool

	ApiHost string

	AcceptTime        int
	DaysToPriceUpdate int

	DaysWithoutChangesOnSale uint
	DaysOnReserve            uint
	ReportURI                string
}

func (c *Configuration) ReadFile(path string) {
	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&c)
	if err != nil {
		panic(err)
	}
}

func NewConfiguration(path string) *Configuration {
	var configuration Configuration
	configuration.ReadFile(path)
	return &configuration
}
