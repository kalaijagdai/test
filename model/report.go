package model

import (
	"fmt"
	"time"
)

// Report model
type Report struct {
	ID             uint        `gorm:"primaryKey" json:"id"`
	CreatedAt      *time.Time  `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt      *time.Time  `json:"updatedAt" sql:"DEFAULT:'now()'"`
	UserID         uint        `gorm:"REFERENCES users(id)" json:"userID"`
	Sells          []Sell      `json:"sells"`
	Dealership     string      `json:"dealership"`
	DealershipID   uint        `json:"dealershipID" binding:"required" validate:"required" gorm:"references dealership(id)"`
	DealershipData *Dealership `json:"dealershipData" gorm:"foreignkey:dealership_id"`
	Status         *AkabStatus `json:"statusAkab" gorm:"foreignkey:status_id"`
	StatusID       int         `json:"statusID" gorm:"references akab_statuses(id)"`
}

// Column mapping model
type ColumnMap struct {
	ID                   uint       `gorm:"primaryKey" json:"id"`
	CreatedAt            *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt            *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
	UserID               uint       `gorm:"REFERENCES users(id)" json:"userId"`
	Vin                  string     `json:"vin" binding:"required" validate:"required"`
	Dealership           string     `json:"dealership" binding:"required" validate:"required"`
	CarMark              string     `json:"carMark" binding:"required" validate:"required"`
	CarModel             string     `json:"carModel" binding:"required" validate:"required"`
	YearOfIssue          string     `json:"yearOfIssue" binding:"required" validate:"required"`
	CarGeneration        string     `json:"carGeneration" binding:"required" validate:"required"`
	CarModification      string     `json:"carModification" binding:"required" validate:"required"`
	TypeOfFuel           string     `json:"typeOfFuel" binding:"required" validate:"required"`
	Transmission         string     `json:"transmission" binding:"required" validate:"required"`
	TypeOfDrive          string     `json:"typeOfDrive" binding:"required" validate:"required"`
	EngineVolume         string     `json:"engineVolume" binding:"required" validate:"required"`
	Color                string     `json:"color" binding:"required" validate:"required"`
	CountryOfManufacture string     `json:"countryOfManufacture" binding:"required" validate:"required"`
	Factory              string     `json:"factory" binding:"required" validate:"required"`
	TypeOfClient         string     `json:"typeOfClient" binding:"required" validate:"required"`
	PayForm              string     `json:"payForm" binding:"required" validate:"required"`
	Bank                 string     `json:"bank" binding:"required" validate:"required"`
	CountryName          string     `json:"countryName" binding:"required" validate:"required"`
	RegionOfSale         string     `json:"regionOfSale" binding:"required" validate:"required"`
	Price                string     `json:"price" binding:"required" validate:"required"`
	DateOfSale           string     `json:"dateOfSale" binding:"required" validate:"required"`
}

// QueryGetReports struct
type QueryGetReports struct {
	UserID       uint
	DealershipID uint   `form:"dealershipID"`
	Offset       int    `form:"offset"`
	Limit        int    `form:"limit"`
	FromDate     string `form:"fromDate"`
	ToDate       string `form:"toDate"`
	VinCode      string `form:"vinCode"`
	CarMark      string `form:"carMark"`
	CarModel     string `form:"carModel"`
}

type AkabExcelResponse struct {
	Result      *[]ExcelReadResponse  `json:"result"`
	ErrorHidden *[]AkabErrorHiddenLog `json:"errorHidden"`
}

// Report model
type ExcelReadResponse struct {
	ID         uint                     `gorm:"primaryKey" json:"id"`
	CreatedAt  *time.Time               `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt  *time.Time               `json:"updatedAt" sql:"DEFAULT:'now()'"`
	UserID     uint                     `gorm:"REFERENCES users(id)" json:"userID"`
	Sells      []ExcelReadResponseSells `json:"sells"`
	Dealership string                   `json:"dealership"`
	Error      string                   `json:"error"`
}

type ExcelReadResponseSells struct {
	ID                   uint       `gorm:"primaryKey" json:"id"`
	CreatedAt            *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt            *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
	ReportID             uint       `json:"reportID"`
	Vin                  string     `json:"vin"`
	Dealership           string     `json:"dealership"`
	CarMark              string     `json:"carMark"`
	CarModel             string     `json:"carModel"`
	YearOfIssue          string     `json:"yearOfIssue"`
	CarGeneration        string     `json:"carGeneration"`
	CarModification      string     `json:"carModification"`
	TypeOfFuel           string     `json:"typeOfFuel"`
	Transmission         string     `json:"transmission"`
	TypeOfDrive          string     `json:"typeOfDrive"`
	EngineVolume         string     `json:"engineVolume"`
	Color                string     `json:"color"`
	CountryOfManufacture string     `json:"countryOfManufacture"`
	Factory              string     `json:"factory"`
	TypeOfClient         string     `json:"typeOfClient"`
	PayForm              string     `json:"payform"`
	Bank                 string     `json:"bank"`
	//SellingDistance      string     `json:"sellingDistance"`
	CountryName  string `json:"countryName"`
	RegionOfSale string `json:"regionOfSale"`
	Price        string `json:"price"`
	PriceUSD     string `json:"priceUSD"`
	DateOfSale   string `json:"dateOfSale"`
}

type AkabErrorHiddenLog struct {
	Vin    string `json:"vin"`
	Column string `json:"column"`
	Value  string `json:"value"`
}

// QueryGetReportsInfoGraph model
type QueryGetReportsInfoGraph struct {
	Year  int `json:"year" form:"year"`
	Month int `json:"month" form:"month"`
}

func (query *QueryGetReportsInfoGraph) Format() string {
	if query.Month == 0 {
		return fmt.Sprintf("%d", query.Year)
	} else if query.Month < 10 {
		return fmt.Sprintf("%d-0%d", query.Year, query.Month)
	}
	return fmt.Sprintf("%d-%d", query.Year, query.Month)
}

func (query *QueryGetReportsInfoGraph) FormatLast() string {
	year := query.Year
	month := query.Month

	if query.Month == 0 {
		return fmt.Sprintf("%d", query.Year-1)
	} else if query.Month == 1 {
		year = year - 1
		month = 12
	} else {
		month--
	}

	if month < 10 {
		return fmt.Sprintf("%d-0%d", year, month)
	}
	return fmt.Sprintf("%d-%d", year, month)
}

// TopSells model
type TopSells struct {
	Mark       string  `json:"mark,omitempty"`
	Model      string  `json:"model,omitempty"`
	Amount     uint    `json:"amount,omitempty"`
	Ratio      float32 `json:"ratio,omitempty"`
	Difference uint    `json:"difference,omitempty"`
}
