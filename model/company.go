package model

import (
	"time"

	"gorm.io/gorm"
)

type Company struct {
	ID        uint       `gorm:"primaryKey" json:"id"`
	Name      string     `json:"name" binding:"required"`
	BIN       string     `json:"bin"`
	Address   Address    `json:"address"`
	AddressID uint       `json:"addressID" gorm:"REFERENCES address(id)"`
	CreatedAt *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
	Deleted   gorm.DeletedAt
}
