package model

import (
	"time"

	"gorm.io/gorm"
)

func (Dealership) TableName() string {
	return "dealership"
}

type Dealership struct {
	ID                 uint              `gorm:"primary_key" json:"id"`
	Name               string            `json:"name" binding:"required"`
	DealershipStatus   *DealershipStatus `json:"dealershipStatus" gorm:"foreignkey:dealership_status_id;references:id"`
	DealershipStatusID uint              `json:"dealershipStatusID" `
	Entity             string            `json:"entity" binding:"required"`
	Logo               string            `json:"logo"`
	Code               string            `json:"code" binding:"required"`
	Marks              []CarMark         `json:"marks" gorm:"many2many:dealership_marks"`
	Users              []User            `json:"users" gorm:"many2many:dealership_users"`
	Telephone          string            `json:"telephone" binding:"required"`
	Fax                string            `json:"fax"`
	Email              string            `json:"email" binding:"required"`
	Website            string            `json:"website"`
	ManagerAffirmation bool              `json:"managerAffirmation"`
	SaleTransfer       bool              `json:"saleTransfer"`
	AcceptTime         int               `json:"acceptTime"`
	DaysToPriceUpdate  int               `json:"daysToPriceUpdate"`
	AddressID          uint              `json:"addressID"`
	RegionID           uint              `json:"regionID"`
	Address            *Address          `json:"address"`
	Region             *Region           `json:"region"`
	Services           []Service         `json:"services"`
	Additions          []Addition        `json:"additions"`
	CreatedAt          *time.Time        `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt          *time.Time        `json:"updatedAt" sql:"DEFAULT:'now()'"`
	Deleted            gorm.DeletedAt
}

func (DealershipStatus) TableName() string {
	return "dealership_status"
}

type DealershipStatus struct {
	ID          uint       `gorm:"primary_key" json:"id"`
	Name        string     `gorm:"unique;not null" json:"name"`
	Description string     `json:"description"`
	CreatedAt   *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt   *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
}
