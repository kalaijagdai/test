package model

import "gorm.io/gorm"

// Region model
type Region struct {
	gorm.Model
	Name      string `json:"name"`
	CountryID uint   `json:"countryID"`
}
