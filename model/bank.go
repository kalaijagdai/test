package model

import (
	"gorm.io/gorm"
)

// Country model
type Bank struct {
	gorm.Model
	Name           string `json:"name"`
	IsPreferential bool   `json:"isPreferential"`
	CountryID      uint   `json:"countryID"`
}
