package model

import "time"

type AssessmentPhotoType struct {
	ID          uint   `gorm:"primaryKey" json:"id"`
	Name        string `gorm:"unique;not null" json:"name"`
	Description string `json:"description"`
}

type AssessmentPhoto struct {
	ID                    uint                 `gorm:"primaryKey" json:"id"`
	URL                   string               `json:"url"`
	AssessmentID          uint                 `gorm:"REFERENCES assessment(id)" json:"assessmentID"`
	AssessmentPhotoTypeID uint                 `gorm:"REFERENCES assessment_photo_types(id)" json:"assessmentPhotoTypeID"`
	AssessmentPhotoType   *AssessmentPhotoType `json:"assessmentPhotoType"`
	IsFavorite            bool                 `gorm:"default:false" json:"isFavorite"`
	CreatedAt             *time.Time           `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt             *time.Time           `json:"updatedAt" sql:"DEFAULT:'now()'"`
}
