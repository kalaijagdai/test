package model

import (
	"time"
)

// client entity
func (Deal) TableName() string {
	return "deal"
}

type Deal struct {
	ID uint `gorm:"primaryKey" json:"id"`

	Client   *Client `json:"client"`
	ClientID uint    `json:"clientID" gorm:"REFERENCES client(id)"`

	Dealership   *Dealership `json:"dealership"`
	DealershipID uint    `json:"dealershipID" gorm:"REFERENCES dealership(id)"`

	Cars []Car `json:"cars" gorm:"many2many:deal_car"`

	User   User `json:"user"`
	UserID uint `json:"userID" gorm:"REFERENCES user(id)"`

	Contacts []Contact `json:"contacts" gorm:"many2many:deal_contact"`

	// information
	Description string `json:"description" binding:"required"`

	DealGoal   DealGoal `json:"dealGoal"`
	DealGoalID uint     `json:"dealGoalID" gorm:"REFERENCES deal_goal(id)"`

	DealStatus   DealStatus `json:"dealStatus"`
	DealStatusID uint       `json:"dealStatusID" gorm:"REFERENCES deal_status(id)"`

	CommunicationID uint          `json:"communicationID" gorm:"REFERENCES communication(id)"`
	Communication   Communication `json:"communication"`

	CreatedAt *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
}

// DealGoal enum
func (DealGoal) TableName() string {
	return "deal_goal"
}

type DealGoal struct {
	ID          uint   `gorm:"primaryKey" json:"id"`
	Name        string `gorm:"unique;not null" json:"name"`
	Description string `json:"description"`
}

// DealStatus enum
func (DealStatus) TableName() string {
	return "deal_status"
}

type DealStatusRequest struct {
	Reason string `json:"reason"`
}

type DealStatus struct {
	ID          uint   `gorm:"primaryKey" json:"id"`
	Name        string `gorm:"unique;not null" json:"name"`
	Description string `json:"description"`
}

// Relevance enum
func (Relevance) TableName() string {
	return "relevance"
}

type Relevance struct {
	ID          uint   `gorm:"primaryKey" json:"id"`
	Name        string `gorm:"unique;not null" json:"name"`
	Description string `json:"description"`
}
