package model

import "time"

type AssessmentType struct {
	ID          uint   `gorm:"primaryKey" json:"id"`
	Name        string `gorm:"unique;not null" json:"name"`
	Description string `json:"description"`
}

// assessment
func (Assessment) TableName() string {
	return "assessment"
}

type Assessment struct {
	ID                 uint              `gorm:"primaryKey" json:"id"`
	Dealership         *Dealership       `json:"dealership"`
	DealershipID       uint              `json:"dealershipID"`
	Car                *Car              `json:"car"`
	CarID              uint              `json:"carID" gorm:"REFERENCES car(id)"`
	CarInfo            *CarInfo          `json:"carInfo"`
	CarInfoID          uint              `json:"carInfoID" gorm:"REFERENCES car_info(id)"`
	Client             *Client           `json:"client"`
	ClientID           uint              `json:"clientID" gorm:"REFERENCES client(id)"`
	User               *User             `json:"user"`
	UserID             uint              `json:"userID" gorm:"REFERENCES users(id)"`
	AssessmentTypeID   uint              `json:"assessmentTypeID" gorm:"REFERENCES assessment_types(id)"`
	AssessmentType     *AssessmentType   `json:"assessmentType"`
	AssessmentStatus   *AssessmentStatus `json:"assessmentStatus"`
	AssessmentStatusID uint              `json:"assessmentStatusID" gorm:"REFERENCES assessment_status(id)"`

	AssessmentGear      *AssessmentGear      `json:"assessmentGear"`
	AssessmentAdmission *AssessmentAdmission `json:"assessmentAdmission"`

	AssessmentReport []AssessmentReport `json:"assessmentReport"`

	AssessmentServices  []AssessmentService  `json:"assessmentServices"`
	AssessmentAdditions []AssessmentAddition `json:"assessmentAdditions"`

	AssessmentPricing        []AssessmentPricing        `json:"assessmentPricing"`
	AssessmentPricingHistory []AssessmentPricingHistory `json:"assessmentPricingHistory"`

	AssessmentPhotos    []AssessmentPhoto    `json:"assessmentPhotos"`
	AssessmentDocuments []AssessmentDocument `json:"assessmentDocuments"`

	TradeIn  *TradeIn  `json:"tradeIn"`
	Sale     *Sale     `json:"sale"`
	Reserves []Reserve `json:"reserves"`

	MaxMarketValue *float32 `json:"maxMarketValue"`
	MinMarketValue *float32 `json:"minMarketValue"`
	SaleLink       string   `json:"saleLink"`

	VinReport []VinReport `json:"vinReport"`

	CreatedAt *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
}

// AssessmentStage enum
func (AssessmentStage) TableName() string {
	return "assessment_stage"
}

type AssessmentStage struct {
	ID          uint   `gorm:"primaryKey" json:"id"`
	Name        string `gorm:"unique;not null"`
	Description string
}

// AssessmentStatus enum
func (AssessmentStatus) TableName() string {
	return "assessment_status"
}

type AssessmentStatus struct {
	ID                uint            `gorm:"primaryKey" json:"id"`
	Name              string          `gorm:"unique;not null" json:"name"`
	Description       string          `json:"description"`
	AssessmentStageID uint            `json:"assessmentStageID" gorm:"REFERENCES assessment_stage(id)"`
	AssessmentStage   AssessmentStage `json:"assessmentStage"`
}

func (AssessmentPricing) TableName() string {
	return "assessment_pricing"
}

type AssessmentPricing struct {
	ID                  uint              `gorm:"primaryKey" json:"id"`
	AssessmentPrice     *float32          `json:"assessmentPrice"`
	PlannedSellingPrice *float32          `json:"plannedSellingPrice"`
	Margin              *float32          `json:"margin"`
	MarginPercent       *float32          `json:"marginPercent"`
	AssessmentID        uint              `json:"assessmentID"`
	AssessmentStatusID  uint              `json:"assessmentStatusID"`
	AssessmentStatus    *AssessmentStatus `json:"assessmentStatus"`
	CreatedAt           *time.Time        `json:"createdAt" sql:"DEFAULT:'now()'"`
}

func (AssessmentPricingHistory) TableName() string {
	return "assessment_pricing_history"
}

type AssessmentPricingHistory struct {
	AssessmentPrice     *float32          `json:"assessmentPrice"`
	PlannedSellingPrice *float32          `json:"historyPrice"`
	AssessmentStatusID  uint              `json:"assessmentStatusID"`
	AssessmentStatus    *AssessmentStatus `json:"assessmentStatus"`
	AssessmentID        uint              `json:"assessmentID"`
	CreatedAt           *time.Time        `json:"createdAt" sql:"DEFAULT:'now()'"`
}

//{
//"assessmentPrice": 1466667,
//"plannedSellingPrice": 2000000
//}
