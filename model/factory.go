package model

import "gorm.io/gorm"

// Factory model
type Factory struct {
	gorm.Model
	Name      string `json:"name"`
	CountryID uint   `json:"countryID"`
}
