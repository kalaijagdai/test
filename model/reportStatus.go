package model

func (AkabStatus) TableName() string {
	return "akab_statuses"
}

type AkabStatus struct {
	ID          int    `gorm:"primaryKey" json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}
