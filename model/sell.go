package model

import (
	"time"

	"gorm.io/gorm"
)

// Sell model
type Sell struct {
	ID                   uint       `gorm:"primaryKey" json:"id"`
	CreatedAt            *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt            *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
	ReportID             uint       `json:"reportID"`
	Company              string     `json:"сompany"`
	Seller               string     `json:"seller"`
	Vin                  string     `json:"vin" binding:"required" validate:"required"`
	Dealership           string     `json:"dealership" binding:"required" validate:"required"`
	DealershipID         uint       `json:"dealershipID" binding:"required" validate:"required"`
	CarMark              string     `json:"carMark" binding:"required" validate:"required"`
	CarModel             string     `json:"carModel" binding:"required" validate:"required"`
	YearOfIssue          uint       `json:"yearOfIssue" binding:"required" validate:"required"`
	CarGeneration        string     `json:"carGeneration" binding:"required" validate:"required"`
	CarModification      string     `json:"carModification" binding:"required" validate:"required"`
	TypeOfFuel           string     `json:"typeOfFuel" binding:"required" validate:"required"`
	Transmission         string     `json:"transmission" binding:"required" validate:"required"`
	TypeOfDrive          string     `json:"typeOfDrive" binding:"required" validate:"required"`
	EngineVolume         string     `json:"engineVolume" binding:"required" validate:"required"`
	Color                string     `json:"color" binding:"required" validate:"required"`
	CountryOfManufacture string     `json:"countryOfManufacture" binding:"required" validate:"required"`
	Factory              string     `json:"factory"`
	TypeOfClient         string     `json:"typeOfClient" binding:"required" validate:"required"`
	PayForm              string     `json:"payForm" binding:"required" validate:"required"`
	Segment              string     `json:"segment"`
	Bank                 string     `json:"bank"`
	SellingDistance      string     `json:"sellingDistance"`
	CountryID            uint       `json:"countryID,omitempty"`
	CountryName          string     `json:"countryName,omitempty"`
	RegionOfSale         string     `json:"regionOfSale" binding:"required" validate:"required"`
	Price                uint       `json:"price" binding:"required" validate:"required"`
	PriceUSD             uint       `json:"priceUSD" `
	DateOfSale           string     `json:"dateOfSale" binding:"required" validate:"required"`
	IsUpdated            bool       `json:"isUpdated"`
	Deleted              gorm.DeletedAt
}

type SellsUpdateHistory struct {
	ID        uint       `gorm:"primaryKey" json:"id"`
	CreatedAt *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UserID    uint       `json:"userID"`
	Sell      Sell       `gorm:"embedded;embeddedPrefix:sell_"`
}
