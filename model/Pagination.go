package model

// Pagination struct
type Pagination struct {
	SearchText string `form:"searchText,omitempty"`
	Offset     int    `form:"offset,omitempty"`
	Limit      int    `form:"limit,omitempty"`
}
