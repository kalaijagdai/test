package model

import "time"

func (Address) TableName() string {
	return "address"
}

type Address struct {
	ID          uint       `gorm:"primary_key" json:"id"`
	Country     string     `json:"country"`
	Region      string     `json:"region"`
	City        string     `json:"city"`
	Description string     `json:"description"`
	PostIndex   string     `json:"postIndex"`
	CreatedAt   *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt   *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
}
