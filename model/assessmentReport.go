package model

import "time"

func (AssessmentReport) TableName() string {
	return "assessment_report"
}

type AssessmentReport struct {
	ID                   uint                   `gorm:"primaryKey" json:"id"`
	Name                 string                 `json:"name"`
	AssessmentID         uint                   `json:"assessmentID"`
	AssessmentStatusID   uint                   `json:"assessmentStatusID" gorm:"REFERENCES assessment_status(id)"`
	AssessmentStatus     AssessmentStatus       `json:"assessmentStatus"`
	User                 User                   `json:"user"`
	UserID               uint                   `json:"userID"  gorm:"REFERENCES user(id)" binding:"required"`
	Notes                string                 `json:"notes"`
	File                 string                 `json:"file"`
	AssessmentInspection []AssessmentInspection `json:"inspections"`
	CreatedAt            *time.Time             `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt            *time.Time             `json:"updatedAt" sql:"DEFAULT:'now()'"`
}

type AssessmentInspection struct {
	ID                        uint                        `gorm:"primaryKey" json:"id"`
	Name                      string                      `json:"name"`
	AssessmentInnerInspection []AssessmentInnerInspection `json:"innerInspections"`
	AssessmentReportID        uint                        `json:"assessmentReportID"`
}

type AssessmentInnerInspection struct {
	ID                            uint                            `gorm:"primaryKey" json:"id"`
	Name                          string                          `json:"name"`
	AssessmentInspectionID        uint                            `json:"inspectionID"`
	AssessmentInnerInspectionView []AssessmentInnerInspectionView `json:"innerInspectionViews"`
}

type AssessmentInnerInspectionView struct {
	ID                          uint                    `gorm:"primaryKey" json:"id"`
	Name                        string                  `json:"name"`
	AssessmentInnerInspectionID uint                    `json:"innerInspectionID"`
	Notes                       string                  `json:"notes"`
	File                        string                  `json:"file"`
	Value						string                  `json:"value"`
}

func (AssessmentInspection) TableName() string {
	return "assessment_inspection"
}

func (AssessmentInnerInspection) TableName() string {
	return "assessment_inner_inspection"
}

func (AssessmentInnerInspectionView) TableName() string {
	return "assessment_inner_inspection_view"
}
