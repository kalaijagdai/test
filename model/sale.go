package model

import "time"

func (Sale) TableName() string {
	return "sale"
}

type Sale struct {
	ID             uint          `gorm:"primaryKey" json:"id"`
	Price          *float32      `json:"price"`
	ContractNumber string        `json:"contractNumber"`
	AssessmentID   uint          `gorm:"REFERENCES assessment(id)" json:"assessmentID"`
	ClientID       uint          `gorm:"REFERENCES client(id)" json:"clientID"`
	Client         *Client       `json:"client"`
	UserID         uint          `gorm:"REFERENCES users(id)" json:"userID"`
	User           *User         `json:"user"`
	IsCASCO        *bool         `gorm:"default:false" json:"isCASCO"`
	IsCredit       *bool         `gorm:"default:false" json:"isCredit"`
	SalesChannelID uint          `gorm:"REFERENCES sales_channel(id)" json:"salesChannelID"`
	SalesChannel   *SalesChannel `json:"salesChannel"`
	SaleAt         *time.Time    `json:"saleAt" sql:"DEFAULT:'now()'"`
	IssueAt        *time.Time    `json:"issueAt" sql:"DEFAULT:'now()'"`
	CreatedAt      *time.Time    `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt      *time.Time    `json:"updatedAt" sql:"DEFAULT:'now()'"`
}

func (SalesChannel) TableName() string {
	return "sales_channel"
}

type SalesChannel struct {
	ID          uint   `gorm:"primaryKey" json:"id"`
	Name        string `gorm:"unique;not null" json:"name"`
	Description string `json:"description"`
}
