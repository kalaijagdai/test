package model

import "time"

// AssessmentGear
func (AssessmentGear) TableName() string {
	return "assessment_gear"
}

type AssessmentGear struct {
	ID uint `gorm:"primary_key" json:"id"`

	AssessmentID uint `json:"assessmentID" gorm:"REFERENCES assessment(id)"`

	// general
	Manual      bool `json:"manual" gorm:"default:false"`
	ServiceBook bool `json:"serviceBook" gorm:"default:false"`
	Triangle    bool `json:"triangle" gorm:"default:false"`

	FirstAidKit  bool `json:"firstAidKit" gorm:"default:false"`
	Extinguisher bool `json:"extinguisher" gorm:"default:false"`
	WheelWrench  bool `json:"wheelWrench" gorm:"default:false"`

	SecretBoltWrench bool `json:"secretBoltWrench" gorm:"default:false"`
	Jack             bool `json:"jack" gorm:"default:false"`
	Compressor       bool `json:"compressor" gorm:"default:false"`
	NumOfKeys        uint `json:"numOfKeys"`

	// ExtraWheel
	ExtraWheel string `json:"extraWheel"`

	// WheelSet
	WheelSet        string `json:"wheelSet"`
	WheelSetComment string `json:"wheelSetComment"`

	// MatSet
	MatSetGear    MatSetGear `json:"matSetGear"`
	MatSetComment string     `json:"matSetComment"`

	// Security
	SecurityGear SecurityGear `json:"securityGear"`

	// Parktronic
	ParktronicGear ParktronicGear `json:"parktronicGear"`

	// Airbag
	AirbagGear AirbagGear `json:"airbagGear"`

	// Assistive systems
	AssistiveSystemGear AssistiveSystemGear `json:"assistiveSystemGear"`

	// Salon
	SalonGear SalonGear `json:"salonGear"`

	PowerWindows       string `json:"powerWindows"`
	InteriorUpholstery string `json:"interiorUpholstery"`
	InteriorColor      string `json:"interiorColor"`

	// Heated seats
	HeatedSeatGear HeatedSeatGear `json:"heatedSeatGear"`

	// SeatVentilation
	SeatVentilationGear SeatVentilationGear `json:"seatVentilationGear"`

	DriverSeat         string `json:"driverSeat"`
	FrontPassengerSeat string `json:"frontPassengerSeat"`
	RearSeat           string `json:"rearSeat"`

	// Exterior
	ExteriorGear ExteriorGear `json:"exteriorGear"`

	// Disks
	LightAlloyDisks bool `json:"lightAlloyDisks" gorm:"default:false"`
	DiskSize        uint `json:"diskSize"`

	// Review
	ReviewGear ReviewGear `json:"reviewGear"`

	// HeadlightType
	HeadLightType string `json:"headLightType"`

	// Exterior mirrors
	MirrorGear MirrorGear `json:"mirrorGear"`

	// Electric heaters
	ElectricHeaterGear ElectricHeaterGear `json:"electricHeaterGear"`

	// comfort
	ComfortGear ComfortGear `json:"comfortGear"`

	// Climate
	Climate string `json:"climate"`

	// Steering wheel
	SteeringWheelGear SteeringWheelGear `json:"steeringWheelGear"`

	// Steering wheel adjustment
	SteeringWheelAdjustment string `json:"steeringWheelAdjustment"`

	// Multimedia
	MultimediaGear MultimediaGear `json:"multimediaGear"`

	// Radio
	RadioGear RadioGear `json:"radioGear"`

	// Theft protection
	TheftProtectionGear TheftProtectionGear `json:"theftProtectionGear"`

	// Alarm
	Alarm     string `json:"alarm"`
	AlarmName string `json:"alarmName"`

	CreatedAt *time.Time `json:"createdAt" sql:"DEFAULT:'now()'"`
	UpdatedAt *time.Time `json:"updatedAt" sql:"DEFAULT:'now()'"`
}

// SecurityGear
func (SecurityGear) TableName() string {
	return "security_gear"
}

type SecurityGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	ABS                   bool `json:"abs" gorm:"default:false"`
	RearDoorLock          bool `json:"rearDoorLock" gorm:"default:false"`
	ESP                   bool `json:"esp" gorm:"default:false"`
	Isofix                bool `json:"isofix" gorm:"default:false"`
	TractionControlSystem bool `json:"tractionControlSystem" gorm:"default:false"`
	DriverFatigueSensor   bool `json:"driverFatigueSensor" gorm:"default:false"`
}

// ParktronicGear
func (ParktronicGear) TableName() string {
	return "parktronic_gear"
}

type ParktronicGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	RearParktronic  bool `json:"rearParktronic" gorm:"default:false"`
	FrontParktronic bool `json:"frontParktronic" gorm:"default:false"`
}

// AirbagGear
func (AirbagGear) TableName() string {
	return "airbag_gear"
}

type AirbagGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	Driver         bool `json:"driver" gorm:"default:false"`
	DriverKnees    bool `json:"driverKnees" gorm:"default:false"`
	Passenger      bool `json:"passenger" gorm:"default:false"`
	Side           bool `json:"side" gorm:"default:false"`
	SideRear       bool `json:"sideRear" gorm:"default:false"`
	SafetyShutters bool `json:"safetyShutters" gorm:"default:false"`
}

// AssistiveSystemGear
func (AssistiveSystemGear) TableName() string {
	return "assistive_system_gear"
}

type AssistiveSystemGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	LaneControl        bool `json:"laneControl" gorm:"default:false"`
	BlindSpotControl   bool `json:"blindSpotControl" gorm:"default:false"`
	NightVision        bool `json:"nightVision" gorm:"default:false"`
	HillStartHelper    bool `json:"hillStartHelper" gorm:"default:false"`
	CollisionAvoidance bool `json:"collisionAvoidance" gorm:"default:false"`
	SignRecognition    bool `json:"signRecognition" gorm:"default:false"`
}

// SalonGear
func (SalonGear) TableName() string {
	return "salon_gear"
}

type SalonGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	FrontCenterArmrest    bool `json:"frontCenterArmrest" gorm:"default:false"`
	LeatherGearshiftLever bool `json:"leatherGearshiftLever" gorm:"default:false"`
	Luke                  bool `json:"luke" gorm:"default:false"`
	ThirdRowOfSeats       bool `json:"thirdRowOfSeats" gorm:"default:false"`
	FoldingPassengerSeat  bool `json:"foldingPassengerSeat" gorm:"default:false"`
	FoldingRearSeat       bool `json:"foldingRearSeat" gorm:"default:false"`
	BlackCeilingTrim      bool `json:"blackCeilingTrim" gorm:"default:false"`
	DoorSills             bool `json:"doorSills" gorm:"default:false"`
	PanoramicViewRoof     bool `json:"panoramicViewRoof" gorm:"default:false"`
	AluminumPedalPads     bool `json:"aluminumPedalPads" gorm:"default:false"`
	SportsSeats           bool `json:"sportsSeats" gorm:"default:false"`
	FoldingRearTable      bool `json:"foldingRearTable" gorm:"default:false"`
}

// HeatedSeatGear
func (HeatedSeatGear) TableName() string {
	return "heated_seat_gear"
}

type HeatedSeatGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	Front bool `json:"front" gorm:"default:false"`
	Rear  bool `json:"rear" gorm:"default:false"`
}

// SeatVentilationGear
func (SeatVentilationGear) TableName() string {
	return "seat_ventilation_gear"
}

type SeatVentilationGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	Front bool `json:"front" gorm:"default:false"`
	Rear  bool `json:"rear" gorm:"default:false"`
}

// ExteriorGear
func (ExteriorGear) TableName() string {
	return "exterior_gear"
}

type ExteriorGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	Film        bool `json:"film" gorm:"default:false"`
	TowBar      bool `json:"towBar" gorm:"default:false"`
	Airbrushing bool `json:"airbrushing" gorm:"default:false"`
	RoofRails   bool `json:"roofRails" gorm:"default:false"`
}

// ReviewGear
func (ReviewGear) TableName() string {
	return "review_gear"
}

type ReviewGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	FogLights         bool `json:"fogLights" gorm:"default:false"`
	HeadLightWasher   bool `json:"headLightWasher" gorm:"default:false"`
	LightSensor       bool `json:"lightSensor" gorm:"default:false"`
	RainSensor        bool `json:"rainSensor" gorm:"default:false"`
	AutoCorrector     bool `json:"autoCorrector" gorm:"default:false"`
	AdaptiveLight     bool `json:"adaptiveLight" gorm:"default:false"`
	AutomaticHighBeam bool `json:"automaticHighBeam" gorm:"default:false"`
}

// MirrorGear
func (MirrorGear) TableName() string {
	return "mirror_gear"
}

type MirrorGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	Heating         bool `json:"heating" gorm:"default:false"`
	ElectricDrive   bool `json:"electricDrive" gorm:"default:false"`
	ElectricFolding bool `json:"electricFolding" gorm:"default:false"`
}

// ElectricHeaterGear
func (ElectricHeaterGear) TableName() string {
	return "electric_heater_gear"
}

type ElectricHeaterGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	WiperZones    bool `json:"wiperZones" gorm:"default:false"`
	Windshield    bool `json:"windshield" gorm:"default:false"`
	WasherNozzles bool `json:"washerNozzles" gorm:"default:false"`
}

// ComfortGear
func (ComfortGear) TableName() string {
	return "comfort_gear"
}

type ComfortGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	Toning                  bool `json:"toning" gorm:"default:false"`
	SmokerPackage           bool `json:"smokerPackage" gorm:"default:false"`
	RemoteEngineStart       bool `json:"remoteEngineStart" gorm:"default:false"`
	PowerSteering           bool `json:"powerSteering" gorm:"default:false"`
	ElectricSteering        bool `json:"electricSteering" gorm:"default:false"`
	RearViewCamera          bool `json:"rearViewCamera" gorm:"default:false"`
	Camera360               bool `json:"camera360" gorm:"default:false"`
	CruiseСontrol           bool `json:"cruiseСontrol" gorm:"default:false"`
	AdaptiveСruiseСontrol   bool `json:"adaptiveСruiseСontrol" gorm:"default:false"`
	TirePressureSensor      bool `json:"tirePressureSensor" gorm:"default:false"`
	ElectricBootLid         bool `json:"electricBootLid" gorm:"default:false"`
	KeylessAccess           bool `json:"keylessAccess" gorm:"default:false"`
	ButtonStartEngine       bool `json:"buttonStartEngine" gorm:"default:false"`
	CooledGloveBox          bool `json:"cooledGloveBox" gorm:"default:false"`
	AutomaticParking        bool `json:"automaticParking" gorm:"default:false"`
	WindshieldProjection    bool `json:"windshieldProjection" gorm:"default:false"`
	PrestartingHeater       bool `json:"prestartingHeater" gorm:"default:false"`
	StartStop               bool `json:"startStop" gorm:"default:false"`
	ActivePowerSteering     bool `json:"activePowerSteering" gorm:"default:false"`
	RearWindowCurtain       bool `json:"rearWindowCurtain" gorm:"default:false"`
	RearDoorCurtain         bool `json:"rearDoorCurtain" gorm:"default:false"`
	AdjustablePedalAssembly bool `json:"adjustablePedalAssembly" gorm:"default:false"`
}

// SteeringWheelGear
func (SteeringWheelGear) TableName() string {
	return "steering_wheel_gear"
}

type SteeringWheelGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	Multifunctional bool `json:"multifunctional" gorm:"default:false"`
	Heating         bool `json:"heating" gorm:"default:false"`
	Leather         bool `json:"leather" gorm:"default:false"`
	PaddleShifters  bool `json:"paddleShifters" gorm:"default:false"`
}

// MultimediaGear
func (MultimediaGear) TableName() string {
	return "multimedia_gear"
}

type MultimediaGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	VoiceControl            bool `json:"voiceControl" gorm:"default:false"`
	RearPassengerMultimedia bool `json:"rearPassengerMultimedia" gorm:"default:false"`
}

// RadioGear
func (RadioGear) TableName() string {
	return "radio_gear"
}

type RadioGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	Established      bool `json:"established" gorm:"default:false"`
	CdDvd            bool `json:"cdDvd" gorm:"default:false"`
	AUX              bool `json:"aux" gorm:"default:false"`
	Bluetooth        bool `json:"bluetooth" gorm:"default:false"`
	USB              bool `json:"usb" gorm:"default:false"`
	TV               bool `json:"tv" gorm:"default:false"`
	NavigationSystem bool `json:"navigationSystem" gorm:"default:false"`
}

// TheftProtectionGear
func (TheftProtectionGear) TableName() string {
	return "theft_protection_gear"
}

type TheftProtectionGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	CentralLocking         bool `json:"centralLocking" gorm:"default:false"`
	VolumeSensor           bool `json:"volumeSensor" gorm:"default:false"`
	NonstandardImmobilizer bool `json:"nonstandardImmobilizer" gorm:"default:false"`
	MechanicalAgent        bool `json:"mechanicalAgent" gorm:"default:false"`
}

// MatSetGear
func (MatSetGear) TableName() string {
	return "mat_set_gear"
}

type MatSetGear struct {
	ID               uint `gorm:"primary_key" json:"id"`
	AssessmentGearID uint `json:"assessmentGearID" gorm:"REFERENCES assessment_gearID(id)"`

	Cloth  bool `json:"cloth" gorm:"default:false"`
	Rubber bool `json:"rubber" gorm:"default:false"`
}
