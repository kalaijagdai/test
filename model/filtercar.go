package model

import (
	"time"

	"gorm.io/gorm"
)

func (FilterCarByUser) TableName() string {
	return "FilterCarByUser"
}

type FilterCar struct {
	ID            uint     `json:"id,omitempty"`
	Model         string   `json:"model" binding:"required"`
	Mark          string   `json:"mark" binding:"required"`
	YearOut       []string `json:"yearOut" binding:"required"`
	TypeOfEngine  string   `json:"typeOfEngine" binding:"required"`
	KPP           string   `json:"KPP" binding:"required"`
	DriveUnit     string   `json:"driveUnit" binding:"required"`
	TypeOfCarcase string   `json:"typeOfCarcase" binding:"required"`
	Color         string   `json:"color" binding:"required"`
	SteeringWheel string   `json:"steeringWheel" binding:"required"`
	Mileage       []string `json:"mileage" binding:"required"`
	Capacity      []string `json:"capacity" binding:"required"`
	Price         []string `json:"price" binding:"required"`
	CheaperMarket []string `json:"cheaperMarket" binding:"required"`
	DaysOnSale    []string `json:"daysOnSale" binding:"required"`
	Region        string   `json:"region" binding:"required"`
	Sites         string   `json:"sites" binding:"required"`
	AmountOfAds   string   `json:"amountOfAds" binding:"required"`
	WithPhoto     bool     `json:"withPhoto"`
	CustomCleared bool     `json:"customCleared"`
	Emergency     bool     `json:"emergency"`
}

type FilterCarByUser struct {
	gorm.Model
	ID                uint   `gorm:"primaryKey"`
	UserID            uint   `gorm:"REFERENCES users(id)"`
	Name              string `gorm:"name"`
	CarModel          string
	Mark              string
	FromYearOut       time.Time
	ToYearOut         time.Time
	TypeOfEngine      string
	KPP               string
	DriveUnit         string
	TypeOfCarcase     string
	Color             string
	SteeringWheel     string
	FromMileage       int64
	ToMileage         int64
	FromCapacity      float64
	ToCapacity        float64
	FromPrice         float64
	ToPrice           float64
	FromCheaperMarket float64
	ToCheaperMarket   float64
	FromDaysOnSale    int
	ToDaysOnSale      int
	Region            string
	Sites             string
	AmountOfAds       int
	WithPhoto         bool
	CustomCleared     bool
	Emergency         bool
	CreatedAt         *time.Time `sql:"DEFAULT:'now()'"`
	UpdatedAt         *time.Time `sql:"DEFAULT:'now()'"`
}

type FilterCarResultKolesa struct {
	Brand         string   `json:"brand"`
	Model         string   `json:"model"`
	Color         string   `json:"color"`
	NotInMovement bool     `json:"not_in_movement"`
	Year          int64    `json:"year"`
	City          string   `json:"city"`
	Body          string   `json:"body"`
	Mileage       int64    `json:"mileage"`
	Price         float64  `json:"price"`
	AveragePrice  float64  `json:"average_price"`
	PhoneNumbers  []string `json:"phone_numbers"`
	URL           string   `json:"url"`
	PublishedData string   `json:"published_data"`
}
